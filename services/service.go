package service

import (
	"context"
	"github.com/gin-gonic/gin"
	"net/http"
	"sla/common/storage/mongodb"
)

type Service struct {
	watchDB *mongodb.Mongo
	ctx     context.Context
	id      string
}

func (s *Service) SetWatchDB(watchDB *mongodb.Mongo) {
	s.watchDB = watchDB
}

func (s *Service) SetID(uuid string) {
	s.id = uuid
}

func (s *Service) HelloSLA(c *gin.Context) {
	type Response struct {
		Status int    `json:"status"`
		Data   string `json:"data"`
	}
	result := Response{
		Status: http.StatusOK,
		Data:   "Hello SLA",
	}
	c.JSON(http.StatusOK, result)
}

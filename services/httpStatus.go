package service

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func Success(c *gin.Context, data interface{}) {
	c.JSON(http.StatusOK, gin.H{
		"code": 20000,
		"data": data,
	})
}

func FailedBy400(c *gin.Context, data interface{}) {
	c.JSON(http.StatusBadRequest, gin.H{
		"code": 40000,
		"data": data,
	})

}

func FailedBy401(c *gin.Context, data interface{}) {
	c.JSON(http.StatusUnauthorized, gin.H{
		"code": 40001,
		"data": data,
	})

}

func FailedBy403(c *gin.Context, data interface{}) {
	c.JSON(http.StatusForbidden, gin.H{
		"code": 40003,
		"data": data,
	})

}

func FailedBy404(c *gin.Context, data interface{}) {
	c.JSON(http.StatusNotFound, gin.H{
		"code": 40004,
		"data": data,
	})

}
func FailedBy500(c *gin.Context, data interface{}) {
	c.JSON(http.StatusInternalServerError, gin.H{
		"code": 50000,
		"data": data,
	})

}

/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/4/24 10:19
*/

package service

import (
	"github.com/gin-gonic/gin"
	"sla/common/tools"
	"sla/public/sla/Display"
)

func (s *Service) GetSLADetailByTimestamp(c *gin.Context) {
	var apiparams sla.ApiSLADetailparams
	if err := c.BindJSON(&apiparams); err != nil {
		FailedBy400(c, err)
		return
	}
	//不得小于2020-01-01 00:00:00
	if apiparams.From <= 1577808000 || apiparams.From > apiparams.To || !tools.IsSameDay(apiparams.From, apiparams.To) {
		FailedBy400(c, "时间字段不正确。")
		return
	}
	if len(apiparams.Province.Province) <= 0 && len(apiparams.Province.Remark) <= 0 {
		FailedBy400(c, "省份和机房为必填项。")
		return
	}
	if apiparams.Top <= 0 {
		FailedBy400(c, "Top必须大于0。")
		return
	}
	search := sla.CreateSearch(s.watchDB)
	data, err := search.SearchDetailByTime(apiparams)
	if err != nil {
		FailedBy500(c, err)
		return
	}
	Success(c, data)
}

func (s *Service) GetSLAsByType(c *gin.Context) {
	var apiparams sla.ApiSLAparams
	if err := c.BindJSON(&apiparams); err != nil {
		FailedBy400(c, err)
		return
	}
	//不得小于2020-01-01 00:00:00
	if apiparams.From <= 1577808000 || apiparams.From > apiparams.To || (apiparams.To-apiparams.From) > 60*60*24 {
		FailedBy400(c, "时间字段不正确。")
		return
	}
	if len(apiparams.Type) <= 0 {
		FailedBy400(c, "Type 不能为空。")
		return
	}
	search := sla.CreateSearch(s.watchDB)
	data, err := search.SearchSLAsByType(apiparams)
	if err != nil {
		FailedBy500(c, err)
		return
	}
	Success(c, data)
}

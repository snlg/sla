/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/4/16 9:51
*/

package service

import (
	"context"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
	"sla/common/config"
	"sla/common/tools"
	"sla/public/parameters"
	display "sla/public/sla/Display"
	"sla/public/sla/sla"
	"strconv"
	"sync"
)

const uuidKey = "requestUUID"

func (s *Service) CalculateSLA(c *gin.Context) {
	uuid := tools.GetUUID()
	logger := zap.L()
	logger = logger.With(zap.String("uuid", uuid))

	s.ctx = context.WithValue(context.Background(), uuidKey, uuid)

	logger.Info("开始执行CalculateSLA方法")
	var apiparams display.ApiSLAparams
	if err := c.BindJSON(&apiparams); err != nil {
		logger.Error("解析参数失败")
		FailedBy400(c, err)
		return
	}
	logger.Debug("解析参数", zap.Any("params", apiparams))
	//不得小于2020-01-01 00:00:00
	if apiparams.From <= 1577808000 || apiparams.From > apiparams.To {
		logger.Error("时间字段不正确")
		FailedBy400(c, "时间字段不正确")
		return
	}
	var types []string
	if apiparams.Type == "" {
		types = append(types, sla.Min, sla.Hour, sla.Day, sla.Month, sla.Year)
	} else {
		types = append(types, apiparams.Type)
	}
	params := parameters.LoadConfig(s.watchDB)
	var provinces []parameters.Province
	if apiparams.Province.Province == "" {
		// 数据库获取省份列表
		types = append(types, sla.Min, sla.Hour, sla.Day, sla.Month, sla.Year)
		provinces = params.Provinces
	} else {
		provinces = append(provinces, apiparams.Province)
	}

	var wg sync.WaitGroup
	logger.Info("收到请求，开始异步处理...")
	Success(c, "收到请求")
	//每个省份单独线程执行
	for _, v := range provinces {
		wg.Add(1)
		go func(v parameters.Province) {
			defer wg.Done()
			var a display.ApiSLAparams
			a.Province = v
			a.From = apiparams.From
			a.To = apiparams.To
			for _, t := range types {
				a.Type = t
				c := display.CreateStand(s.watchDB, params)
				c.GenParams(a).CalAndWriteSLA()
			}
		}(v)
	}
	go func() {
		wg.Wait()
		logger.Info("已完成本次调用。")
	}()
}

func (s *Service) SendSLAToES(c *gin.Context) {
	uuid := tools.GetUUID()
	logger := zap.L()
	logger = logger.With(zap.String("uuid", uuid))

	s.ctx = context.WithValue(context.Background(), uuidKey, uuid)

	from, err := strconv.ParseInt(c.Query("from"), 10, 64)
	if err != nil {
		logger.Error("参数from不正确")
		FailedBy400(c, "参数from不正确")
		return
	}

	to, err := strconv.ParseInt(c.Query("to"), 10, 64)
	if err != nil {
		logger.Error("参数to不正确")
		FailedBy400(c, "参数to不正确")
		return
	}
	if from >= to {
		logger.Error("参数不正确")
		FailedBy400(c, "参数不正确")
		return
	}
	es := sla.CreateSLAES(config.CreateConfig().GetConfig().MonitorES, s.watchDB, parameters.LoadConfig(s.watchDB))
	enums := []string{sla.Min, sla.Hour, sla.Day, sla.Month, sla.Year}

	var wg sync.WaitGroup
	logger.Info("收到请求，开始异步处理...")
	Success(c, "收到请求")
	wg.Add(1)
	t := sla.CreateSLATime()
	go func() {
		defer wg.Done()
		for _, e := range enums {
			var startTime, endTime int64
			switch e {
			case sla.Min:
				endTime = t.TruncateSeconds(to)
				startTime = t.TruncateSeconds(from)
			case sla.Hour:
				endTime = t.TruncateMinutes(to)
				startTime = t.TruncateMinutes(from)
			case sla.Day:
				endTime = t.TruncateHours(to)
				startTime = t.TruncateHours(from)
			case sla.Month:
				endTime = t.TruncateDay(to)
				startTime = t.TruncateDay(from)
			case sla.Year:
				endTime = t.TruncateMonth(to)
				startTime = t.TruncateMonth(from)
			}
			es.GetDataByTime(startTime, endTime, e).WriteToES()

		}
	}()

	go func() {
		wg.Wait()
		logger.Info("已完成本次调用。")
	}()

}

func (s *Service) SendSLADetailToES(c *gin.Context) {
	uuid := tools.GetUUID()
	logger := zap.L()
	logger = logger.With(zap.String("uuid", uuid))

	from, err := strconv.ParseInt(c.Query("from"), 10, 64)
	if err != nil {
		logger.Error("参数from不正确")
		FailedBy400(c, "参数from不正确")
		return
	}

	to, err := strconv.ParseInt(c.Query("to"), 10, 64)
	if err != nil {
		logger.Error("参数to不正确")
		FailedBy400(c, "参数to不正确")
		return
	}
	if from >= to {
		logger.Error("参数不正确")
		FailedBy400(c, "参数不正确")
		return
	}

	es := sla.CreateSLAES(config.CreateConfig().GetConfig().MonitorES, s.watchDB, parameters.LoadConfig(s.watchDB))

	var wg sync.WaitGroup
	logger.Info("收到请求，开始异步处理...")
	Success(c, "收到请求")
	wg.Add(1)
	go func() {
		defer wg.Done()
		es.GetDetailByTime(from, to, 0).WriteDetailToES()
	}()

	go func() {
		wg.Wait()
		logger.Info("已完成本次调用。")
	}()

}

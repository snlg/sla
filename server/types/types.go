/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/4/15 22:52
*/

package slaapi

type Server struct {
	Address string
	Port    string
	Mode    string
}

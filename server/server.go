/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/4/15 22:49
*/

package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
	"sla/common/config"
	"sla/common/logger"
	"sla/common/storage/mongodb"
	"sla/common/tools"
	"sla/router"
	service "sla/services"
	"strings"
	"time"
)

func main() {
	err := Run()
	if err != nil {
		fmt.Println("异常退出...")
		fmt.Println(err)
	}
}

func Run() error {
	//config := config.Get()
	fmt.Println("*** 尝试加载yaml配置文件 ***")
	c := config.CreateConfig().GetConfig()
	fmt.Println("*** yaml配置文件加载完成 ***")
	err := logger.InitLogger(&c.Log)
	if err != nil {
		fmt.Println("*** 无法加载日志模块 ***")
		return err
	}
	zap.L().Info("加载日志模块成功")
	mConf := c.Mongo
	mgConf := mongodb.MongoConf{
		TimeoutSeconds: 0,
		MaxOpenConns:   0,
		MaxIdleConns:   0,
		URI:            mConf.BuildURI(),
		RsName:         mConf.RsName,
		SocketTimeout:  0,
	}
	m, err := mongodb.NewMgo(mgConf, time.Second*15)
	if err != nil {
		zap.L().Error(err.Error())
		return err
	}
	s := new(service.Service)
	s.SetWatchDB(m)
	s.SetID(tools.GetUUID())

	switch strings.ToLower(c.Server.Mode) {
	case "dev":
		gin.SetMode(gin.DebugMode)
		zap.L().Info("进入 Debug 模式")
	default:
		gin.SetMode(gin.ReleaseMode)
		zap.L().Info("进入 Release 模式")
	}
	r := router.SetupRouter(*s)
	zap.L().Info("监听：" + c.Server.Address + ":" + c.Server.Port)
	_ = r.Run(c.Server.Address + ":" + c.Server.Port)
	return nil
}

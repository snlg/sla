/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/4/10 21:28
*/

package sla

import (
	"context"
	"fmt"
	"go.uber.org/zap"
	"sla/common/config"
	"sla/common/storage/elasticsearch"
	"sla/common/storage/mongodb"
	"sla/common/tools"
	"sla/public/parameters"
	sla2 "sla/public/sla/sla"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"
)

const uuidKey = "requestUUID"

type calParams struct {
	count    int
	duration int64
	t        int64
	province string
	remark   string
	types    string
	p        *parameters.Parameters
	mongo    *mongodb.Mongo
	ctx      context.Context
	esConf   elasticsearch.MonitorES
}

type Stand interface {
	GeneratorRecentSLA() //计算最近n分钟的sla,实时刷新各个时间维度的值

	//GeneratorSLAByDateRange(start int64, end int64)      //按挤时间范围计算全部sla，包括时间范围内的全部时间维度的值
	//GeneratorSLAByProvince(province parameters.Province) //按省份计算sla

	GetRecentSLA() Stand
	GetRecentSLADetail() Stand
	GetRPMByGroup()
	GenParams(p ApiSLAparams) Stand
	Alert(slaAlert config.Alert)
	SetES(esConf elasticsearch.MonitorES) Stand
	CalAndWriteSLA()
}

func CreateStand(mongo *mongodb.Mongo, parameters *parameters.Parameters) Stand {
	return &calParams{
		mongo: mongo,
		p:     parameters,
		ctx:   context.WithValue(context.Background(), uuidKey, tools.GetUUID()),
	}
}

func (c *calParams) SetES(esConf elasticsearch.MonitorES) Stand {
	c.esConf = esConf
	return c
}

func (c *calParams) GetRecentSLA() Stand {
	sla := sla2.CreateSLAES(c.esConf, c.mongo, c.p)
	sla.GetDataByType("min").WriteToES()
	sla.GetDataByType("hour").WriteToES()
	sla.GetDataByType("day").WriteToES()
	sla.GetDataByType("month").WriteToES()
	sla.GetDataByType("year").WriteToES()
	return c
}

func (c *calParams) GetRecentSLADetail() Stand {
	sla := sla2.CreateSLAES(c.esConf, c.mongo, c.p)
	currentTime := time.Now()
	tenMinutesAgo := currentTime.Add(-10 * time.Minute)

	// 数据采集延迟导致的脏数据先进行移除后再写入
	// top计划是1000，但是导致部分数据无货获取，暂时改成0获取全量明细数据
	sla.GetDetailByTime(tenMinutesAgo.Unix(), currentTime.Unix(), 0).
		RemoveDetailFromES(tenMinutesAgo.Unix(), currentTime.Unix()).
		WriteDetailToES()
	//sla.GetDetailByTime(tenMinutesAgo.Unix(), currentTime.Unix(), 0).
	//	WriteDetailToES()
	return c
}

func (c *calParams) CalAndWriteSLA() {
	uuid := c.ctx.Value(uuidKey).(string)
	logger := zap.L()
	logger = logger.With(zap.String("uuid", uuid))

	for i := 0; i < c.count; i++ {
		s := sla2.CreateSLA(c.province, c.remark, c.p, c.t-int64(i)*c.duration, c.types, c.mongo)
		s, err := s.SetID()
		if err != nil {
			logger.Error("SetID 失败", zap.Any("error", err))
			continue
		}
		zap.L().Debug("开始计算，计算参数", zap.Any("sla", s))
		_ = s.CalSLA().Write()
	}
}

func (c *calParams) GenParams(p ApiSLAparams) Stand {
	uuid := c.ctx.Value(uuidKey).(string)
	logger := zap.L()
	logger = logger.With(zap.String("uuid", uuid))

	logger.Info("已开始处理" + fmt.Sprintf("%+v", p))
	t := sla2.CreateSLATime().TruncateSeconds(p.To)
	tHour := sla2.CreateSLATime().TruncateMinutes(t)
	tDay := sla2.CreateSLATime().TruncateHours(t)
	tMonth := sla2.CreateSLATime().TruncateDay(t)
	tYear := sla2.CreateSLATime().TruncateMonth(t)
	var count int64
	var duration int64
	var T int64
	switch p.Type {
	case sla2.Min:
		count = (p.To-p.From)/60 + 1
		duration = 60
		T = t
	case sla2.Hour:
		count = (p.To-p.From)/(60*60) + 1
		duration = 60 * 60
		T = tHour
	case sla2.Day:
		count = (p.To-p.From)/(60*60*24) + 1
		duration = 60 * 60 * 24
		T = tDay
	case sla2.Month:
		count = (p.To-p.From)/(60*60*24*tools.GetDaysInMonth(tMonth)) + 1
		duration = 60 * 60 * 24 * tools.GetDaysInMonth(tMonth)
		T = tMonth
	case sla2.Year:
		count = (p.To-p.From)/(60*60*24*tools.GetDaysInYear(tMonth)) + 1
		duration = 60 * 60 * 24 * tools.GetDaysInYear(tMonth)
		T = tYear
	}
	c.count = int(count)
	c.duration = duration
	c.t = T
	c.province = p.Province.Province
	c.remark = p.Province.Remark
	c.types = p.Type
	logger.Info("GenParams本次调用已完成")
	return c
}

func (c *calParams) GeneratorRecentSLA() {
	uuid := c.ctx.Value(uuidKey).(string)
	logger := zap.L()
	logger = logger.With(zap.String("uuid", uuid))

	logger.Info("本轮任务已开始执行...")

	//生成最近10分钟的sla
	t := sla2.CreateSLATime().TruncateSeconds(time.Now().Unix())
	tHour := sla2.CreateSLATime().TruncateMinutes(t)
	tDay := sla2.CreateSLATime().TruncateHours(t)
	tMonth := sla2.CreateSLATime().TruncateDay(t)
	tYear := sla2.CreateSLATime().TruncateMonth(t)

	var wg sync.WaitGroup

	//每个省份单独线程执行
	for _, v := range c.p.Provinces {
		wg.Add(1)
		go func(v parameters.Province) {
			defer wg.Done()
			//计算最近minutes分钟内的sla并记录
			c.calculateAndLogSLA(t, 5, 60, v, sla2.Min)
			//计算t所在前后2小时内的sla（小时）并记录
			c.calculateAndLogSLA(tHour, 2, 60*60, v, sla2.Hour)
			//计算t所在前后2天内的sla（天）并记录
			c.calculateAndLogSLA(tDay, 2, 60*60*24, v, sla2.Day)
			//计算t所在前后2月内的sla（月）并记录
			c.calculateAndLogSLA(tMonth, 2, 60*60*24*tools.GetDaysInMonth(tMonth), v, sla2.Month)
			//计算t所在前后2年内的sla（年）并记录
			c.calculateAndLogSLA(tYear, 2, 60*60*24*tools.GetDaysInYear(tYear), v, sla2.Year)

			logger.Debug("已完成：" + v.Province + ", " + v.Remark)
		}(v)
	}

	wg.Wait()
	logger.Info("本轮GeneratorRecentSLA任务已全部完成。")
}

func (c *calParams) calculateAndLogSLA(t int64, count int, duration int64, province parameters.Province, types string) {
	cp := calParams{
		count:    count,
		duration: duration,
		t:        t,
		province: province.Province,
		remark:   province.Remark,
		types:    types,
		mongo:    c.mongo,
		p:        c.p,
		ctx:      c.ctx,
	}

	zap.L().Debug("准备计算，计算参数", zap.Int("calParams.count", cp.count),
		zap.Int64("calParams.duration", cp.duration),
		zap.Int64("calParams.t", cp.t),
		zap.String("calParams.province", cp.province),
		zap.String("calParams.remark", cp.remark),
		zap.String("calParams.types", cp.types))
	cp.CalAndWriteSLA()
}

func (c *calParams) GetRPMByGroup() {
	slaES := sla2.CreateSLAES(c.esConf, c.mongo, c.p)

	currentTime := sla2.CreateSLATime()
	calTime := currentTime.TruncateSeconds(currentTime.Now())
	slaES.GetGroupsByTime(calTime).WriteGroupsToES()

}

func (c *calParams) Alert(slaalert config.Alert) {
	//	按当前时间的倒数第二分钟的数据进行告警判断，比如当前时间是14:23，则获取14:21的SLA数据进行判断告警
	//	目前告警策略按分钟级数据告警，更灵敏
	uuid := c.ctx.Value(uuidKey).(string)
	logger := zap.L()
	logger = logger.With(zap.String("uuid", uuid))

	logger.Info("开始进行告警信息推送...")

	current := time.Now()
	alertStart := current.Add(-3 * time.Minute)

	sla := sla2.CreateBasicSLA(c.p, c.mongo)
	risk := make([]sla2.Detail, 0)
	riskDefault, err := sla.GetDetailByRisk("", "", alertStart.Unix(), alertStart.Unix()+60, 100, parameters.MongoCollectionMetadata)
	riskSMS, err := sla.GetDetailByRisk("", "", alertStart.Unix(), alertStart.Unix()+60, 10000, parameters.MongoCollectionMetadataSms)

	risk = append(risk, riskDefault...)
	risk = append(risk, riskSMS...)
	if err != nil {
		logger.Error("获取数据失败," + err.Error())
		return
	}
	logger.Debug("告警原始数据量：" + strconv.Itoa(len(risk)))

	// 已经在getTopResults中进行排序，此处不再需要排序
	//sort.Slice(risk, func(i, j int) bool {
	//	if risk[i].RiskWeight == risk[j].RiskWeight {
	//		return risk[i].URL < risk[j].URL
	//	}
	//	return risk[i].RiskWeight > risk[j].RiskWeight
	//})

	type alertsData struct {
		description string
		affectedUrl string
		sla         float64
	}
	count := make(map[string]int)
	alertsMsg := make(map[string]alertsData)

	for _, detail := range risk {
		logger.Debug("处理明细数据", zap.Any("detail", detail))
		//if detail.RiskWeight > slaalert.Threshold {
		area := detail.Province + "-" + detail.ReMark
		v := strconv.FormatFloat(detail.RiskWeight*100, 'f', 4, 64)
		//slaV := strconv.FormatFloat((1-detail.RiskWeight)*100, 'f', 4, 64)
		slaV := detail.RiskWeight
		if val, ok := alertsMsg[area]; ok {
			slaV += val.sla
		}
		if slaV > 1 {
			slaV = 1
		}
		ratioMsg := ""
		if detail.Province == parameters.SLATypeSMS {
			kind := c.p.SMSDevice[detail.IPAddress].Kind
			describe := c.p.SMSDevice[detail.IPAddress].Describe
			ratioMsg += "[" + kind + ", " + detail.IPAddress + ", " + describe + "]"
		}
		ratioMsg += "["
		if detail.RtRisk > 0 {
			ratioMsg += "响应时间 " + strconv.FormatFloat(detail.RtRisk*100, 'f', 0, 64) + "%, "
		}
		if detail.ErRisk > 0 {
			ratioMsg += "状态码 " + strconv.FormatFloat(detail.ErRisk*100, 'f', 0, 64) + "%, "
		}
		if detail.ElRisk > 0 {
			ratioMsg += "错误日志 " + strconv.FormatFloat(detail.ElRisk*100, 'f', 0, 64) + "%, "
		}
		if detail.AlRisk > 0 {
			ratioMsg += "请求量 " + strconv.FormatFloat(detail.AlRisk*100, 'f', 0, 64) + "%, "
		}
		if detail.ActRisk > 0 {
			ratioMsg += "降级量 " + strconv.FormatFloat(detail.ActRisk*100, 'f', 0, 64) + "%, "
		}
		if len(ratioMsg) > 0 {
			ratioMsg = ratioMsg[:len(ratioMsg)-2]
		}
		ratioMsg += "]"
		if _, ok := alertsMsg[area]; !ok {
			alertsMsg[area] = alertsData{}
		}
		url := strings.ReplaceAll(tools.RemoveRegexSymbols(detail.URL), "*", "\\*")
		if count[area] < 5 {
			alertsMsg[area] = alertsData{
				affectedUrl: alertsMsg[area].affectedUrl + url, //避免与md中的关键字冲突
				description: alertsMsg[area].description + "\n##### **" + url + " 影响 " + v + "% " + ratioMsg + "**",
				sla:         slaV,
			}
		} else {
			alertsMsg[area] = alertsData{
				affectedUrl: alertsMsg[area].affectedUrl,
				description: alertsMsg[area].description,
				sla:         slaV,
			}
		}
		count[area]++
	}
	conf := config.CreateConfig().GetConfig()
	for key, value := range alertsMsg {
		if count[key] > 5 {
			value.description += "\n##### ** ( 剩余 " + strconv.Itoa(count[key]-5) + " 条未显示 ) **"
		}
		s := (1 - value.sla) * 100
		level := getValueForThreshold(s, conf.SLAAlertConfig.Conditions)
		if level == 0 {
			logger.Info("不满足告警阈值", zap.Any("value", value))
			continue
		}
		ystAlert := tools.YstAlertMessage{
			Name:        slaalert.Name,
			Type:        slaalert.Type,
			Area:        key,
			Business:    "SLA",
			StartTime:   alertStart.Unix(),
			Value:       strconv.FormatFloat(s, 'f', 4, 64),
			Description: "当前SLA为 " + strconv.FormatFloat(s, 'f', 4, 64) + "% " + value.description,
			Level:       level,
			Source:      "SLA汇聚平台",
			AlertStatus: "PENDING",
		}
		ystAlert.Attributes.AffectedUrl = value.affectedUrl
		alert := tools.YSTAlert{Webhook: slaalert.Webhook, Msg: ystAlert}
		alert.SendWithMarkdown()
		logger.Debug("YstAlertMessage", zap.Any("alert", ystAlert))
	}
	logger.Info("告警信息推送完成，推送项：" + strconv.Itoa(len(alertsMsg)))

}

func getValueForThreshold(input float64, conditions []config.Condition) int {
	// 按阈值从大到小排序
	sort.SliceStable(conditions, func(i, j int) bool {
		return conditions[i].Threshold > conditions[j].Threshold
	})

	// 如果输入值大于最高阈值，则返回 0
	if input > conditions[0].Threshold {
		return 0
	}

	// 遍历条件数组，找到输入值对应的区间
	for i := 0; i < len(conditions)-1; i++ {
		if input <= conditions[i].Threshold && input > conditions[i+1].Threshold {
			return conditions[i].Value
		}
	}

	// 如果输入值小于最低阈值，则返回最低阈值对应的值
	if input <= conditions[len(conditions)-1].Threshold {
		return conditions[len(conditions)-1].Value
	}

	return 0 // 默认返回值，防止意外情况
}

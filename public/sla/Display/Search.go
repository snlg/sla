/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/4/24 10:45
*/

package sla

import (
	"context"
	"sla/common/storage/mongodb"
	"sla/public/parameters"
	sla2 "sla/public/sla/sla"
)

type Search interface {
	SearchDetailByTime(detailparams ApiSLADetailparams) (interface{}, error)
	SearchSLAsByType(params ApiSLAparams) (interface{}, error)
}

type search struct {
	mongo      *mongodb.Mongo
	ctx        context.Context
	parameters *parameters.Parameters
}

func (s *search) SearchSLAsByType(params ApiSLAparams) (interface{}, error) {
	sla := sla2.CreateBasicSLA(s.parameters, s.mongo)
	x, err := sla.GetSLAByProvince(params.Province.Province, params.Province.Remark, params.From, params.To, params.Type)
	if err != nil {
		return nil, err
	}
	return x, nil
}

func (s *search) SearchDetailByTime(detailparams ApiSLADetailparams) (interface{}, error) {
	sla := sla2.CreateBasicSLA(s.parameters, s.mongo)
	var x []sla2.Detail
	var err error
	collectionName := parameters.MongoCollectionMetadata
	if detailparams.Province.Province == parameters.SLATypeSMS {
		collectionName = parameters.MongoCollectionMetadataSms
	}
	x, err = sla.GetDetailByRisk(
		detailparams.Province.Province,
		detailparams.Province.Remark,
		detailparams.From,
		detailparams.To,
		detailparams.Top,
		collectionName)

	if err != nil {
		return nil, err
	}
	return x, nil
}

func CreateSearch(m *mongodb.Mongo) Search {
	p := parameters.LoadConfig(m)
	return &search{
		mongo:      m,
		parameters: p,
	}
}

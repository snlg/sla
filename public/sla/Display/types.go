/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/4/16 11:42
*/

package sla

import "sla/public/parameters"

type ApiSLAparams struct {
	Type     string              `json:"type"`
	From     int64               `json:"from" `
	To       int64               `json:"to"`
	Province parameters.Province `json:"province"`
}

type ApiSLADetailparams struct {
	From     int64               `json:"from" `
	To       int64               `json:"to"`
	Province parameters.Province `json:"province"`
	Top      int                 `json:"top"`
}

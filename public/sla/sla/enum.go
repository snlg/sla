/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/4/10 21:34
*/

package sla

const (
	Min   string = "min"
	Hour  string = "hour"
	Day   string = "day"
	Month string = "month"
	Year  string = "year"
)

func CalType(input string) string {
	switch input {
	case "min":
		return "Any"
	case "hour":
		return "min"
	case "day":
		return "hour"
	case "month":
		return "day"
	case "year":
		return "day"
	default:
		return "None"
	}
}

/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/4/15 14:59
*/

package sla

import (
	"go.uber.org/zap"
	"sla/common/storage/elasticsearch"
	"sla/common/storage/mongodb"
	"sla/common/tools"
	"sla/public/parameters"
	"strconv"
	"time"
)

type slaES struct {
	esConf    elasticsearch.MonitorES
	slaData   []sla
	slaDetail []Detail
	slaGroups []MetaGroups
	mongo     *mongodb.Mongo
	p         *parameters.Parameters
}

func CreateSLAES(e elasticsearch.MonitorES, m *mongodb.Mongo, p *parameters.Parameters) SLAES {
	return &slaES{
		esConf: e,
		mongo:  m,
		p:      p,
	}
}

type SLAES interface {
	GetDataByType(types string) SLAES
	WriteToES()
	WriteDetailToES()
	RemoveDetailFromES(startTime int64, endTime int64) SLAES
	GetDataByTime(startTime int64, endTime int64, types string) SLAES
	GetDetailByTime(startTime int64, endTime int64, top int) SLAES
	GetGroupsByTime(timestamp int64) SLAES
	WriteGroupsToES()
}

func (s *slaES) WriteGroupsToES() {
	client, err := elasticsearch.NewElasticClient(s.esConf)
	index := s.esConf.GroupIndex
	if err != nil {
		zap.L().Error("初始化 ElasticSearch client 失败, " + err.Error())
		return
	}
	zap.L().Error("WriteGroupToES获取要要写入的数据量, ", zap.Any("len", len(s.slaGroups)))
	if len(s.slaGroups) > 0 {
		err := client.IndexDocumentsInBatches(index, convertGroupsToMap(s.slaGroups), 1000) //batchSize后续可以配置到配置文件里
		if err != nil {
			zap.L().Error("写入 ElasticSearch 失败, " + err.Error())
			return
		}
		zap.L().Info("写入 ElasticSearch 成功 ")
	} else {
		zap.L().Error("未查询到数据")
	}

}

func (s *slaES) GetGroupsByTime(timestamp int64) SLAES {
	sla := CreateBasicSLA(s.p, s.mongo)
	groups, err := sla.GetRpmByGroup(timestamp, parameters.MongoCollectionMetadataSms)
	if err != nil {
		zap.L().Error("获取数据失败, " + err.Error())
	} else if len(groups) > 0 {
		s.slaGroups = groups
	}
	return s
}

func convertGroupsToMap(groups []MetaGroups) []map[string]interface{} {
	var groupMaps []map[string]interface{}
	for _, group := range groups {
		t := time.Unix(group.Timestamp, 0).UTC()
		//生成唯一ID
		id := tools.CalculateSHA1(strconv.FormatInt(group.Timestamp, 10) + group.Group + group.GroupClass + group.Url)
		groupMap := map[string]interface{}{
			//"id":         detail.ID,
			"id":         id,
			"URL":        group.Url,
			"@timestamp": t.Format("2006-01-02T15:04:05.000Z"),
			"Group":      group.Group,
			"GroupClass": group.GroupClass,
			"CName":      group.CName,
			"AllLines":   group.AllLines,
		}
		groupMaps = append(groupMaps, groupMap)
	}
	return groupMaps
}

// ConvertSLAToMap 将 sla 切片转换为 []map[string]interface{}
func convertSLAToMap(s []sla) []map[string]interface{} {
	var sMaps []map[string]interface{}
	for _, item := range s {
		t := time.Unix(item.Timestamp, 0).UTC()
		sla := item.SLA //grafana中小数怎么都展示不出来，这里转成可以保留4位小数的整数，后面再研究，已解决
		sMap := map[string]interface{}{
			"id":         item.ID,
			"type":       item.Type,
			"@timestamp": t.Format("2006-01-02T15:04:05.000Z"),
			"province":   item.Province,
			"reMark":     item.ReMark,
			"SLA":        sla,
		}
		sMaps = append(sMaps, sMap)
	}
	return sMaps
}

func convertDetailToMap(details []Detail) []map[string]interface{} {
	var detailMaps []map[string]interface{}
	for _, detail := range details {
		t := time.Unix(detail.TimeStamp, 0).UTC()
		//生成唯一ID
		id := tools.CalculateSHA1(strconv.FormatInt(detail.TimeStamp, 10) + detail.Province + detail.ReMark + detail.URL)
		detailMap := map[string]interface{}{
			//"id":         detail.ID,
			"id":         id,
			"URL":        detail.URL,
			"@timestamp": t.Format("2006-01-02T15:04:05.000Z"),
			"RiskWeight": detail.RiskWeight,
			"RtRisk":     detail.RtRisk,
			"ErRisk":     detail.ErRisk,
			"ElRisk":     detail.ElRisk,
			"AlRisk":     detail.AlRisk,
			"ActRisk":    detail.ActRisk,
			"Province":   detail.Province,
			"Remark":     detail.ReMark,
			"IPAddress":  detail.IPAddress,
		}
		detailMaps = append(detailMaps, detailMap)
	}
	return detailMaps
}

func (s *slaES) GetDataByTime(startTime int64, endTime int64, types string) SLAES {
	s.slaData = []sla{}
	sla := CreateBasicSLA(s.p, s.mongo)
	data, err := sla.GetSLAByTime(startTime, endTime, types)
	if err != nil {
		zap.L().Error("获取数据失败, " + err.Error())
	} else if len(data) > 0 {
		s.slaData = data
	}
	return s
}

func (s *slaES) GetDataByType(types string) SLAES {
	t := CreateSLATime()
	s.slaData = []sla{}
	var startTime, endTime int64

	switch types {
	case Min:
		endTime = t.Now()
		startTime = t.SecondsAgo(60 * 60) //最近60分钟的数据
	case Hour:
		endTime = t.TruncateMinutes(t.Now())
		startTime = t.TruncateMinutes(t.SecondsAgo(60 * 60)) //最近2小时的数据
	case Day:
		endTime = t.TruncateHours(t.Now())
		startTime = t.TruncateHours(t.SecondsAgo(24 * 60 * 60)) //最近2天
	case Month:
		endTime = t.TruncateDay(t.Now())
		startTime = t.TruncateDay(t.SecondsAgo(24 * 60 * 60 * tools.GetDaysInMonth(endTime))) //最近2月
	case Year:
		endTime = t.TruncateMonth(t.Now())
		startTime = t.TruncateMonth(t.SecondsAgo(24 * 60 * 60 * tools.GetDaysInYear(endTime))) //最近2年
	default:
		zap.L().Error("类型不存在：" + types)
		return nil
	}
	sla := CreateBasicSLA(s.p, s.mongo)
	data, err := sla.GetSLAByTime(startTime, endTime, types)
	if err != nil {
		zap.L().Error("获取day数据失败, " + err.Error())
	} else if len(data) > 0 {
		s.slaData = data
	}
	return s
}

func (s *slaES) WriteToES() {
	client, err := elasticsearch.NewElasticClient(s.esConf)
	index := s.esConf.SLAIndex
	if err != nil {
		zap.L().Error("初始化 ElasticSearch client 失败, " + err.Error())
		return
	}
	if len(s.slaData) > 0 {
		err := client.IndexDocumentsInBatches(index, convertSLAToMap(s.slaData), 1000) //batchSize后续可以配置到配置文件里
		if err != nil {
			zap.L().Error("写入 ElasticSearch 失败, " + err.Error())
			return
		}
		zap.L().Info("写入 ElasticSearch 成功 ")
	} else {
		zap.L().Error("未查询到数据")
	}

}

func (s *slaES) GetDetailByTime(startTime int64, endTime int64, top int) SLAES {
	logger := zap.L()
	start := time.Now()
	defer func() {
		logger.Info("GetDetailByTime总耗时", zap.Any("during", time.Since(start)))
	}()
	sla := CreateBasicSLA(s.p, s.mongo)
	var details []Detail
	collectionNames := []string{parameters.MongoCollectionMetadata, parameters.MongoCollectionMetadataSms}
	//collectionNames := []string{"SMSMetadata"}
	for _, collectionName := range collectionNames {
		x, err := sla.GetDetailByRisk("", "", startTime, endTime, top, collectionName)
		logger.Info("GetDetailByRisk获取数据量", zap.Any("x", len(x)))
		// testing only
		//x, err := sla.GetDetailByRisk("浙江", "网管", startTime, endTime, top, collectionName)

		if err != nil {
			zap.L().Error(err.Error())
		} else {
			details = append(details, x...)
		}
	}
	s.slaDetail = details
	logger.Info("GetDetailByRisk总数据量", zap.Any("x", len(s.slaDetail)))
	return s

}

func (s *slaES) RemoveDetailFromES(startTime int64, endTime int64) SLAES {
	client, err := elasticsearch.NewElasticClient(s.esConf)
	index := s.esConf.DetailIndex
	if err != nil {
		zap.L().Error("初始化 ElasticSearch client 失败, " + err.Error())
		return s
	}
	err = client.DeleteDocumentsByTimeRange(index, startTime, endTime)
	if err != nil {
		zap.L().Error("删除文档失败, " + err.Error())
	} else {
		zap.L().Info("删除文档成功")
	}
	return s
}

func (s *slaES) WriteDetailToES() {
	client, err := elasticsearch.NewElasticClient(s.esConf)
	index := s.esConf.DetailIndex
	if err != nil {
		zap.L().Error("初始化 ElasticSearch client 失败, " + err.Error())
		return
	}
	zap.L().Error("WriteDetailToES获取要要写入的数据量, ", zap.Any("len", len(s.slaDetail)))
	if len(s.slaDetail) > 0 {
		err := client.IndexDocumentsInBatches(index, convertDetailToMap(s.slaDetail), 1000) //batchSize后续可以配置到配置文件里
		if err != nil {
			zap.L().Error("写入 ElasticSearch 失败, " + err.Error())
			return
		}
		zap.L().Info("写入 ElasticSearch 成功 ")
	} else {
		zap.L().Error("未查询到数据")
	}

}

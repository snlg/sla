/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/4/10 21:56
*/

package sla

import (
	"time"
)

type slaTime struct {
	Timestamp int64
}

func CreateSLATime() SLATime {
	return &slaTime{
		Timestamp: time.Now().Unix(),
	}
}

type SLATime interface {
	TruncateSeconds(timestamp int64) int64
	TruncateMinutes(timestamp int64) int64
	TruncateHours(timestamp int64) int64
	TruncateDay(timestamp int64) int64
	TruncateMonth(timestamp int64) int64
	SecondsAgo(t int64) int64
	Now() int64
}

// TruncateSeconds 将时间戳的秒置为0
func (s slaTime) TruncateSeconds(timestamp int64) int64 {
	return timestamp / 60 * 60
}

// TruncateMinutes 将时间戳的分置为0
func (s slaTime) TruncateMinutes(timestamp int64) int64 {
	return timestamp / 3600 * 3600
}

// TruncateHours 将时间戳的小时置为0
func (s slaTime) TruncateHours(timestamp int64) int64 {
	t := time.Unix(timestamp, 0)
	t = time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, t.Location())
	return t.Unix()
}

// TruncateDay 将时间戳的天置为01
func (s slaTime) TruncateDay(timestamp int64) int64 {
	t := time.Unix(timestamp, 0)
	t = time.Date(t.Year(), t.Month(), 1, 0, 0, 0, 0, t.Location())
	return t.Unix()
}

// TruncateMonth 将时间戳的月置为01
func (s slaTime) TruncateMonth(timestamp int64) int64 {
	t := time.Unix(timestamp, 0)
	t = time.Date(t.Year(), 1, 1, 0, 0, 0, 0, t.Location())
	return t.Unix()
}

func (s slaTime) SecondsAgo(t int64) int64 {
	return s.Timestamp - t
}

func (s slaTime) Now() int64 {
	return s.Timestamp
}

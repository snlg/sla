/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/4/11 10:00
*/

package sla

import (
	"context"
	"errors"
	"fmt"
	"go.uber.org/zap"
	"math"
	"reflect"
	"regexp"
	"sla/common/storage/mongodb"
	"sla/common/storage/mongodb/types"
	"sla/common/tools"
	"sla/public/metaData"
	"sla/public/parameters"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"
)

type sla struct {
	ID         string  `json:"-" bson:"_id"`
	Type       string  `json:"type" bson:"type"`
	Timestamp  int64   `json:"timestamp" bson:"timestamp"`
	Province   string  `json:"province" bson:"province"`
	ReMark     string  `json:"reMark" bson:"reMark"`
	SLA        float64 `json:"SLA" bson:"SLA"`
	parameters *parameters.Parameters
	mongo      *mongodb.Mongo
	ctx        context.Context
}

type Detail struct {
	//ID         string  `json:"-"`
	Province   string  `json:"Province"`
	ReMark     string  `json:"ReMark"`
	IPAddress  string  `json:"IPAddress"`
	URL        string  `json:"Url" `
	TimeStamp  int64   `json:"@TimeStamp"`
	RiskWeight float64 `json:"RiskWeight"`
	RtRisk     float64 `json:"RtRisk"`
	ErRisk     float64 `json:"ErRisk"`
	ElRisk     float64 `json:"ElRisk"`
	AlRisk     float64 `json:"AlRisk"`
	ActRisk    float64 `json:"ActRisk"`
}

type MetaGroups struct {
	Group      string `json:"Group"`
	GroupClass string `json:"GroupClass"`
	Url        string `json:"Url"`
	CName      string `json:"CName"`
	AllLines   int    `json:"AllLines"`
	Timestamp  int64  `json:"TimeStamp"`
}

type SLA interface {
	CalSLA() SLA
	Write() error
	SetID() (SLA, error)
	GetDetailByRisk(province string, remark string, from int64, to int64, top int, collectionName string) ([]Detail, error)
	GetSLAByProvince(province string, remark string, from int64, to int64, types string) (interface{}, error)
	GetSLAByTime(from int64, to int64, types string) ([]sla, error)
	GetRpmByGroup(timestamp int64, collection string) ([]MetaGroups, error)
}

func (s *sla) GetRpmByGroup(timestamp int64, collection string) ([]MetaGroups, error) {
	//testing
	//timestamp = 1722418140
	//timestamp = 1723184040

	logger := zap.L()

	var ipAddresses []interface{}
	groupCond := make(map[string]interface{}, 0)
	groupClassCond := make(map[string]interface{}, 0)

	groupMaps := make(map[interface{}][]interface{}, 0)
	groupClassMaps := make(map[interface{}][]interface{}, 0)

	for _, device := range s.parameters.SMSDevice {
		ipAddresses = append(ipAddresses, device.IPAddress)

		groupMaps[device.Group] = append(groupMaps[device.Group], device.IPAddress)
		groupClassMaps[device.GroupClass] = append(groupClassMaps[device.GroupClass], device.IPAddress)

	}

	for gk, gv := range groupMaps {

		condElse := interface{}("Unknown")
		if len(groupCond) > 0 {
			condElse = groupCond
		}

		cond := map[string]interface{}{
			"$cond": []interface{}{
				map[string]interface{}{
					"$in": []interface{}{
						"$ipAddress",
						gv,
					},
				},
				gk,
				condElse,
			},
		}

		// 将新的条件存储到 groupClassCond 中
		groupCond = cond

	}

	for gk, gv := range groupClassMaps {

		condElse := interface{}("Unknown")
		if len(groupClassCond) > 0 {
			condElse = groupClassCond
		}

		// 构造当前的 $cond
		cond := map[string]interface{}{
			"$cond": []interface{}{
				map[string]interface{}{
					"$in": []interface{}{
						"$ipAddress",
						gv,
					},
				},
				gk,
				condElse,
			},
		}

		groupClassCond = cond

	}

	filter := []map[string]interface{}{
		{"$match": map[string]interface{}{
			"ipAddress": map[string]interface{}{"$in": ipAddresses},
			"timestamp": timestamp,
		}},
		{"$unwind": "$urls"},
		{"$group": map[string]interface{}{
			"_id": map[string]interface{}{
				"group":      groupCond,
				"groupclass": groupClassCond,
				"url":        "$urls.url",
				"timestamp":  "$timestamp",
			},
			"AllLines": map[string]interface{}{"$sum": "$urls.all_lines"},
		}},
		{"$project": map[string]interface{}{
			"_id":        0,
			"Group":      "$_id.group",
			"GroupClass": "$_id.groupclass",
			"Url":        "$_id.url",
			"AllLines":   1,
			"TimeStamp":  "$_id.timestamp",
		},
		},
	}

	logger.Debug("执行 GetRpmByGroup 方法", zap.Any("查询条件", filter))

	var result []MetaGroups
	//var result1 []interface{}
	if err := s.mongo.Table(collection).AggregateAll(context.Background(), filter, &result); err != nil {
		return result, err
	}

	updateMetaGroups(&result, s.parameters.SMSNameList)

	zap.L().Debug("result", zap.Any("result1", result))
	return result, nil
}

func updateMetaGroups(groups *[]MetaGroups, s map[string]string) {
	var wg sync.WaitGroup

	for i := range *groups {
		wg.Add(1)
		go func(i int) {
			defer wg.Done()
			updateCName(&(*groups)[i], s)
		}(i)
	}
	wg.Wait()
}

func updateCName(m *MetaGroups, s map[string]string) {
	group := m.Group
	urlPattern := getStringFromLastTwoSlashes(m.Url)
	id := group + urlPattern
	m.CName = s[id]
}

func getStringFromLastTwoSlashes(input string) string {
	lastSlash := strings.LastIndex(input, "/")
	if lastSlash == -1 {
		return input
	}
	secondLastSlash := strings.LastIndex(input[:lastSlash], "/")
	if secondLastSlash == -1 {
		return input
	}
	return input[secondLastSlash : lastSlash+1]
}

func (s *sla) CalSLA() SLA {
	switch s.Type {
	case Min:
		if s.Province == parameters.SLATypeSMS {
			s.SLA = s.calAnySLAByTimeSMS(60)
		} else {
			s.SLA = s.calAnySLAByTime(60)
		}
	case Hour:
		s.SLA = s.calAnySLABySLA(60*60, 60)
	case Day:
		s.SLA = s.calAnySLABySLA(24*60*60, 24)
	case Month:
		s.SLA = s.calAnySLABySLA(tools.GetDaysInMonth(s.Timestamp)*24*60*60, tools.GetDaysInMonth(s.Timestamp))
	case Year:
		s.SLA = s.calAnySLABySLA(tools.GetDaysInYear(s.Timestamp)*24*60*60, tools.GetDaysInYear(s.Timestamp))
	default:

	}
	return s
}

func (s *sla) SetID() (SLA, error) {
	if s.Timestamp != 0 && s.Type != "" && s.Province != "" && s.ReMark != "" {
		s.ID = tools.CalculateSHA1(strconv.FormatInt(s.Timestamp, 10) + s.Province + s.ReMark + s.Type)
		return s, nil
	}
	return s, errors.New("关键字段不能为空！")
}

func (s *sla) Write() error {
	logger := zap.L()
	if s.SLA >= 0 {
		filter := map[string]interface{}{
			"_id": s.ID,
		}

		err := s.mongo.Table(parameters.MongoCollectionMetadataDisplayStand).Upsert(s.ctx, filter, s)
		if err != nil {
			logger.Error("写入mongodb失败, " + err.Error())
			return err
		}
		logger.Debug("SLA写入正常。" + s.Province + ", " + s.ReMark + ", " + s.Type + ", " +
			strconv.FormatInt(s.Timestamp, 10) + ", " + strconv.FormatFloat(s.SLA, 'f', -1, 64))
	} else {
		logger.Debug("数据为空，无需插入数据库，" + s.Province + ", " + s.ReMark + ", " + s.Type + ", " +
			strconv.FormatInt(s.Timestamp, 10) + ", " + strconv.FormatFloat(s.SLA, 'f', -1, 64))
	}
	return nil
}

func CreateSLA(province string, reMark string, p *parameters.Parameters, t int64, types string, m *mongodb.Mongo) SLA {
	return &sla{
		Province:   province,
		ReMark:     reMark,
		parameters: p,
		Timestamp:  t,
		Type:       types,
		mongo:      m,
	}
}

func CreateBasicSLA(p *parameters.Parameters, m *mongodb.Mongo) SLA {
	return &sla{
		parameters: p,
		mongo:      m,
	}
}

// 忘了干啥的了，先注释点看看情况，另外这段代码可能有问题，会产生重复数据，必要的话回退到2024年7月30号之前的版本，这段代码是在8月1号-8月2号修改的
//func mergeMeta[T metaData.CommonMethods](m []T, getKey func(T) string) []T {
//	// 使用两个map来进行合并
//	regexMap := make(map[string]T)
//	urlMap := make(map[string]map[string]metaData.URLDetail)
//	ipAddressMap := make(map[string]string)
//
//	for _, meta := range m {
//		key := getKey(meta)
//		if _, exists := regexMap[key]; !exists {
//			regexMap[key] = meta
//			urlMap[key] = make(map[string]metaData.URLDetail)
//		}
//
//		if metaSMS, ok := any(meta).(metaData.MetaSMSData); ok {
//			ipAddressMap[key] = metaSMS.GetIpAddress()
//		}
//
//		for _, urlDetail := range meta.GetBase().URLs {
//			if existingURLDetail, exists := urlMap[key][urlDetail.URL]; exists {
//				existingURLDetail.RtAll += urlDetail.RtAll
//				existingURLDetail.ErLines += urlDetail.ErLines
//				existingURLDetail.ErrorLines += urlDetail.ErrorLines
//				existingURLDetail.BytesAll += urlDetail.BytesAll
//				existingURLDetail.NormalLines += urlDetail.NormalLines
//				existingURLDetail.NormalCount += urlDetail.NormalCount
//				existingURLDetail.AllLines += urlDetail.AllLines
//
//				if existingURLDetail.ActionLines == nil {
//					existingURLDetail.ActionLines = make(map[string]float64)
//				}
//				for action, value := range urlDetail.ActionLines {
//					existingURLDetail.ActionLines[action] += value
//				}
//
//				urlMap[key][urlDetail.URL] = existingURLDetail
//			} else {
//				urlMap[key][urlDetail.URL] = urlDetail
//			}
//		}
//	}
//
//	// 将合并结果放入最终的Meta数组中
//	var mergedMeta []T
//	for key, meta := range regexMap {
//		var mergedURLs []metaData.URLDetail
//		for _, urlDetail := range urlMap[key] {
//			mergedURLs = append(mergedURLs, urlDetail)
//		}
//		base := meta.GetBase()
//		base.URLs = mergedURLs
//		meta.SetBase(base)
//
//		if metaSMS, ok := any(meta).(metaData.MetaSMSData); ok {
//			metaSMS.SetIpAddress(ipAddressMap[key])
//		}
//
//		mergedMeta = append(mergedMeta, meta)
//	}
//	return mergedMeta
//
//}

//func mergeMetaArray(m []*metaData.Meta) []*metaData.Meta {
//	return mergeMeta(m,
//		func(meta *metaData.Meta) string { return meta.GetBase().Regex },
//	)
//}
//
//func mergeMetaSMSArray(m []*metaData.MetaSMS) []*metaData.MetaSMS {
//	return mergeMeta(m,
//		func(meta *metaData.MetaSMS) string { return fmt.Sprintf("%s-%s", meta.IpAddress, meta.GetBase().Regex) },
//	)
//}

func calculateRatio(min, max, current float64) float64 {
	logger := zap.L()
	// min max 为阈值范围，current 为当前值，返回为 min 到 max 之间的位置比例
	if current <= min {
		logger.Debug("当前值小于或等于最小阈值", zap.Float64("当前值", current), zap.Float64("最小阈值", min))
		return 0
	} else if current >= max {
		logger.Debug("当前值大于或等于最大阈值", zap.Float64("当前值", current), zap.Float64("最大阈值", max))
		return 1
	} else {
		// 计算比例
		ratio := (current - min) / (max - min)
		logger.Debug("计算位置比例", zap.Float64("当前值", current), zap.Float64("最小阈值", min), zap.Float64("最大阈值", max), zap.Float64("位置比例", ratio))
		return ratio
	}
}

func (s *sla) GetSLAByTime(from int64, to int64, t string) ([]sla, error) {
	logger := zap.L()
	filter := map[string]interface{}{
		"type": t,
		"timestamp": map[string]interface{}{
			"$gte": from,
			"$lte": to,
		},
	}
	logger.Debug("执行 GetSLAByTime 方法", zap.Any("查询条件", filter))

	var result []sla
	b := true
	f := s.mongo.Table(parameters.MongoCollectionMetadataDisplayStand).Find(filter)
	f.Option(&types.FindOpts{WithObjectID: &b})
	err := f.All(context.Background(), &result)

	if err != nil {
		// 使用 logger 记录 error 日志
		logger.Error("查询数据库时发生错误", zap.Error(err))
		return nil, err
	}
	// 剔除不匹配的省份
	var slas []sla
	reg := tools.InitRegexpCache(metaData.ExtractUrls(s.parameters.Urls))
	for _, r := range result {
		meta := metaData.CreateMeta(s.mongo)

		meta.InitByEntry(metaData.MessageEntry{Province: r.Province, ReMark: r.ReMark, URL: s.parameters.Urls[0].Url, Timestamp: time.Now()}).UpdateParameters(s.parameters)
		meta.UpdateReCache(reg)
		if meta.InList() {
			slas = append(slas, r)
		}
	}

	return slas, nil
}

func (s *sla) GetSLAByProvince(province string, remark string, from int64, to int64, types string) (interface{}, error) {
	logger := zap.L()
	var filter map[string]interface{}

	if province == "" {
		filter = map[string]interface{}{
			"type": types,
			"timestamp": map[string]interface{}{
				"$gte": from,
				"$lt":  to,
			},
		}
	} else {
		filter = map[string]interface{}{
			"province": province,
			"reMark":   remark,
			"type":     types,
			"timestamp": map[string]interface{}{
				"$gte": from,
				"$lt":  to,
			},
		}
	}

	logger.Debug("执行 GetSLAByProvince 方法", zap.Any("查询条件", filter))

	var result []sla
	err := s.mongo.Table(parameters.MongoCollectionMetadataDisplayStand).Find(filter).All(s.ctx, &result)
	if err != nil {
		logger.Error("查询数据库时发生错误", zap.Error(err))
		return nil, err
	}
	return result, nil
}

func (s *sla) GetDetailByRisk(province string, remark string, from int64, to int64, top int, collectionName string) ([]Detail, error) {
	logger := zap.L()
	start := time.Now()
	defer func() {
		logger.Info("GetDetailByTime总耗时", zap.Any("during", time.Since(start)))
	}()

	logger.Info("开始执行GetDetailByRisk...：")
	logger.Debug("调用 GetDetailByRisk 函数",
		zap.String("province", province),
		zap.String("remark", remark),
		zap.Int64("from", from),
		zap.Int64("to", to),
		zap.String("collectionName", collectionName),
	)

	var filter map[string]interface{}
	//for testing
	//from = 1723010400
	//to = 1723011000
	if province == "" {
		filter = map[string]interface{}{
			"timestamp": map[string]interface{}{
				"$gte": from,
				"$lt":  to,
			},
		}
	} else {
		filter = map[string]interface{}{
			"province": province,
			"reMark":   remark,
			"timestamp": map[string]interface{}{
				"$gte": from,
				"$lt":  to,
			},
		}
	}

	var result interface{}
	switch collectionName {
	case parameters.MongoCollectionMetadata:
		result = []metaData.Meta{}
	case parameters.MongoCollectionMetadataSms:
		result = []metaData.MetaSMS{}
	default:
		return nil, fmt.Errorf("未知的名称: %s", collectionName)
	}

	b := true
	f := s.mongo.Table(collectionName).Find(filter)
	f.Option(&types.FindOpts{WithObjectID: &b})
	err := f.All(s.ctx, &result)
	if err != nil {
		logger.Debug("在查询数据库时发生错误", zap.Error(err))
		return nil, err
	}
	logger.Info("GetDetailByRisk完成数据库查询，查询条件：", zap.Any("filter", filter))

	data := make([]Detail, 0)
	weights := make(map[WeightKey]float64)
	provinces := make([]parameters.Province, 0)

	if province == "" {
		provinces = s.parameters.Provinces
	} else {
		provinces = append(provinces, parameters.Province{
			Province: province,
			Remark:   remark,
		})
	}
	// 并发处理省份数据
	var wg sync.WaitGroup
	var mu sync.Mutex

	// 补齐数据
	for _, p := range provinces {
		wg.Add(1)

		pCopy := p
		go func(prov parameters.Province) {
			defer wg.Done()
			var tempData []Detail
			var tempWeights map[WeightKey]float64

			if collectionName == parameters.MongoCollectionMetadata {
				tempMeta := s.addToMetaIfNotExists(result.([]metaData.Meta), []parameters.Province{prov}, from, to)
				tempData, tempWeights = s.processMetaResults(tempMeta, collectionName)
			} else if collectionName == parameters.MongoCollectionMetadataSms {
				tempMeta := s.addToMetaSMSIfNotExists(result.([]metaData.MetaSMS), []parameters.Province{prov}, from, to)
				tempData, tempWeights = s.processMetaResults(tempMeta, collectionName)
			}

			mu.Lock()
			data = append(data, tempData...)
			weights = mergeWeights(weights, tempWeights)
			mu.Unlock()
		}(pCopy)
	}

	wg.Wait()
	return s.getTopResults(data, top, weights), nil
}

func mergeWeights(map1, map2 map[WeightKey]float64) map[WeightKey]float64 {
	merged := make(map[WeightKey]float64)

	for k, v := range map1 {
		merged[k] = v
	}

	for k, v := range map2 {
		if existingValue, found := merged[k]; found {
			merged[k] = existingValue + v
		} else {
			merged[k] = v
		}
	}

	return merged
}

func (s *sla) processMetaResults(result interface{}, collectionName string) ([]Detail, map[WeightKey]float64) {

	var data []Detail
	weights := make(map[WeightKey]float64)
	reg := tools.InitRegexpCache(metaData.ExtractUrls(s.parameters.Urls))

	switch collectionName {
	case parameters.MongoCollectionMetadata:
		//var c []*metaData.Meta
		//value, ok := result.([]*metaData.Meta)
		//if ok {
		//	c = mergeMetaArray(value)
		//} else {
		//	var metaPointerSlice []*metaData.Meta
		//	for i := range result.([]metaData.Meta) {
		//		metaPointerSlice = append(metaPointerSlice, &result.([]metaData.Meta)[i])
		//	}
		//	c = mergeMetaArray(metaPointerSlice)
		//}

		for _, meta := range result.([]metaData.Meta) {
			data, weights = s.processMeta(meta, data, reg, weights)
		}

		//for _, meta := range c {
		//	data, weights = s.processMeta(*meta, data, reg, weights)
		//}

	case parameters.MongoCollectionMetadataSms:
		//var c []*metaData.MetaSMS
		//value, ok := result.([]*metaData.MetaSMS)
		//if ok {
		//	c = mergeMetaSMSArray(value)
		//} else {
		//	var metaSMSPointerSlice []*metaData.MetaSMS
		//	for i := range result.([]metaData.MetaSMS) {
		//		metaSMSPointerSlice = append(metaSMSPointerSlice, &result.([]metaData.MetaSMS)[i])
		//	}
		//	c = mergeMetaSMSArray(metaSMSPointerSlice)
		//}
		//
		//for _, meta := range c {
		//	data, weights = s.processMetaSMS(*meta, data, reg, weights)
		//}
		for _, meta := range result.([]metaData.MetaSMS) {
			data, weights = s.processMetaSMS(meta, data, reg, weights)
		}
	}

	return data, weights
}

func (s *sla) processMeta(meta metaData.Meta, data []Detail, reg []*regexp.Regexp, weights map[WeightKey]float64) ([]Detail, map[WeightKey]float64) {
	// 处理 Meta 逻辑
	uuid := tools.GetUUID()
	logger := zap.L()
	logger.Debug("处理 Meta 数据", zap.String("uuid", uuid), zap.Any("meta", meta))
	para, m := parameters.MatchUrl(s.parameters.Urls, meta.Province, meta.ReMark, meta.Regex)
	if !m {
		logger.Debug("未找到匹配规则，跳过", zap.String("uuid", uuid), zap.String("province", meta.Province), zap.String("remark", meta.ReMark))
		return data, weights
	}
	meta.UpdateParameters(s.parameters)
	meta.UpdateReCache(reg)
	if !meta.InList() {
		logger.Debug("不在参数清单中，跳过", zap.String("uuid", uuid), zap.String("province", meta.Province), zap.String("remark", meta.ReMark))
		return data, weights
	}
	//这里的赋值在并发下会产生数据错乱的问题
	//s.Province, s.ReMark = meta.Province, meta.ReMark
	//s.Timestamp = meta.Timestamp
	r := s.getAllRatio(para, meta.GetBase(), "0.0.0.0")
	urlMap := createURLMap(meta.GetBase().URLs)
	for key, value := range r {
		data, weights = s.calculateRisk(value, para, &meta, urlMap, key, data, weights,
			func(meta metaData.CommonMethods) string {
				return "0.0.0.0"
			})
	}

	return data, weights
}

func createURLMap(urls []metaData.URLDetail) map[string]bool {
	urlMap := make(map[string]bool)
	for _, urlDetail := range urls {
		urlMap[urlDetail.URL] = urlDetail.Update
	}
	return urlMap
}

type WeightKey struct {
	TimeStamp int64
	Province  string
	ReMark    string
}

func (s *sla) processMetaSMS(meta metaData.MetaSMS, data []Detail, reg []*regexp.Regexp, weights map[WeightKey]float64) ([]Detail, map[WeightKey]float64) {
	// 处理 MetaSMS 逻辑
	uuid := tools.GetUUID()
	logger := zap.L()
	logger.Debug("处理 MetaSMS 数据", zap.String("uuid", uuid), zap.Any("meta", meta))
	para, m := parameters.MatchUrl(s.parameters.Urls, meta.Province, meta.ReMark, meta.Regex)
	if !m {
		logger.Debug("未找到匹配规则，跳过", zap.String("uuid", uuid), zap.String("province", meta.Province), zap.String("remark", meta.ReMark))
		return data, weights
	}
	meta.UpdateParameters(s.parameters)
	meta.UpdateReCache(reg)
	if !meta.InList() {
		logger.Debug("不在参数清单中，跳过", zap.String("uuid", uuid), zap.String("province", meta.Province), zap.String("remark", meta.ReMark))
		return data, weights
	}
	//s.Province, s.ReMark = meta.Province, meta.ReMark
	//s.Timestamp = meta.Timestamp

	r := s.getAllRatio(para, meta.GetBase(), meta.GetIpAddress())
	urlMap := createURLMap(meta.GetBase().URLs)
	for key, value := range r {
		data, weights = s.calculateRisk(value, para, &meta, urlMap, key, data, weights,
			func(meta metaData.CommonMethods) string {
				sms := meta.(*metaData.MetaSMS)
				return sms.GetIpAddress()
			})
	}

	return data, weights
}

func (s *sla) calculateRiskDetails(value ratio, para parameters.Url, meta metaData.CommonMethods, urlMap map[string]bool, key string,
	getIPAddress func(meta metaData.CommonMethods) string, universal parameters.Universal) (Detail, float64, bool) {
	logger := zap.L()
	uuid := tools.GetUUID()
	logger.Debug("计算风险比率", zap.String("uuid", uuid), zap.String("province", meta.GetBase().Province), zap.String("remark", meta.GetBase().ReMark),
		zap.String("url", key),
		zap.Float64("RTratio", value.RTRatio),
		zap.Float64("Eratio", value.ERatio),
		zap.Float64("ELratio", value.ELRatio),
		zap.Float64("Alratio", value.AlRatio),
		zap.Any("Actratio", value.ActRatio),
	)

	var rm Detail
	rm.Province = meta.GetBase().Province
	rm.ReMark = meta.GetBase().ReMark
	rm.URL = key
	rm.TimeStamp = meta.GetBase().Timestamp

	downRisk, downWeight := s.actRatio(value.ActRatio, universal)

	allWeight := universal.WTRate + universal.WPRate + universal.WERate + universal.WARate + downWeight

	rm.ElRisk = (value.ELRatio * universal.WERate / allWeight) * para.Weight * value.PCT
	rm.ErRisk = (value.ERatio * universal.WPRate / allWeight) * para.Weight * value.PCT
	rm.RtRisk = (value.RTRatio * universal.WTRate / allWeight) * para.Weight * value.PCT
	rm.AlRisk = (value.AlRatio * universal.WARate / allWeight) * para.Weight * value.PCT
	rm.ActRisk = (downRisk / allWeight) * para.Weight * value.PCT

	rm.RiskWeight = rm.RtRisk + rm.ErRisk + rm.ElRisk + rm.AlRisk + rm.ActRisk
	rm.IPAddress = getIPAddress(meta)

	if (value.AllLines < parameters.GetMinRPM(meta.GetBase().Province, meta.GetBase().ReMark, meta.GetBase().Regex, *s.parameters) && !urlMap[rm.URL]) || (urlMap[rm.URL] && rm.AlRisk <= 0) {
		//if (value.AllLines < parameters.GetMinRPM(meta.GetBase().Province, meta.GetBase().ReMark, meta.GetBase().Regex, *s.parameters) && !urlMap[rm.URL]) || (urlMap[rm.URL] && rm.AlRisk <= 0) {
		logger.Debug("不满足最小RPM条件，且没有请求量风险，跳过", zap.String("uuid", uuid), zap.Any("meta", meta), zap.Any("rm", rm), zap.Any("value", value))
		return rm, 0, false
	}

	return rm, para.Weight, true
}

func (s *sla) calculateRisk(value ratio, para parameters.Url, meta metaData.CommonMethods, urlMap map[string]bool,
	key string, data []Detail, weights map[WeightKey]float64, getIPAddress func(meta metaData.CommonMethods) string) ([]Detail, map[WeightKey]float64) {
	rm, weight, valid := s.calculateRiskDetails(value, para, meta, urlMap, key, getIPAddress,
		parameters.GetUniversal(meta.GetBase().Province, meta.GetBase().ReMark, *s.parameters))

	if !valid {
		return data, weights
	}
	weightKey := WeightKey{
		TimeStamp: rm.TimeStamp,
		Province:  rm.Province,
		ReMark:    rm.ReMark,
	}

	weights[weightKey] = weights[weightKey] + weight
	data = append(data, rm)

	return data, weights
}

func (s *sla) getTopResults(data []Detail, top int, weights map[WeightKey]float64) []Detail {
	//此处进行权重计算SLAIgnoreThreshold和MinimumChangeUnit评估
	logger := zap.L()
	//bugfix
	//universal := parameters.GetUniversal(s.Province, s.ReMark, *s.parameters)
	allWeight := 0.0
	for _, risk := range data {
		allWeight += risk.RiskWeight
	}

	// 计算SLA影响百分比并获取 top N 结果
	var pureData []Detail

	for _, d := range data {
		if d.RiskWeight <= 0 {
			logger.Debug("风险权重为0，跳过", zap.Any("Detail", d))
			continue
		}
		universal := parameters.GetUniversal(d.Province, d.ReMark, *s.parameters)
		if !s.reserved(d.RiskWeight/weights[WeightKey{Province: d.Province, ReMark: d.ReMark, TimeStamp: d.TimeStamp}], universal) {
			logger.Debug("不满足SLAIgnoreThreshold约束，跳过", zap.Any("Detail", d))
			continue
		}

		d.ElRisk = d.ElRisk / d.RiskWeight
		d.ErRisk = d.ErRisk / d.RiskWeight
		d.RtRisk = d.RtRisk / d.RiskWeight
		d.AlRisk = d.AlRisk / d.RiskWeight
		d.ActRisk = d.ActRisk / d.RiskWeight
		d.RiskWeight = d.RiskWeight / weights[WeightKey{Province: d.Province, ReMark: d.ReMark, TimeStamp: d.TimeStamp}]

		pureData = append(pureData, d)
	}

	regex := tools.InitRegexpCache(metaData.ExtractUrls(s.parameters.Urls))

	for index, d := range pureData {
		x, i, f := tools.FindMatches(regex, d.URL)
		if !f {
			continue
		}
		s.optimize(&pureData[index].RiskWeight, parameters.GetMinimumChangeUnit(d.Province, d.ReMark, x[i], *s.parameters))
	}

	sort.Slice(pureData, func(i, j int) bool {
		if pureData[i].RiskWeight == pureData[j].RiskWeight {
			return pureData[i].URL < pureData[j].URL
		}
		return pureData[i].RiskWeight > pureData[j].RiskWeight
	})

	if top > 0 && len(pureData) > top {
		return pureData[:top]
	}

	return pureData
}

//func generateKey(timestamp int64, province, reMark, url string) string {
//	return fmt.Sprintf("%d_%s_%s_%s", timestamp, province, reMark, url)
//}

func generateKey(timestamp int64, parts ...string) string {
	return fmt.Sprintf("%d_%s", timestamp, strings.Join(parts, "_"))
}

func processMeta[T any](meta []T, from int64, to int64, provinces []parameters.Province,
	generateMeta func(a int64, p parameters.Province, r, u, ip string) (T, bool), param parameters.Parameters) []T {

	if provinces == nil || len(provinces) == 0 {
		return meta
	}

	regex := tools.InitRegexpCache(metaData.ExtractUrls(param.Urls))

	all := make([]int64, 0)
	currentTime := time.Now().Unix()
	//延迟获取数据，此处为180表示去除最近3分钟的数据
	currentTime = currentTime - currentTime%60 + 60 - 120 //180
	if from%60 != 0 {
		from = from - from%60 + 60
	}
	if to%60 != 0 {
		to = to - to%60 + 60
	}
	for i := from; i < to && i <= currentTime; i += 60 {
		all = append(all, i)
	}
	//zap.L().Info("关键值", zap.Any("all", all), zap.Any("currentTime", currentTime))
	metaMap := make(map[string]int) // key是生成的key，value是meta数组的索引
	for index, data := range meta {
		dataValue := reflect.ValueOf(data)
		timestamp := dataValue.FieldByName("Timestamp").Int()
		province := dataValue.FieldByName("Province").String()
		reMark := dataValue.FieldByName("ReMark").String()
		urls := dataValue.FieldByName("URLs").Interface().([]metaData.URLDetail)
		ip := dataValue.FieldByName("IpAddress")
		ipAddress := "0.0.0.0"
		if ip.IsValid() {
			ipAddress = ip.String()
		}
		for _, url := range urls {
			s, i, f := tools.FindMatches(regex, url.URL)
			if !f {
				continue
			}
			key := generateKey(timestamp, province, reMark, s[i], ipAddress)
			metaMap[key] = index
		}
	}
	provinceMap := make(map[parameters.Province]bool)
	for _, p := range provinces {
		provinceMap[p] = true
	}

	refRegex := make(map[string]string)
	for _, data := range param.Refs {
		//key := generateKey(0, data.Province, data.ReMark, data.Regex, data.IPAddress)
		if data.IPAddress == "" {
			data.IPAddress = "0.0.0.0"
		}
		keyUrl := generateKey(0, data.Province, data.ReMark, data.Url, data.IPAddress)
		//refMap[key] = true
		refRegex[keyUrl] = data.Regex
	}

	for _, data := range param.Refs {
		provinceKey := parameters.Province{
			Province: data.Province,
			Remark:   data.ReMark,
		}
		if !provinceMap[provinceKey] {
			continue
		}
		for _, a := range all {
			s, i, f := tools.FindMatches(regex, data.Url)
			if !f {
				continue
			}
			key := generateKey(a, data.Province, data.ReMark, s[i], data.IPAddress)
			refUrlKey := generateKey(0, data.Province, data.ReMark, data.Url, data.IPAddress)
			if index, existsMeta := metaMap[key]; existsMeta {
				// 修改meta切片中的数据
				dataValue := reflect.ValueOf(&meta[index]).Elem()
				urls := dataValue.FieldByName("URLs").Interface().([]metaData.URLDetail)
				find := false

				for _, mUrl := range urls {
					if mUrl.URL == data.Url {
						find = true
						break
					}
				}
				if !find {
					urls = append(urls, metaData.URLDetail{URL: data.Url, Update: true})

					urlField := dataValue.FieldByName("URLs")
					urlField.Set(reflect.ValueOf(urls))
				}
			} else {
				// 生成新数据
				newMeta, success := generateMeta(a, parameters.Province{
					Province: data.Province,
					Remark:   data.ReMark,
				}, refRegex[refUrlKey], data.Url, data.IPAddress)
				if success {
					meta = append(meta, newMeta)
					//metaMap[key] = idx
					metaMap[key] = len(meta) - 1
				}
			}
		}
	}

	//var timestamp []int64
	//for x, _ := range meta {
	//	dataValue := reflect.ValueOf(&meta[x]).Elem()
	//	timestamp = append(timestamp, dataValue.FieldByName("Timestamp").Int())
	//}
	//seen := make(map[int64]bool)
	//result := make([]int64, 0, len(timestamp))
	//
	//for _, t := range timestamp {
	//	if _, found := seen[t]; !found {
	//		seen[t] = true
	//		result = append(result, t)
	//	}
	//}
	//
	//zap.L().Info("去重后", zap.Any("result", result))

	//metaMap := make(map[string]int) // key是生成的key，value是meta数组的索引
	//for index, data := range meta {
	//	dataValue := reflect.ValueOf(data)
	//	timestamp := dataValue.FieldByName("Timestamp").Int()
	//	province := dataValue.FieldByName("Province").String()
	//	reMark := dataValue.FieldByName("ReMark").String()
	//	urls := dataValue.FieldByName("URLs").Interface().([]metaData.URLDetail)
	//	ip := dataValue.FieldByName("IpAddress")
	//	ipAddress := "0.0.0.0"
	//	if ip.IsValid() {
	//		ipAddress = ip.String()
	//	}
	//	for _, url := range urls {
	//		s, i, f := tools.FindMatches(regex, url.URL)
	//		if !f {
	//			continue
	//		}
	//		key := generateKey(timestamp, province, reMark, s[i], ipAddress)
	//		metaMap[key] = index
	//	}
	//}
	//
	//uniqueUrls := make(map[string]bool)
	//uniqueIPs := make(map[string]bool)
	////refMap := make(map[string]bool)
	//refRegex := make(map[string]string)
	//refIPUrl := make(map[string]bool)
	//for _, data := range param.Refs {
	//	//key := generateKey(0, data.Province, data.ReMark, data.Regex, data.IPAddress)
	//	if data.IPAddress == "" {
	//		data.IPAddress = "0.0.0.0"
	//	}
	//	keyUrl := generateKey(0, data.Province, data.ReMark, data.Url, data.IPAddress)
	//	//refMap[key] = true
	//	uniqueIPs[data.IPAddress] = true
	//	uniqueUrls[data.Url] = true
	//	refIPUrl[keyUrl] = true
	//	refRegex[keyUrl] = data.Regex
	//}
	//
	////zap.L().Info("数据量：", zap.Any("provinces", len(provinces)), zap.Any("all", len(all)), zap.Any("uniqueUrls", len(uniqueUrls)), zap.Any("uniqueIPs", len(uniqueIPs)))
	//
	//for _, p := range provinces {
	//	for _, a := range all {
	//		for u := range uniqueUrls {
	//			for ip := range uniqueIPs {
	//				s, i, f := tools.FindMatches(regex, u)
	//				if !f {
	//					continue
	//				}
	//				//refKey := generateKey(0, p.Province, p.Remark, s[i], ip)
	//				refUrlKey := generateKey(0, p.Province, p.Remark, u, ip)
	//				//_, existsKey := refMap[refKey]
	//				_, existsUrlKey := refIPUrl[refUrlKey]
	//				if existsUrlKey {
	//
	//					existingMetaKey := generateKey(a, p.Province, p.Remark, s[i], ip)
	//					if index, existsMeta := metaMap[existingMetaKey]; existsMeta {
	//						// 修改meta切片中的数据
	//						dataValue := reflect.ValueOf(&meta[index]).Elem()
	//						urls := dataValue.FieldByName("URLs").Interface().([]metaData.URLDetail)
	//						find := false
	//
	//						for _, mUrl := range urls {
	//							if mUrl.URL == u {
	//								find = true
	//								break
	//							}
	//						}
	//						if !find {
	//							urls = append(urls, metaData.URLDetail{URL: u, Update: true})
	//
	//							urlField := dataValue.FieldByName("URLs")
	//							urlField.Set(reflect.ValueOf(urls))
	//						}
	//
	//					} else {
	//						// 生成新数据
	//
	//						newMeta, success := generateMeta(a, p, refRegex[refUrlKey], u, ip)
	//						if success {
	//							meta = append(meta, newMeta)
	//							metaMap[existingMetaKey] = len(meta) - 1
	//						}
	//					}
	//				}
	//			}
	//		}
	//	}
	//}

	return meta
}
func filterMeta(meta []metaData.Meta, provinces []parameters.Province) []metaData.Meta {
	cache := make([]metaData.Meta, 0)
	for _, province := range provinces {
		for _, m := range meta {
			if m.Province == province.Province && m.ReMark == province.Remark {
				cache = append(cache, m)
			}
		}
	}
	return cache
}

func filterMetaSMS(meta []metaData.MetaSMS, provinces []parameters.Province) []metaData.MetaSMS {
	cache := make([]metaData.MetaSMS, 0)
	for _, province := range provinces {
		for _, m := range meta {
			if m.Province == province.Province && m.ReMark == province.Remark {
				cache = append(cache, m)
			}
		}
	}
	return cache
}
func (s *sla) addToMetaSMSIfNotExists(meta []metaData.MetaSMS, provinces []parameters.Province, from int64, to int64) []metaData.MetaSMS {
	logger := zap.L()
	//start := time.Now()
	//defer func() {
	//	logger.Info("addToMetaSMSIfNotExists总耗时", zap.Any("during", time.Since(start)))
	//}()
	return processMeta(filterMetaSMS(meta, provinces), from, to, parameters.GetProvincesByType(provinces, parameters.SLATypeSMS), func(a int64, p parameters.Province, r, u, ip string) (metaData.MetaSMS, bool) {
		if p.Province != parameters.SLATypeSMS {
			return metaData.MetaSMS{}, false
		}
		newMeta := metaData.MetaSMS{
			IpAddress: ip,
			MetaBase: metaData.MetaBase{
				Timestamp: a,
				Province:  p.Province,
				ReMark:    p.Remark,
				Regex:     r,
				URLs: []metaData.URLDetail{
					{
						URL:    u,
						Update: true,
					},
				},
			},
		}
		err := newMeta.SetID()
		if err != nil {
			logger.Debug("Meta setID失败:" + err.Error())
			return metaData.MetaSMS{}, false
		}
		logger.Debug("数据补齐：", zap.Any("Data", newMeta))
		return newMeta, true
	}, *s.parameters)
}

// Specific function for Meta
func (s *sla) addToMetaIfNotExists(meta []metaData.Meta, provinces []parameters.Province, from int64, to int64) []metaData.Meta {
	logger := zap.L()
	//start := time.Now()
	//defer func() {
	//	logger.Info("addToMetaIfNotExists总耗时", zap.Any("provinces", provinces), zap.Any("during", time.Since(start)))
	//}()
	return processMeta(filterMeta(meta, provinces), from, to, parameters.GetProvincesByType(provinces, parameters.SLATypeDefault), func(a int64, p parameters.Province, r, u, ip string) (metaData.Meta, bool) {
		if p.Province == parameters.SLATypeSMS {
			return metaData.Meta{}, false
		}
		newMeta := metaData.Meta{
			MetaBase: metaData.MetaBase{
				Timestamp: a,
				Province:  p.Province,
				ReMark:    p.Remark,
				Regex:     r,
				URLs: []metaData.URLDetail{
					{
						URL:    u,
						Update: true,
					},
				},
			},
		}
		err := newMeta.SetID()
		if err != nil {
			logger.Debug("Meta setID失败:" + err.Error())
			return metaData.Meta{}, false
		}
		logger.Debug("数据补齐：", zap.Any("Data", newMeta))
		return newMeta, true
	}, *s.parameters)
}

func (s *sla) actRatio(ActRatio map[string]float64, universal parameters.Universal) (downRisk, downWeight float64) {
	logger := zap.L()

	for k, cv := range ActRatio {
		downRisk += cv * universal.ACTRate[k]
		// 避免降级权重过低，导致未能正确反应在SLA指标上，或影响太小，也避免过多的降级权重影响其他SLA权重指标
		if downRisk > 0 {
			downWeight += universal.ACTRate[k]
		}
		logger.Debug("计算降级风险和权重", zap.String("指标", k), zap.Float64("当前值", cv), zap.Float64("权重", universal.ACTRate[k]))
	}
	return downRisk, downWeight
}

//func (s *sla) calGenericSLA(m []metaData.CommonMethods, getIPAddress func(meta metaData.CommonMethods) string) float64 {
//	logger := zap.L()
//
//	var allWeight float64
//	var riskUrl float64
//	var risks []float64
//	universal := parameters.GetUniversal(s.Province, s.ReMark, *s.parameters)
//	for _, v := range m {
//		logger.Debug("处理 calMid 数据", zap.Any("calMid", v))
//
//		para, match := parameters.MatchUrl(s.parameters.Urls, s.Province, s.ReMark, v.GetBase().Regex)
//		if !match {
//			continue
//		}
//
//		r := s.getAllRatio(para, v.GetBase(), getIPAddress(v))
//		urlMap := createURLMap(v.GetBase().URLs)
//
//		for key, value := range r {
//
//			rm, weight, valid := s.calculateRiskDetails(value, para, v, urlMap, key, getIPAddress, universal)
//
//			if !valid {
//				continue
//			}
//
//			risk := rm.RiskWeight
//			risks = append(risks, risk)
//			allWeight += weight
//		}
//	}
//
//	//此处进行权重计算SLAIgnoreThreshold和MinimumChangeUnit评估
//
//	for _, risk := range risks {
//		if s.reserved(risk/allWeight, universal) {
//			riskUrl += risk
//		}
//	}
//
//	returnVal := 1 - riskUrl/allWeight
//	if returnVal < 0 {
//		returnVal = 0
//	}
//	return returnVal
//}

//func (s *sla) calSLA(m []*metaData.Meta) float64 {
//	metaSlice := make([]metaData.CommonMethods, len(m))
//	for i, v := range m {
//		metaSlice[i] = v
//	}
//	return s.calGenericSLA(metaSlice,
//		func(meta metaData.CommonMethods) string {
//			return "0.0.0.0"
//		})
//}
//
//func (s *sla) calSMSSLA(m []*metaData.MetaSMS) float64 {
//	metaSMSSlice := make([]metaData.CommonMethods, len(m))
//	for i, v := range m {
//		metaSMSSlice[i] = v
//	}
//
//	return s.calGenericSLA(metaSMSSlice,
//		func(meta metaData.CommonMethods) string {
//			sms := meta.(*metaData.MetaSMS)
//			return sms.GetIpAddress()
//		})
//}

type ratio struct {
	RTRatio  float64
	ERatio   float64
	ELRatio  float64
	AlRatio  float64
	ActRatio map[string]float64
	PCT      float64
	AllLines float64
}

func (s *sla) optimize(r *float64, val float64) {
	if *r > 0 && *r < val {
		*r = val
	}
}

func (s *sla) getAllRatio(para parameters.Url, v metaData.MetaBase, ipAddress string) map[string]ratio {
	logger := zap.L()
	result := make(map[string]ratio)
	allLines := 0.0
	for _, urlDetail := range v.URLs {

		ref := parameters.MatchRef(s.parameters.Refs, v.Province, v.ReMark, urlDetail.URL, ipAddress)

		RTRatio := 0.0
		if urlDetail.NormalLines != 0 {
			RTRatio = calculateRatio(para.RTZero, para.RTMax, urlDetail.RtAll*1000/urlDetail.NormalLines)
		}
		ERatio := 0.0
		if urlDetail.AllLines != 0 && urlDetail.ErLines > parameters.GetMinErrCodeRPM(v.Province, v.ReMark, v.Regex, *s.parameters) {
			ERatio = calculateRatio(para.ERZero, 1, urlDetail.ErLines/urlDetail.AllLines)
		}
		ELRatio := calculateRatio(para.ELZero, para.ELMax, urlDetail.ErrorLines)

		ALRatio := 0.0

		x := ref.RealLines / 24 / 60
		xCount := ref.Count
		//首先要保障平均分每种请求大于预设值
		logger.Debug("判断RPM情况", zap.Float64("x", x),
			zap.Float64("minrpm", parameters.GetMinRPM(v.Province, v.ReMark, v.Regex, *s.parameters)),
			zap.Any("urlDetail", urlDetail),
			zap.Any("para", para))

		//if xCount > 24*60*0.2 {
		//	if (x > parameters.GetMinRPM(v.Province, v.ReMark, v.Regex, *s.parameters) && urlDetail.AllLines <= x*para.ALMinPercent && s.parameters.SMSDevice[ipAddress].Kind != "StandBy") ||
		//		(x == 0 && urlDetail.AllLines > parameters.GetMinRPM(v.Province, v.ReMark, v.Regex, *s.parameters) && s.parameters.SMSDevice[ipAddress].Kind == "StandBy") {
		//		logger.Debug("请求量异常检测通过：", zap.Any("原始数据", v), zap.Any("realLines", x), zap.Any("minRPM", parameters.GetMinRPM(v.Province, v.ReMark, v.Regex, *s.parameters)))
		//		ALRatio = 1.0
		//	}
		//}

		if xCount > 24*60*0.2 { //请求记录的点数小于20%，则不进行判断， 该参数暂时没有加入配置项，待优化

			minRPM := parameters.GetMinRPM(v.Province, v.ReMark, v.Regex, *s.parameters)
			isStandBy := s.parameters.SMSDevice[ipAddress].Kind == "StandBy"

			isAboveMinRPM := x > minRPM
			isBelowMinPercent := urlDetail.AllLines <= x*para.ALMinPercent
			isZeroWithHighLines := x == 0 && urlDetail.AllLines > minRPM

			if (isAboveMinRPM && isBelowMinPercent && !isStandBy) || (isZeroWithHighLines && isStandBy) {
				logger.Debug("请求量异常检测通过：",
					zap.Any("原始数据", v),
					zap.Any("realLines", x),
					zap.Any("minRPM", minRPM))
				ALRatio = 1.0
			}
		}

		ActRatio := make(map[string]float64)
		for k, c := range urlDetail.ActionLines {
			min, ok := para.Action[k+"_Min"]
			if !ok {
				min = math.MaxFloat64 - 1
			}
			max, ok := para.Action[k+"_Max"]
			if !ok {
				max = math.MaxFloat64
			}
			ActRatio[k] = calculateRatio(min, max, c)
		}
		r := ratio{
			RTRatio:  RTRatio,
			ERatio:   ERatio,
			ELRatio:  ELRatio,
			AlRatio:  ALRatio,
			ActRatio: ActRatio,
			PCT:      urlDetail.AllLines,
			AllLines: urlDetail.AllLines,
		}
		allLines += urlDetail.AllLines
		result[urlDetail.URL] = r

	}
	// 按请求量计算接口比例，用于分割权重
	for key := range result {
		temp := result[key]
		if temp.PCT <= 0 {
			temp.PCT = 1
		} else {
			temp.PCT /= allLines
		}
		result[key] = temp
	}

	return result
}

func (s *sla) calAnySLAByTimeSMS(t int64) float64 {
	logger := zap.L()
	filter := map[string]interface{}{
		"timestamp": map[string]interface{}{
			"$gte": s.Timestamp,
			"$lt":  s.Timestamp + t,
		},
		"province": s.Province,
		"reMark":   s.ReMark,
	}
	var m []metaData.MetaSMS
	err := s.mongo.Table(parameters.MongoCollectionMetadataSms).Find(filter).All(s.ctx, &m)
	if err != nil {
		logger.Debug("查询数据库时发生错误", zap.Error(err))
		return -1
	}

	p := make([]parameters.Province, 0)
	p = append(p, parameters.Province{
		Province: s.Province,
		Remark:   s.ReMark,
	})
	logger.Debug("result补齐前", zap.Any("m", m))
	m = s.addToMetaSMSIfNotExists(m, p, s.Timestamp, s.Timestamp+t)
	var result []metaData.MetaSMS
	reg := tools.InitRegexpCache(metaData.ExtractUrls(s.parameters.Urls))
	for _, meta := range m {
		// 创建 meta 的副本
		metaCopy := meta
		metaCopy.UpdateParameters(s.parameters)
		metaCopy.UpdateReCache(reg)
		if !metaCopy.InList() {
			logger.Debug("不在参数清单中，跳过", zap.String("province", metaCopy.Province), zap.String("remark", metaCopy.ReMark))
			continue
		}
		// 将副本的地址添加到 result 中
		result = append(result, metaCopy)
	}

	data, weights := s.processMetaResults(result, parameters.MongoCollectionMetadataSms)

	risk := s.getTopResults(data, 0, weights)

	//sort.Slice(data, func(i, j int) bool {
	//	return data[i].RiskWeight > data[j].RiskWeight
	//})

	slaV := 0.0
	for _, detail := range risk {
		slaV += detail.RiskWeight

	}
	if slaV > 1 {
		slaV = 1
	}
	return 1 - slaV

	//var c []*metaData.MetaSMS
	//logger.Debug("result补齐后数据", zap.Any("result", result))
	//c = mergeMetaSMSArray(result)
	//// return s.calSLA(c) / float64(t/60) 目前只有分钟使用改方法t默认就是60，暂时不需要处理
	//logger.Debug("result合并后", zap.Any("c", c))
	//
	//return s.calSMSSLA(c)
}

func (s *sla) calAnySLAByTime(t int64) float64 {
	logger := zap.L()

	filter := map[string]interface{}{
		"timestamp": map[string]interface{}{
			"$gte": s.Timestamp,
			"$lt":  s.Timestamp + t,
		},
		"province": s.Province,
		"reMark":   s.ReMark,
	}
	var m []metaData.Meta
	err := s.mongo.Table(parameters.MongoCollectionMetadata).Find(filter).All(s.ctx, &m)
	if err != nil {
		logger.Debug("查询数据库时发生错误", zap.Error(err))
		return -1
	}

	// 先将数据补齐，因数据延迟，只补齐3分钟前的数据
	//currentTime := time.Now().Unix()
	//if currentTime-s.Timestamp >= 180 {
	p := make([]parameters.Province, 0)
	p = append(p, parameters.Province{
		Province: s.Province,
		Remark:   s.ReMark,
	})
	logger.Debug("result补齐前", zap.Any("m", m))
	m = s.addToMetaIfNotExists(m, p, s.Timestamp, s.Timestamp+t)
	//}
	var result []metaData.Meta
	reg := tools.InitRegexpCache(metaData.ExtractUrls(s.parameters.Urls))
	for _, meta := range m {
		metaCopy := meta
		metaCopy.UpdateParameters(s.parameters)
		metaCopy.UpdateReCache(reg)
		if !metaCopy.InList() {
			logger.Debug("不在参数清单中，跳过", zap.String("province", metaCopy.Province), zap.String("remark", metaCopy.ReMark))
			continue
		}
		result = append(result, metaCopy)
	}

	data, weights := s.processMetaResults(result, parameters.MongoCollectionMetadata)

	risk := s.getTopResults(data, 0, weights)

	//sort.Slice(data, func(i, j int) bool {
	//	return data[i].RiskWeight > data[j].RiskWeight
	//})

	slaV := 0.0
	for _, detail := range risk {
		slaV += detail.RiskWeight

	}
	if slaV > 1 {
		slaV = 1
	}
	return 1 - slaV

	//var c []*metaData.Meta
	//logger.Debug("result补齐后数据", zap.Any("result", result))
	//c = mergeMetaArray(result)
	//// return s.calSLA(c) / float64(t/60) 目前只有分钟使用改方法t默认就是60，暂时不需要处理
	//logger.Debug("result合并后", zap.Any("c", c))
	//return s.calSLA(c)
}

// t为时间戳的增量值，i为计算单位，分钟、小时、天
func (s *sla) calAnySLABySLA(t int64, i int64) float64 {
	logger := zap.L()

	calType := CalType(s.Type)
	if calType == "Any" || calType == "None" {
		return -1
	}
	filter := map[string]interface{}{
		"timestamp": map[string]interface{}{
			"$gte": s.Timestamp,
			"$lt":  s.Timestamp + t,
		},
		"province": s.Province,
		"reMark":   s.ReMark,
		"type":     calType,
	}
	var slas []sla
	err := s.mongo.Table(parameters.MongoCollectionMetadataDisplayStand).Find(filter).All(s.ctx, &slas)
	if err != nil {
		logger.Error("查询数据库时发生错误", zap.Error(err))
		return -1
	}
	// 这里可以除以（t/60）的前提条件是数据补齐，比叡该小时只采集到50个点，有10个点未上报或者时间还未到，则这些点原则上补齐，认为这些点的sla为1，即100%
	return s.calSLABySLA(slas, float64(i)) / float64(i)
}

func (s *sla) calSLABySLA(slas []sla, n float64) float64 {
	slaSum := 0.0
	for _, s := range slas {
		slaSum += s.SLA
		n--
	}
	// 数据补齐
	slaSum += n
	return slaSum
}

func (s *sla) reserved(value float64, universal parameters.Universal) bool {
	return value > universal.SLAIgnoreThreshold
}

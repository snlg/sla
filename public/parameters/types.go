/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/4/10 17:22
*/

package parameters

import "time"

type Parameters struct {
	Urls            []Url
	Provinces       []Province
	Universal       []Universal
	Refs            []Ref
	SMSDevice       map[string]SMSDevice
	minRPMCache     map[string]float64
	minERRCodeCache map[string]float64
	SMSNameList     map[string]string
}

// Url 静态参数
type Url struct {
	Province          string             `json:"Province" bson:"Province"`
	ReMark            string             `json:"ReMark" bson:"ReMark"`
	Url               string             `json:"Url" bson:"Url"`
	RTZero            float64            `json:"RTZero" bson:"RTZero"`
	RTMax             float64            `json:"RTMax" bson:"RTMax"`
	ERZero            float64            `json:"ERZero" bson:"ERZero"`
	ELZero            float64            `json:"ELZero" bson:"ELZero"`
	ELMax             float64            `json:"ELMax" bson:"ELMax"`
	ALMinPercent      float64            `json:"ALMinPercent" bson:"ALMinPercent"`
	Action            map[string]float64 `json:"Action" bson:"Action"`
	MinRPM            float64            `json:"MinRPM" bson:"MinRPM"`
	MinErrCodeRPM     float64            `json:"MinErrCodeRPM" bson:"MinErrCodeRPM"`
	Weight            float64            `json:"Weight" bson:"Weight"`
	EffectStart       int64              `json:"EffectStart" bson:"EffectStart"`
	EffectEnd         int64              `json:"EffectEnd" bson:"EffectEnd"`
	MinimumChangeUnit float64            `json:"MinimumChangeUnit" bson:"MinimumChangeUnit"`
}

// Ref 动态参数
type Ref struct {
	ID        string    `json:"_id" bson:"_id"`
	Province  string    `json:"Province" bson:"Province"`
	ReMark    string    `json:"ReMark" bson:"ReMark"`
	Regex     string    `json:"Regex" bson:"Regex"`
	Url       string    `json:"url" bson:"url"`
	RealLines float64   `json:"RealLines" bson:"RealLines"` //前一日该接口总请求量
	Count     float64   `json:"Count" bson:"Count"`         //前一日该接口总记录数
	IPAddress string    `json:"IPAddress" bson:"IPAddress"`
	UpdateAt  time.Time `json:"UpdateAt" bson:"UpdateAt"`
}

type Province struct {
	Province string `json:"province" bson:"province"`
	Remark   string `json:"remark" bson:"remark"`
}
type Universal struct {
	Province           string             `json:"Province" bson:"Province"`
	ReMark             string             `json:"ReMark" bson:"ReMark"`
	WPRate             float64            `json:"WPRate" bson:"WPRate"`
	WTRate             float64            `json:"WTRate" bson:"WTRate"`
	WERate             float64            `json:"WERate" bson:"WERate"`
	WARate             float64            `json:"WARate" bson:"WARate"`
	ACTRate            map[string]float64 `json:"ACTRate" bson:"ACTRate"`
	MinRPM             float64            `json:"MinRPM" bson:"MinRPM"`
	MinErrCodeRPM      float64            `json:"MinErrCodeRPM" bson:"MinErrCodeRPM"`
	TimeRangeStart     string             `json:"TimeRangeStart" bson:"TimeRangeStart"`
	TimeRangeEnd       string             `json:"TimeRangeEnd" bson:"TimeRangeEnd"`
	SLAIgnoreThreshold float64            `json:"SLAIgnoreThreshold" bson:"SLAIgnoreThreshold"`
	MinimumChangeUnit  float64            `json:"MinimumChangeUnit" bson:"MinimumChangeUnit"`
}

type SMSDevice struct {
	IPAddress  string `json:"IPAddress" bson:"IPAddress"`
	Kind       string `json:"Kind" bson:"Kind"`
	Describe   string `json:"Describe" bson:"Describe"`
	Group      string `json:"Group" bson:"Group"`
	GroupClass string `json:"GroupClass" bson:"GroupClass"`
}

type SMSNameList struct {
	Group   string `json:"Group" bson:"Group"`
	CName   string `json:"cName" bson:"cName"`
	SMSName string `json:"smsName" bson:"smsName"`
}

// 以下常量关联业务逻辑，不可修改

const (
	SLATypeSMS     = "直播源"
	SLATypeDefault = "默认"
)

const (
	LogTypeAccess      = 0
	LogTypeError       = 1
	LogTypeDegradation = 2
	LogTypeHearts      = 9
)

const (
	MongoCollectionMetadata             = "Metadata"
	MongoCollectionMetadataSms          = "SMSMetadata"
	MongoCollectionMetadataSmsNameList  = "SMSNameList"
	MongoCollectionMetadataRef          = "ref_parameters"
	MongoCollectionMetadataUrl          = "url_parameters"
	MongoCollectionMetadataProvinces    = "Provinces"
	MongoCollectionMetadataUniversal    = "universal_parameters"
	MongoCollectionMetadataSmsDevices   = "SMSDevices"
	MongoCollectionMetadataDisplayStand = "DisplayStand"
	MongoCollectionMetadataFilters      = "Metadata_filters"
)

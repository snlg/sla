/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/4/10 17:22
*/

package parameters

import (
	"context"
	"errors"
	"fmt"
	"go.uber.org/zap"
	"reflect"
	"sla/common/storage/mongodb"
	"sla/common/tools"
	"strconv"
	"time"
)

//func GetMinRPM(province, remark, url string, parms Parameters) float64 {
//	// 首先检查URL参数表是否有完全匹配的项
//	r := 0.0
//	para, m := MatchUrl(parms.Urls, province, remark, url)
//	if !m {
//		return parms.Global.MinRPM
//	}
//	r = para.MinRPM
//
//	if r > 0 {
//		return r
//	}
//
//	return parms.Global.MinRPM
//
//}

func getMinValue(province, remark, url string, getMinField func(url2 Url) float64, globalMin float64, parms Parameters) float64 {
	logger := zap.L()

	// 首先检查 URL 参数表是否有完全匹配的项
	para, matched := MatchUrl(parms.Urls, province, remark, url)
	if !matched {
		logger.Debug("未找到完全匹配的 URL 参数项，使用全局最小值", zap.String("Province", province), zap.String("Remark", remark), zap.String("URL", url))
		return globalMin
	}

	minValue := getMinField(para)
	if minValue > 0 {
		logger.Debug("找到匹配的 URL 参数项，使用其指定的最小值", zap.String("Province", province), zap.String("Remark", remark), zap.String("URL", url), zap.Float64("最小值", minValue))
		return minValue
	}

	logger.Debug("找到匹配的 URL 参数项，但其指定的最小值无效，使用全局最小值", zap.String("Province", province), zap.String("Remark", remark), zap.String("URL", url))
	return globalMin
}

func GetUniversal(province, remark string, parms Parameters) Universal {
	universal := Universal{}
	level := 0
	for _, u := range parms.Universal {
		if u.Province == "ALL" && u.ReMark == "ALL" && level == 0 {
			universal = u
		}
		if u.Province == "" && level < 1 {
			universal = u
		} else if u.ReMark == "" && province == u.Province {
			if level < 2 {
				universal = u
				level = 1
			}
		} else if province == u.Province && remark == u.ReMark {
			universal = u
			break
		}
	}
	return universal
}

func GetMinRPM(province, remark, url string, parms Parameters) float64 {
	return parms.minRPMCache[GetMinRPMKey(province, remark, url)]
	//return getMinValue(province, remark, url,
	//	func(p Url) float64 { return p.MinRPM },
	//	GetUniversal(province, remark, parms).MinRPM, parms)
}

func GetMinErrCodeRPM(province, remark, url string, parms Parameters) float64 {
	return parms.minERRCodeCache[GetMinRPMKey(province, remark, url)]
	//return getMinValue(province, remark, url,
	//	func(p Url) float64 { return p.MinErrCodeRPM },
	//	GetUniversal(province, remark, parms).MinRPM, parms)
}

func GetMinimumChangeUnit(province, remark, url string, parms Parameters) float64 {
	return getMinValue(province, remark, url,
		func(p Url) float64 { return p.MinimumChangeUnit },
		GetUniversal(province, remark, parms).MinimumChangeUnit, parms)
}

func RefreshRefs(mongo *mongodb.Mongo, parameters Parameters) error {
	t := time.Now()
	to := time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, t.Location()).Unix()
	from := to - 24*60*60
	filter := map[string]interface{}{
		"timestamp": map[string]interface{}{
			"$gte": from,
			"$lt":  to,
		},
	}

	pipeline := []map[string]interface{}{
		{"$match": filter},
		{"$unwind": "$urls"},
		{"$group": map[string]interface{}{
			"_id": map[string]interface{}{
				"Province":  "$province",
				"ReMark":    "$reMark",
				"Url":       "$urls.url",
				"Regex":     "$regex",
				"IPAddress": "$ipAddress",
			},
			"RealLines": map[string]interface{}{"$sum": "$urls.normal_lines"},
			"Count":     map[string]interface{}{"$sum": 1},
		}},
	}

	zap.L().Debug("刷新Ref语句：", zap.Any("pipeline", pipeline))

	var results, resultsMeta, resultsSMSMeta []map[string]interface{}
	if err := mongo.Table(MongoCollectionMetadata).AggregateAll(context.Background(), pipeline, &resultsMeta); err != nil {
		return err
	}
	zap.L().Debug("Metadata原始数据量：", zap.Any("len(resultsMeta)", len(resultsMeta)))
	if err := mongo.Table(MongoCollectionMetadataSms).AggregateAll(context.Background(), pipeline, &resultsSMSMeta); err != nil {
		return err
	}
	zap.L().Debug("SMSMetadata原始数据量：", zap.Any("len(resultsSMSMeta)", len(resultsSMSMeta)))

	results = append(results, resultsMeta...)
	results = append(results, resultsSMSMeta...)

	//uniqueUrls := make(map[string]bool)
	//uniqueUrlsMap := make(map[string]string)
	//for _, url := range parameters.Urls {
	//	uniqueUrls[url.Url] = true
	//	uniqueUrlsMap[url.Url] = url.Province
	//}
	//
	//uniqueIPs := make(map[string]bool)
	//for _, ip := range parameters.SMSDevice {
	//	uniqueIPs[ip.IPAddress] = true
	//}

	//existMap := make(map[string]bool)

	//zap.L().Debug("Map", zap.Any("uniqueUrls", uniqueUrls), zap.Any("uniqueIPs", uniqueIPs), zap.Any("provinces", parameters.Provinces), zap.Any("uniqueUrlsMap", uniqueUrlsMap))

	//var requests []interface{}
	//for _, p := range parameters.Provinces {
	//	for url := range uniqueUrls {
	//		zap.L().Debug("判断条件", zap.Any("p.Province", p.Province), zap.Any("uniqueUrlsMap[url]", uniqueUrlsMap[url]), zap.Any("url", url))
	//		if p.Province == SLATypeSMS && uniqueUrlsMap[url] == SLATypeSMS {
	//			requests = append(requests, createRequests(p, url, uniqueIPs, resultsSMSMeta, &existMap)...)
	//		} else {
	//			if uniqueUrlsMap[url] == SLATypeSMS {
	//				continue
	//			}
	//			requests = append(requests, createRequests(p, url, map[string]bool{"0.0.0.0": true}, resultsMeta, &existMap)...)
	//		}
	//	}
	//}
	requests := createRequests(results, parameters)

	if len(requests) > 0 {
		i, err := mongo.Table(MongoCollectionMetadataRef).BulkWrite(context.Background(), requests)
		if err != nil {
			return err
		}
		zap.L().Info("更新成功，更新数量：" + strconv.Itoa(int(i)))
	} else {
		zap.L().Info("未更新任何数据。")
	}
	return nil
}

func createRequests(resultsMeta []map[string]interface{}, parameters Parameters) []interface{} {
	var requests []interface{}
	for _, res := range resultsMeta {
		ref := buildRef(res)
		ref.RealLines = getRealLines(res)
		ref.Count = getCount(res)
		if _, b := MatchUrl(parameters.Urls, ref.Province, ref.ReMark, ref.Regex); !b {
			continue
		}
		ref, err := setID(ref)

		if err != nil {
			continue
		}
		request := map[string]interface{}{
			"filter": map[string]interface{}{"_id": ref.ID},
			"update": map[string]interface{}{"$set": ref},
			"upsert": true,
		}
		requests = append(requests, request)
	}
	return requests
}

// 已废弃
//func createRequests(p Province, url string, uniqueIPs map[string]bool, resultsMeta []map[string]interface{}, existMap *map[string]bool) []interface{} {
//
//	var requests []interface{}
//	for ip := range uniqueIPs {
//		var ref Ref
//		for _, res := range resultsMeta {
//			ref = buildRef(res)
//			if p.Province == ref.Province && p.Remark == ref.ReMark && url == ref.Regex && ip == ref.IPAddress {
//				ref.RealLines = getRealLines(res)
//				//break
//				ref, err := setID(ref)
//				if err != nil {
//					continue
//				}
//				if _, ok := (*existMap)[ref.ID]; ok {
//					continue
//				}
//				(*existMap)[ref.ID] = true
//				request := map[string]interface{}{
//					"filter": map[string]interface{}{"_id": ref.ID},
//					"update": map[string]interface{}{"$set": ref},
//					"upsert": true,
//				}
//				requests = append(requests, request)
//			} else {
//			}
//
//		}
//	}
//	return requests
//}

func buildRef(res map[string]interface{}) Ref {
	var ipAddress string
	if ip, ok := res["_id"].(map[string]interface{})["IPAddress"]; ok {
		ipAddress = ip.(string)
	} else {
		ipAddress = "0.0.0.0"
	}

	return Ref{
		Province:  res["_id"].(map[string]interface{})["Province"].(string),
		ReMark:    res["_id"].(map[string]interface{})["ReMark"].(string),
		Url:       res["_id"].(map[string]interface{})["Url"].(string),
		Regex:     res["_id"].(map[string]interface{})["Regex"].(string),
		IPAddress: ipAddress,
		UpdateAt:  time.Now(),
	}
}

func getRealLines(res map[string]interface{}) float64 {
	if val, ok := res["RealLines"].(int32); ok {
		return float64(val)
	} else if val, ok := res["RealLines"].(float64); ok {
		return val
	}
	return 0.0
}

func getCount(res map[string]interface{}) float64 {
	if val, ok := res["Count"].(int32); ok {
		return float64(val)
	} else if val, ok := res["Count"].(float64); ok {
		return val
	}
	return 0.0
}

func setID(r Ref) (Ref, error) {
	if r.Province != "" && r.ReMark != "" && r.Regex != "" {
		r.ID = tools.CalculateSHA1(r.Province + r.ReMark + r.Regex + r.Url + r.IPAddress)
		return r, nil
	}
	return r, errors.New("关键字段不能为空！")
}

func loadConfigFromDB(mongo *mongodb.Mongo) (Parameters, error) {
	var urls []Url
	err := mongo.Table(MongoCollectionMetadataUrl).Find(nil).All(context.Background(), &urls)
	if err != nil {
		zap.L().Error("loadConfigFromDB失败：" + err.Error())
		return Parameters{}, err
	}

	var provinces []Province
	err = mongo.Table(MongoCollectionMetadataProvinces).Find(nil).All(context.Background(), &provinces)
	if err != nil {
		zap.L().Error("loadConfigFromDB失败：" + err.Error())
		return Parameters{}, err
	}
	var universals []Universal
	err = mongo.Table(MongoCollectionMetadataUniversal).Find(nil).All(context.Background(), &universals)
	if err != nil {
		zap.L().Error("loadConfigFromDB失败：" + err.Error())
		return Parameters{}, err
	}

	var refs []Ref
	err = mongo.Table(MongoCollectionMetadataRef).Find(nil).All(context.Background(), &refs)
	if err != nil {
		zap.L().Error("loadConfigFromDB失败：" + err.Error())
		return Parameters{}, err
	}

	var smsDevices []SMSDevice
	err = mongo.Table(MongoCollectionMetadataSmsDevices).Find(nil).All(context.Background(), &smsDevices)
	if err != nil {
		zap.L().Error("loadConfigFromDB失败：" + err.Error())
		return Parameters{}, err
	}

	smsDevicesMap := make(map[string]SMSDevice)
	for _, data := range smsDevices {
		smsDevicesMap[data.IPAddress] = data
	}

	var smsNameList []SMSNameList
	err = mongo.Table(MongoCollectionMetadataSmsNameList).Find(nil).All(context.Background(), &smsNameList)
	if err != nil {
		zap.L().Error("loadConfigFromDB失败：" + err.Error())
		return Parameters{}, err
	}
	smsNameListMap := make(map[string]string)
	for _, data := range smsNameList {
		smsNameListMap[data.Group+data.SMSName] = data.CName
	}

	parameters := Parameters{
		Urls:        urls,
		Provinces:   provinces,
		Universal:   universals,
		Refs:        refs,
		SMSDevice:   smsDevicesMap,
		minRPMCache: nil,
		SMSNameList: smsNameListMap,
	}

	parameters.minRPMCache = loadMinRPMCache(parameters)
	parameters.minERRCodeCache = loadMinERRRPMCache(parameters)
	return parameters, nil
}

func loadMinRPMCache(parms Parameters) map[string]float64 {
	cache := make(map[string]float64)

	for _, province := range parms.Provinces {
		for _, url := range parms.Urls {
			key := GetMinRPMKey(province.Province, province.Remark, url.Url)
			cache[key] = getMinValue(province.Province, province.Remark, url.Url,
				func(p Url) float64 { return p.MinRPM },
				GetUniversal(province.Province, province.Remark, parms).MinRPM, parms)
		}
	}

	return cache
}
func loadMinERRRPMCache(parms Parameters) map[string]float64 {
	cache := make(map[string]float64)

	for _, province := range parms.Provinces {
		for _, url := range parms.Urls {
			key := GetMinRPMKey(province.Province, province.Remark, url.Url)
			cache[key] = getMinValue(province.Province, province.Remark, url.Url,
				func(p Url) float64 { return p.MinErrCodeRPM },
				GetUniversal(province.Province, province.Remark, parms).MinErrCodeRPM, parms)
		}
	}

	return cache
}

func GetMinRPMKey(province, remark, regex string) string {
	return fmt.Sprintf("%s|%s|%s", province, remark, regex)
}

func LoadConfig(mongo *mongodb.Mongo) *Parameters {
	newConfig, err := loadConfigFromDB(mongo)
	if err != nil {
		zap.L().Error("加载参数列表失败:" + err.Error())
		return &Parameters{}
	}
	zap.L().Info("刷新参数成功。")
	return &newConfig
}

//func MatchUrl(urls []Url, province, remark, url string) (Url, bool) {
//	var value Url
//	b := true
//	for _, val := range urls {
//		if val.Province == province && val.ReMark == remark && val.Url == url {
//			value = val
//			break
//		} else if val.Url == url && val.Province == "ALL" && val.ReMark == "ALL" {
//			value = val
//		}
//	}
//
//	if reflect.DeepEqual(value, Url{}) {
//		//没有找到对应的参数，跳过
//		zap.L().Debug("未找到对应的参数", zap.String("province", province), zap.String("remark", remark), zap.String("url", url))
//		b = false
//	}
//
//	return value, b
//}

func copyNonZeroFields(source, target *Url, placeHolder *map[string]int, level int) {
	sourceVal := reflect.ValueOf(*source)
	targetVal := reflect.ValueOf(target).Elem() // 传递 target 的指针并调用 Elem() 获取可寻址的值

	for i := 0; i < sourceVal.NumField(); i++ {
		sourceField := sourceVal.Type().Field(i)
		sourceFieldValue := sourceVal.Field(i)
		targetFieldValue := targetVal.Field(i)

		// 获取字段名
		fieldName := sourceField.Name

		// 如果是 Action 字段，则钻取每个项进行零值比较和设置
		if fieldName == "Action" {
			// 检查目标结构体中的 Action 字段是否已经初始化
			if targetFieldValue.IsNil() {
				// 初始化 Action 字段
				targetFieldValue.Set(reflect.MakeMap(sourceFieldValue.Type()))
			}

			// 获取 Action 字段的值
			actionMap := sourceFieldValue.Interface().(map[string]float64)
			targetActionMap := targetFieldValue.Interface().(map[string]float64)

			// 迭代 Action 字段的每个项
			for key, val := range actionMap {
				// 检查当前项的值是否为零
				if val != 0 {
					// 获取当前项在 placeHolder 中的级别
					itemLevel := (*placeHolder)[fieldName+"."+key]
					// 如果当前级别为空或当前级别小于新的级别，则更新 placeHolder 中的级别
					if itemLevel == 0 || level > itemLevel {
						(*placeHolder)[fieldName+"."+key] = level
						// 设置目标结构体中对应的项的值
						targetActionMap[key] = val
					}

				}
			}
		} else {
			// 非 Action 字段，直接进行赋值操作

			// 检查当前字段的 level 是否大于 placeHolder 中相同字段的值 ， 且非0值
			if level > (*placeHolder)[fieldName] && !reflect.DeepEqual(sourceFieldValue.Interface(), reflect.Zero(sourceFieldValue.Type()).Interface()) {
				targetFieldValue.Set(sourceFieldValue)
				// 更新 placeHolder 中的 level 值
				(*placeHolder)[fieldName] = level
			}
		}
	}
}

func MatchUrl(urls []Url, province, remark, url string) (Url, bool) {
	var value Url
	currentTime := time.Now().Unix()
	placeHolder := make(map[string]int)
	b := true
	url = tools.RemoveRegexSymbols(url)

	for _, val := range urls {
		tmpUrl := tools.RemoveRegexSymbols(val.Url)
		if val.Province == province && val.ReMark == remark && tmpUrl == url && val.EffectStart <= currentTime && val.EffectEnd >= currentTime {
			copyNonZeroFields(&val, &value, &placeHolder, 4)
		} else if val.Province == province && val.ReMark == remark && tmpUrl == url && val.EffectStart == 0 && val.EffectEnd == 0 {
			copyNonZeroFields(&val, &value, &placeHolder, 3)
		} else if tmpUrl == url && val.Province == province && val.ReMark == "ALL" && val.EffectStart == 0 && val.EffectEnd == 0 {
			copyNonZeroFields(&val, &value, &placeHolder, 2)
		} else if tmpUrl == url && val.Province == "ALL" && val.ReMark == "ALL" && val.EffectStart == 0 && val.EffectEnd == 0 {
			copyNonZeroFields(&val, &value, &placeHolder, 1)
		}
	}

	if reflect.DeepEqual(value, Url{}) {
		//没有找到对应的参数，跳过
		zap.L().Debug("未找到对应的参数", zap.String("province", province), zap.String("remark", remark), zap.String("url", url))
		b = false
	}

	return value, b
}

func MatchRef(refs []Ref, province, remark, url, ip string) Ref {
	if &ip == nil || ip == "" {
		ip = "0.0.0.0"
	}
	var value Ref
	for _, val := range refs {
		if val.Province == province && val.ReMark == remark && val.Url == url && (ip == "0.0.0.0" || val.IPAddress == ip) {
			value = val
			if ip == "0.0.0.0" {
				value.IPAddress = "0.0.0.0"
			}
			break
		}
	}

	if reflect.DeepEqual(value, Ref{}) {
		//没有找到对应的参数，跳过
		zap.L().Debug("未找到对应的参数", zap.String("province", province), zap.String("remark", remark), zap.String("url", url), zap.String("ip", ip))
		value = Ref{
			Province:  province,
			ReMark:    remark,
			Url:       url,
			RealLines: 0, //默认值给0
			IPAddress: ip,
		}
	}

	return value
}

func GetProvincesByType(provinces []Province, types string) []Province {
	switch types {
	case SLATypeSMS:
		smsProvince := make([]Province, 0)
		for _, province := range provinces {
			if province.Province == SLATypeSMS {
				smsProvince = append(smsProvince, province)
			}
		}
		return smsProvince
	default:
		defaultProvinces := make([]Province, 0)
		for _, province := range provinces {
			if province.Province != SLATypeSMS {
				defaultProvinces = append(defaultProvinces, province)
			}
		}
		return defaultProvinces
	}
}

/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/6/28 16:10
*/

package metaData

import (
	"context"
	"errors"
	"go.uber.org/zap"
	"regexp"
	"sla/common/storage/mongodb"
	"sla/common/tools"
	"sla/public/parameters"
	"strconv"
	"time"
)

const uuidKey = "requestUUID"

type CommonMethods interface {
	SetID() error
	GetID() string
	GetUUID() string
	SetUUID(uuid string)
	GetContext() context.Context
	InList() bool
	UpdateParameters(p *parameters.Parameters)
	UpdateUrl()
	UpdateReCache(r []*regexp.Regexp)
	Write() error
	GetBase() MetaBase
	GetURLs() []URLDetail
	SetURLs(urls []URLDetail)
	SetBase(base MetaBase)
}

func CreateMetaData(x string, mongo *mongodb.Mongo) CommonMethods {
	if x == parameters.SLATypeSMS {
		return CreateMetaSMS(mongo)
	} else {
		return CreateMeta(mongo)
	}
}

type MetaBase struct {
	ID         string                 `json:"_id" bson:"_id"`
	Timestamp  int64                  `json:"timestamp" bson:"timestamp"`
	Province   string                 `json:"province" bson:"province"`
	ReMark     string                 `json:"reMark" bson:"reMark"`
	Regex      string                 `json:"regex" bson:"regex"`
	URLs       []URLDetail            `json:"urls" bson:"urls"`
	parameters *parameters.Parameters `bson:"-"`
	mongo      *mongodb.Mongo         `bson:"-"`
	ctx        context.Context        `bson:"-"`
	reCache    []*regexp.Regexp       `bson:"-"`
}

type URLDetail struct {
	URL         string             `json:"url" bson:"url"`
	RtAll       float64            `json:"rt_all" bson:"rt_all"`
	ErLines     float64            `json:"er_lines" bson:"er_lines"`
	ErrorLines  float64            `json:"error_lines" bson:"error_lines"`
	BytesAll    float64            `json:"bytes_all" bson:"bytes_all"`
	NormalLines float64            `json:"normal_lines" bson:"normal_lines"`
	NormalCount float64            `json:"normal_count" bson:"normal_count"`
	AllLines    float64            `json:"all_lines" bson:"all_lines"`
	ActionLines map[string]float64 `json:"action_lines" bson:"action_lines"`
	Update      bool               `bson:"-"`
}

func ExtractUrls(data []parameters.Url) []string {
	var urls []string
	for _, entry := range data {
		urls = append(urls, entry.Url)
	}
	return urls
}

func updateURLDetail(url *URLDetail, entry MessageEntry) {
	url.URL = entry.URL
	url.AllLines += entry.Lines
	if entry.Status == 200 && entry.Type == parameters.LogTypeAccess {
		url.RtAll += entry.RequestTime
		url.BytesAll += entry.Bytes
		url.NormalLines += entry.Lines
		url.NormalCount++
	} else if entry.Status >= 400 && entry.Type == parameters.LogTypeAccess {
		url.ErLines += entry.Lines
	} else if entry.Type == parameters.LogTypeError {
		url.ErrorLines += entry.Lines
	} else if entry.Type == parameters.LogTypeDegradation {
		if url.ActionLines == nil {
			url.ActionLines = make(map[string]float64)
		}
		url.ActionLines[entry.Action] += entry.Lines
	}
}

func (m *MetaBase) GetContext() context.Context {
	return m.ctx
}

func (m *MetaBase) GetUUID() string {
	return m.ctx.Value(uuidKey).(string)
}

func (m *MetaBase) SetUUID(uuid string) {
	m.ctx = context.WithValue(context.Background(), uuidKey, uuid)
}

func (m *MetaBase) InList() bool {
	start := time.Now()
	defer func() {
		zap.L().Debug("InList耗时", zap.Any("during", time.Since(start)))
	}()
	url := tools.RemoveRegexSymbols(m.Regex)
	if _, _, b := tools.FindMatches(m.reCache, url); b &&
		tools.IsInSlice(m.parameters.Provinces, parameters.Province{Province: m.Province, Remark: m.ReMark}) &&
		tools.IsTimeInRange(parameters.GetUniversal(m.Province, m.ReMark, *m.parameters).TimeRangeStart,
			parameters.GetUniversal(m.Province, m.ReMark, *m.parameters).TimeRangeEnd, m.Timestamp) {
		return true
	}
	return false
}

func (m *MetaBase) GetID() string {
	return m.ID
}

func (m *MetaBase) SetID() error {
	if m.Timestamp != 0 && m.Province != "" && m.ReMark != "" && m.Regex != "" {
		m.ID = tools.CalculateSHA1(strconv.FormatInt(m.Timestamp, 10) + m.Province + m.ReMark + m.Regex)
		return nil
	}
	return errors.New("关键字段不能为空！")
}

func (m *MetaBase) UpdateParameters(p *parameters.Parameters) {
	start := time.Now()
	defer func() {
		zap.L().Debug("UpdateParameters耗时", zap.Any("during", time.Since(start)))
	}()
	m.parameters = p
}

func (m *MetaBase) UpdateReCache(r []*regexp.Regexp) {
	start := time.Now()
	defer func() {
		zap.L().Debug("UpdateReCache耗时", zap.Any("during", time.Since(start)))
	}()
	m.reCache = r
}

func (m *MetaBase) UpdateUrl() {
	start := time.Now()
	defer func() {
		zap.L().Debug("UpdateUrl耗时", zap.Any("during", time.Since(start)))
	}()
	url, idx, exist := tools.FindMatches(m.reCache, m.Regex)
	if exist {
		m.Regex = url[idx]
	}
}
func (m *MetaBase) getURLs() []URLDetail {
	return m.URLs
}
func (m *MetaBase) setURLs(urls []URLDetail) {
	m.URLs = urls
}

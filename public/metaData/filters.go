/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/4/30 16:11
*/

package metaData

import (
	"context"
	"go.uber.org/zap"
	"sla/common/storage/mongodb"
	"sla/public/parameters"
)

type Filters struct {
	mongo         *mongodb.Mongo     `bson:"-"`
	ctx           context.Context    `bson:"-"`
	em            *ExpressionMatcher `bson:"-"`
	filterStrings []filterString     `bson:"-"`
}

type filterString struct {
	FilterString string `bson:"FilterString"`
}

func CreateFilter(mongo *mongodb.Mongo) *Filters {
	return &Filters{
		mongo: mongo,
	}
}

func (f *Filters) GetFilters() *Filters {
	err := f.mongo.Table(parameters.MongoCollectionMetadataFilters).Find(nil).All(f.ctx, &f.filterStrings)
	if err != nil {
		zap.L().Error("获取数据失败：" + err.Error())
		return f
	}

	// 预编译表达式
	expressions := filterStringToSlice(f.filterStrings)
	matcher, err := NewExpressionMatcher(expressions)
	if err != nil {
		zap.L().Error("表达式预编译失败：" + err.Error())
		return f
	}

	f.em = matcher
	zap.L().Info("刷新filters参数并完成预编译成功。")
	return f
}

func (f *Filters) IsMatch(data MessageEntry) bool {
	if f.em == nil {
		zap.L().Error("表达式匹配器尚未初始化。")
		return false
	}
	return f.em.Match(data)
}

func filterStringToSlice(filters []filterString) []string {
	var filterStrings []string
	for _, f := range filters {
		filterStrings = append(filterStrings, f.FilterString)
	}
	return filterStrings
}

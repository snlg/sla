/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/4/9 11:30
*/

package metaData

import (
	"context"
	"go.uber.org/zap"
	"sla/common/storage/mongodb"
	"sla/common/storage/mongodb/types"
	"sla/common/tools"
	"sla/public/parameters"
	"time"
)

func CreateMeta(mongo *mongodb.Mongo) *Meta {
	return &Meta{
		MetaBase: MetaBase{
			//Update: false,
			mongo: mongo,
			ctx:   context.WithValue(context.Background(), uuidKey, tools.GetUUID()),
		},
	}
}

type MetaData interface {
	CommonMethods
	InitByEntry(entry MessageEntry) MetaData
	GetDataFromMongoByID() MetaData
	Append(entry MessageEntry) MetaData
}

type Meta struct {
	MetaBase `bson:",inline"`
}

func (m *Meta) GetURLs() []URLDetail {
	return m.getURLs()
}

func (m *Meta) SetURLs(urls []URLDetail) {
	m.setURLs(urls)
}

func (m *Meta) Append(entry MessageEntry) MetaData {
	start := time.Now()
	defer func() {
		zap.L().Debug("Append耗时", zap.Any("during", time.Since(start)))
	}()
	found := false
	for i, url := range m.URLs {
		if entry.URL == url.URL {
			updateURLDetail(&m.URLs[i], entry)
			found = true
			break
		}
	}
	if !found {
		url := URLDetail{}
		updateURLDetail(&url, entry)
		m.URLs = append(m.URLs, url)
	}
	return m
}

func (m *Meta) Write() error {
	start := time.Now()
	defer func() {
		zap.L().Debug("Write耗时", zap.Any("during", time.Since(start)))
	}()
	filter := map[string]interface{}{
		"_id": m.ID,
	}
	//m.ID = ""
	err := m.mongo.Table(parameters.MongoCollectionMetadata).Upsert(m.ctx, filter, m)
	return err

	//return errors.New("不在参数列表清单中。")
}

func (m *Meta) GetDataFromMongoByID() MetaData {
	start := time.Now()
	defer func() {
		zap.L().Debug("GetDataFromMongoByID耗时", zap.Any("during", time.Since(start)))
	}()
	//数据库获取
	filter := map[string]interface{}{
		"_id": m.ID,
	}
	err := m.mongo.Table(parameters.MongoCollectionMetadata).Find(filter).One(m.ctx, &m)
	if err != types.ErrDocumentNotFound {
		//m.Update = true
	}
	return m
}

func (m *Meta) InitByEntry(entry MessageEntry) MetaData {
	m.Province = entry.Province
	m.ReMark = entry.ReMark
	m.Regex = entry.URL
	m.Timestamp = entry.Timestamp.Unix()
	return m
}

func (m *Meta) GetBase() MetaBase {
	return m.MetaBase
}

func (m *Meta) SetBase(base MetaBase) {
	m.MetaBase = base
}

/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/4/29 18:27
*/

package metaData

import (
	"github.com/Knetic/govaluate"
	"go.uber.org/zap"
	"reflect"
)

type ExpressionMatcher struct {
	expressions         []string
	compiledExpressions []*govaluate.EvaluableExpression
}

func NewExpressionMatcher(expressions []string) (*ExpressionMatcher, error) {
	compiledExpressions := make([]*govaluate.EvaluableExpression, len(expressions))
	for i, exprStr := range expressions {
		expr, err := govaluate.NewEvaluableExpression(exprStr)
		if err != nil {
			return nil, err
		}
		compiledExpressions[i] = expr
	}
	return &ExpressionMatcher{
		expressions:         expressions,
		compiledExpressions: compiledExpressions,
	}, nil
}

func (em *ExpressionMatcher) Match(data MessageEntry) bool {
	parameters := make(map[string]interface{})
	fillParameters(data, parameters)

	for i, expression := range em.compiledExpressions {
		result, err := expression.Evaluate(parameters)
		if err != nil {
			zap.L().Error("Error:" + err.Error())
			continue
		}

		if result.(bool) {
			zap.L().Debug("匹配规则："+em.expressions[i], zap.Any("msg", data))
			return true
		}
	}

	return false
}

func fillParameters(data interface{}, parameters map[string]interface{}) {
	val := reflect.ValueOf(data)
	typ := val.Type()

	for i := 0; i < val.NumField(); i++ {
		field := typ.Field(i)
		fieldValue := val.Field(i)

		// 仅添加导出字段
		if field.PkgPath != "" {
			continue
		}

		parameters[field.Tag.Get("json")] = fieldValue.Interface()
	}
}

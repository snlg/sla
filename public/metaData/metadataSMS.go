/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/6/28 10:11
*/

package metaData

import (
	"context"
	"errors"
	"go.uber.org/zap"
	"sla/common/storage/mongodb"
	"sla/common/storage/mongodb/types"
	"sla/common/tools"
	"sla/public/parameters"
	"strconv"
	"time"
)

func CreateMetaSMS(mongo *mongodb.Mongo) *MetaSMS {
	return &MetaSMS{
		MetaBase: MetaBase{
			//Update: false,
			mongo: mongo,
			ctx:   context.WithValue(context.Background(), uuidKey, tools.GetUUID()),
		},
	}
}

type MetaSMSData interface {
	CommonMethods
	InitByEntry(entry MessageEntry) MetaSMSData
	GetDataFromMongoByID() MetaSMSData
	Append(entry MessageEntry) MetaSMSData
	SetIpAddress(ip string)
	GetIpAddress() string
}

type MetaSMS struct {
	MetaBase  `bson:",inline"`
	IpAddress string `json:"ipAddress" bson:"ipAddress"`
}

func (m *MetaSMS) GetURLs() []URLDetail {
	return m.getURLs()
}

func (m *MetaSMS) SetURLs(urls []URLDetail) {
	m.setURLs(urls)
}

func (m *MetaSMS) SetID() error {
	if m.Timestamp != 0 && m.Province != "" && m.ReMark != "" && m.Regex != "" {
		m.ID = tools.CalculateSHA1(strconv.FormatInt(m.Timestamp, 10) + m.Province + m.ReMark + m.Regex + m.IpAddress)
		return nil
	}
	return errors.New("关键字段不能为空！")
}

func (m *MetaSMS) InitByEntry(entry MessageEntry) MetaSMSData {
	m.Province = entry.Province
	m.ReMark = entry.ReMark
	m.Regex = entry.URL
	m.IpAddress = entry.IpAddress
	m.Timestamp = entry.Timestamp.Unix()
	return m
}

func (m *MetaSMS) GetDataFromMongoByID() MetaSMSData {
	start := time.Now()
	defer func() {
		zap.L().Debug("GetDataFromMongoByID耗时", zap.Any("during", time.Since(start)))
	}()
	//数据库获取
	filter := map[string]interface{}{
		"_id": m.ID,
	}
	err := m.mongo.Table(parameters.MongoCollectionMetadataSms).Find(filter).One(m.ctx, &m)
	if err != types.ErrDocumentNotFound {
		//m.Update = true
	}
	return m
}

func (m *MetaSMS) Append(entry MessageEntry) MetaSMSData {
	start := time.Now()
	defer func() {
		zap.L().Debug("Append耗时", zap.Any("during", time.Since(start)))
	}()
	found := false
	for i, url := range m.URLs {
		if entry.URL == url.URL {
			updateURLDetail(&m.URLs[i], entry)
			found = true
			break
		}
	}
	if !found {
		url := URLDetail{}
		updateURLDetail(&url, entry)
		m.URLs = append(m.URLs, url)
	}
	return m
}

func (m *MetaSMS) Write() error {
	start := time.Now()
	defer func() {
		zap.L().Debug("Write耗时", zap.Any("during", time.Since(start)))
	}()
	filter := map[string]interface{}{
		"_id": m.ID,
	}
	//m.ID = ""
	err := m.mongo.Table(parameters.MongoCollectionMetadataSms).Upsert(m.ctx, filter, m)
	return err
}

func (m *MetaSMS) GetBase() MetaBase {
	return m.MetaBase
}

func (m *MetaSMS) SetBase(base MetaBase) {
	m.MetaBase = base
}

func (m *MetaSMS) SetIpAddress(ip string) {
	m.IpAddress = ip
}
func (m *MetaSMS) GetIpAddress() string {
	return m.IpAddress
}

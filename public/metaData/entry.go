/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/4/9 11:31
*/

package metaData

import "time"

type MessageEntry struct {
	Timestamp   time.Time `json:"@timestamp"`
	Province    string    `json:"host_group"`
	ReMark      string    `json:"host_remark_type"`
	URL         string    `json:"url"`
	Status      int32     `json:"status"`
	RequestTime float64   `json:"request_time_all"`
	Lines       float64   `json:"lines"`
	Bytes       float64   `json:"body_byte_sent"`
	Type        int32     `json:"type"`
	Action      string    `json:"action"`
	IpAddress   string    `json:"ip"`
}

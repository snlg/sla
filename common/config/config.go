/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/4/8 15:14
*/

package config

import (
	"encoding/json"
	"fmt"
	"github.com/spf13/viper"
	"os"
	"strings"
)

func CreateConfig() Cfg {
	return &cfg{}
}

func (c cfg) GetConfig() *cfg {
	viper.SetConfigFile("./application.yaml")
	viper.SetDefault("Server.Address", "0.0.0.0")
	viper.SetDefault("Server.Port", "18099")
	viper.SetDefault("Server.Mode", "Dev")
	viper.SetEnvPrefix("SLA")
	viper.AutomaticEnv()
	replacer := strings.NewReplacer(".", "_")
	viper.SetEnvKeyReplacer(replacer)
	if err := viper.ReadInConfig(); err != nil {
		panic(fmt.Errorf("配置文件严重异常: %s \n", err))
	}
	if err := viper.Unmarshal(&c); err != nil {
		panic(fmt.Errorf("配置文件反序列化失败, err:%s \n", err))
		//fmt.Printf("配置文件反序列化失败, err:%s \n", err)
	}
	conditionsStr := os.Getenv("SLA_ALERT_CONDITIONS")
	if conditionsStr != "" {
		var conditions []Condition
		if err := json.Unmarshal([]byte(conditionsStr), &conditions); err != nil {
			panic(fmt.Errorf("环境变量conditionsStr解析失败, err:%s \n", err))
		}
		c.SLAAlertConfig.Conditions = conditions
	}

	return &c
}

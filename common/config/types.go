/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/4/8 15:13
*/

package config

import (
	log "sla/common/logger/types"
	"sla/common/storage/elasticsearch"
	kafka "sla/common/storage/kafka/types"
	"sla/common/storage/mongodb"
	slaapi "sla/server/types"
	"time"
)

type Cfg interface {
	GetConfig() *cfg
}
type cfg struct {
	Kafka          kafka.KafkaConfig       `json:"kafka"`
	Log            log.LogConfig           `json:"log"`
	Mongo          mongodb.Config          `json:"mongo"`
	Cron           Cron                    `json:"cron"`
	Etcd           Etcd                    `json:"etcd"`
	MonitorES      elasticsearch.MonitorES `json:"monitorES"`
	Server         slaapi.Server           `json:"server"`
	SLAAlertConfig Alert                   `json:"slaAlertConfig"`
}

type Cron struct {
	Interval struct {
		CalSLA     string `json:"calSLA"`
		WriteES    string `json:"writeES"`
		DetailToES string `json:"detailToES"`
		Alert      string `json:"alert"`
		RefreshRef string `json:"refreshRef"`
		SMSRpmToES string `json:"smsRpmToES"`
	} `json:"Interval"`
}

type Etcd struct {
	EndPoints []string      `json:"endpoints"`
	Timeout   time.Duration `json:"timeout"`
}

type Alert struct {
	Conditions []Condition `json:"conditions"`
	Name       string      `json:"name"`
	Type       string      `json:"type"`
	Webhook    string      `json:"webhook"`
}

type Condition struct {
	Threshold float64 `json:"threshold"`
	Value     int     `json:"value"`
}

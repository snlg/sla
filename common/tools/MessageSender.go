/*
@Project ：ops_tools_go
@Author  ：SHAN
@Date    ：2023/12/19 20:32
*/

package tools

import (
	"bytes"
	"encoding/json"
	"go.uber.org/zap"
	"net/http"
)

type MessageSender interface {
	SendWithMarkdown()
}

type DingTalk struct {
	Webhook string `json:"webhook"`
	Title   string `json:"title"`
	Content string `json:"content"`
}

type dingTalkMessage struct {
	MsgType  string                 `json:"msgtype"`
	Markdown map[string]interface{} `json:"markdown"`
	At       map[string]interface{} `json:"at"`
}

type YSTAlert struct {
	Webhook string          `json:"webhook"`
	Msg     YstAlertMessage `json:"msg"`
}

type YstAlertMessage struct {
	Name      string `json:"name"`
	Type      string `json:"type"`
	Area      string `json:"area"`
	Business  string `json:"business"`
	StartTime int64  `json:"startTime"`
	//EndTime    int64  `json:"endTime"` //不需要设置结束时间
	Attributes struct {
		AffectedUrl string `json:"AffectedUrl"`
	} `json:"attributes"`
	Value       string `json:"value"`
	Description string `json:"description"`
	Level       int    `json:"level"`
	Source      string `json:"source"`
	AlertStatus string `json:"alertStatus"`
}

// SendWithMarkdown https://inf-doc.ysten.conf:8888/docs/yunwei/%E6%99%BA%E8%83%BD%E5%91%8A%E8%AD%A6/%E5%BC%80%E5%8F%91%E7%9B%B8%E5%85%B3/%E6%8E%A5%E5%8F%A3%E6%96%87%E6%A1%A3/#_1
func (s *YSTAlert) SendWithMarkdown() {
	if SendPostByJson(s.Webhook, []interface{}{s.Msg}) {
		zap.L().Info("已推送至智能告警平台...")
		return
	}
	zap.L().Error("推送智能告警平台失败...")
}

// SendWithMarkdown https://open.dingtalk.com/document/robots/custom-robot-access
// For testing
func (s *DingTalk) SendWithMarkdown() {
	message := dingTalkMessage{
		MsgType: "markdown",
		Markdown: map[string]interface{}{
			"title": s.Title,
			"text":  s.Content,
		},
		At: map[string]interface{}{
			//"isAtAll": true,
			"isAtAll":   false,
			"atMobiles": []string{"18626069513"},
		},
	}
	if SendPostByJson(s.Webhook, message) {
		zap.L().Error("推送钉钉告警平台失败...")
		return
	}
	zap.L().Info("已推送至钉钉告警平台...")
}

func SendPostByJson(webhook string, body interface{}) bool {
	payload, err := json.Marshal(body)
	zap.L().Info("ALertMSG", zap.Any("body", body))
	if err != nil {
		zap.L().Error("JSON 序列化失败:"+err.Error(), zap.Any("body", body))
		return false
	}
	resp, err := http.Post(webhook, "application/json", bytes.NewReader(payload))
	if err != nil {
		zap.L().Error("HTTP POST 请求失败:"+err.Error(), zap.Any("body", body))
		return false
	}
	buf := new(bytes.Buffer)
	_, err = buf.ReadFrom(resp.Body)
	if err != nil {
		zap.L().Error("其他错误:"+err.Error(), zap.Any("body", body))
		return false
	}
	responseBody := buf.String()
	zap.L().Info(responseBody)
	if resp.StatusCode != 200 {
		zap.L().Error("HTTP POST 请求失败:"+responseBody, zap.Any("body", body))
		return false
	}
	defer resp.Body.Close()
	return true
}

/*
	if hasRisk {
	ystMsg := tools.YstAlertMessage{
	Name:      "接入层风险预警",
	Type:      "NGINX_RISK",
	Area:      ds.PlatformName,
	Business:  rd.RiskName,
	StartTime: int64(ts),
	//EndTime:     int64(ts),
	Attributes:  struct{}{},
	Value:       strconv.FormatFloat(data[1], 'f', -1, 32),
	Description: msg,
	Level:       riskUrlConfig.YSTAlertLevel,
	Source:      "风险预警",
	AlertStatus: "PENDING",
	}
	ystAlert := tools.YSTAlert{Webhook: ystwebhook, Msg: ystMsg}
	ystAlert.SendWithMarkdown()
	zap.L().Info(msg)
	}
*/

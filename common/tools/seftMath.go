/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/4/26 15:43
*/

package tools

// CalculateChangeRate
// 暂时没有好的方法，如机器学习或其他检测机制
// 目前使用最近一个数据较 N 个点平均值的变化率，作为参考
func CalculateChangeRate(valueData []float64) float64 {
	var sum float64
	for _, value := range valueData {
		sum += value
	}
	average := sum / float64(len(valueData))

	currentValue := valueData[len(valueData)-1]

	changeRate := (currentValue - average) / average

	return changeRate
}

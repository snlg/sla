/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/4/9 15:17
*/

package tools

import (
	"crypto/sha1"
	"encoding/hex"
	"github.com/google/uuid"
	"go.uber.org/zap"
	"reflect"
	"regexp"
	"strings"
	"time"
)

func CalculateSHA1(input string) string {
	hash := sha1.New()
	hash.Write([]byte(input))
	hashedBytes := hash.Sum(nil)
	hashedString := hex.EncodeToString(hashedBytes)

	return hashedString
}

func StringContains(arr []string, query string) bool {
	for _, s := range arr {
		if s == query {
			return true
		}
	}
	return false
}

func MatchesPattern(arr []string, pattern string) bool {
	re, err := regexp.Compile(pattern)
	if err != nil {
		return false
	}
	for _, s := range arr {
		if re.MatchString(s) {
			return true
		}
	}
	return false
}

// FindMatches 性能太差
//
//	func FindMatches(arr []string, target string) ([]string, int, bool) {
//		var matches []string
//		var maxMatchLength int
//		var bestMatchIndex = 0
//		var bestMatchProportion float64
//
//		for _, pattern := range arr {
//			re, err := regexp.Compile(pattern)
//			if err != nil {
//				continue
//			}
//			match := re.FindString(target)
//			if match != "" {
//				matches = append(matches, pattern)
//				matchLength := len(match)
//				patternLength := len(pattern)
//				matchProportion := float64(matchLength) / float64(patternLength)
//
//				if matchLength > maxMatchLength || (matchLength == maxMatchLength && matchProportion > bestMatchProportion) {
//					maxMatchLength = matchLength
//					bestMatchProportion = matchProportion
//					bestMatchIndex = len(matches) - 1
//				}
//			}
//		}
//
//		return matches, bestMatchIndex, len(matches) > 0
//	}

func InitRegexpCache(arr []string) []*regexp.Regexp {
	var reCache []*regexp.Regexp
	reCache = make([]*regexp.Regexp, len(arr))
	for i, pattern := range arr {
		re, err := regexp.Compile(pattern)
		if err != nil {
			continue
		}
		reCache[i] = re
	}
	return reCache
}

func FindMatches(reCache []*regexp.Regexp, target string) ([]string, int, bool) {
	var matches []string
	var maxMatchLength int
	var bestMatchIndex = 0
	var bestMatchProportion float64

	for i, re := range reCache {
		if re == nil {
			continue
		}
		match := re.FindString(target)
		if match != "" {
			matches = append(matches, reCache[i].String())
			matchLength := len(match)
			patternLength := len(reCache[i].String())
			matchProportion := float64(patternLength) / float64(matchLength)

			if matchLength > maxMatchLength || (matchLength == maxMatchLength && matchProportion > bestMatchProportion) {
				maxMatchLength = matchLength
				bestMatchProportion = matchProportion
				bestMatchIndex = len(matches) - 1
			}
		}
	}

	return matches, bestMatchIndex, len(matches) > 0
}

func RemoveRegexSymbols(target string) string {
	if strings.HasPrefix(target, "^") {
		target = target[1:len(target)]
	}
	if strings.HasSuffix(target, "$") {
		target = target[0 : len(target)-1]
	}
	re := regexp.MustCompile(`\\`)
	u := re.ReplaceAllString(target, "")
	return u
}

// IsInSlice 判断一个对象是否在对象数组中
func IsInSlice(slice interface{}, item interface{}) bool {
	sliceValue := reflect.ValueOf(slice)
	if sliceValue.Kind() != reflect.Slice {
		zap.L().Error("类型不正确，需要切片。")
		return false
	}

	for i := 0; i < sliceValue.Len(); i++ {
		sliceItem := sliceValue.Index(i).Interface()
		if reflect.DeepEqual(sliceItem, item) {
			return true
		}
	}

	return false
}

// IsTimeInRange 判读时间戳是否在时间字符串（比如08:00-23:00）内
func IsTimeInRange(startTime, endTime string, timestamp int64) bool {
	layout := "15:04"
	start, err := time.Parse(layout, startTime)
	if err != nil {
		zap.L().Error("格式化时间失败，" + err.Error())
		return false
	}

	end, err := time.Parse(layout, endTime)
	if err != nil {
		zap.L().Error("格式化时间失败，" + err.Error())
		return false
	}

	// 将时间戳转换为时间对象
	tsTime := time.Unix(timestamp, 0)

	// 提取小时和分钟
	tsHour, tsMin, _ := tsTime.Clock()

	// 构造开始时间和结束时间的时间对象
	startTimeObj := time.Date(0, 1, 1, start.Hour(), start.Minute(), 0, 0, time.UTC)
	endTimeObj := time.Date(0, 1, 1, end.Hour(), end.Minute(), 0, 0, time.UTC)

	// 构造时间戳的时间对象
	tsTimeObj := time.Date(0, 1, 1, tsHour, tsMin, 0, 0, time.UTC)

	// 比较时间
	return tsTimeObj.After(startTimeObj) && tsTimeObj.Before(endTimeObj)
}

func isLeapYear(year int) bool {
	// 判断是否为闰年的逻辑
	if year%400 == 0 {
		return true
	} else if year%100 == 0 {
		return false
	} else if year%4 == 0 {
		return true
	}
	return false
}

func GetDaysInMonth(timestamp int64) int64 {
	t := time.Unix(timestamp, 0)

	year, month, _ := t.Date()

	switch month {
	case time.February:
		if isLeapYear(year) {
			return 29
		} else {
			return 28
		}
	case time.April, time.June, time.September, time.November:
		return 30
	default:
		return 31
	}
}

func GetDaysInYear(timestamp int64) int64 {
	t := time.Unix(timestamp, 0)
	year, _, _ := t.Date()

	if isLeapYear(year) {
		return 366
	} else {
		return 365
	}
}

func GetUUID() string {
	return uuid.New().String()
}

func IsSameDay(timestamp1, timestamp2 int64) bool {
	time1 := time.Unix(timestamp1, 0)
	time2 := time.Unix(timestamp2, 0)
	return time1.Year() == time2.Year() && time1.Month() == time2.Month() && time1.Day() == time2.Day()
}

/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/4/12 19:27
*/

package transaction

import "sync"

// Lock 本地锁，备用
type Lock struct {
	mu    sync.Mutex
	locks sync.Map
}

func (tl *Lock) Lock(id string) {
	tl.mu.Lock()
	defer tl.mu.Unlock()
	lock, _ := tl.locks.LoadOrStore(id, &sync.Mutex{})
	lock.(*sync.Mutex).Lock()
}

func (tl *Lock) Unlock(id string) {
	tl.mu.Lock()
	defer tl.mu.Unlock()
	if lock, ok := tl.locks.Load(id); ok {
		lock.(*sync.Mutex).Unlock()
		tl.locks.Delete(id)
	}
}

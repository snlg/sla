/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/4/12 21:44
*/

package transaction

import (
	"context"
	"errors"
	clientv3 "go.etcd.io/etcd/client/v3"
	"go.etcd.io/etcd/client/v3/concurrency"
	"time"
)

// EtcdLock Etcd异步锁
type EtcdLock struct {
	client *clientv3.Client
}

func NewEtcdLock(endpoints []string, dialTimeout time.Duration) (*EtcdLock, error) {
	cfg := clientv3.Config{
		Endpoints:   endpoints,
		DialTimeout: dialTimeout,
	}

	client, err := clientv3.New(cfg)
	if err != nil {
		return nil, err
	}

	return &EtcdLock{
		client: client,
	}, nil
}

func (e *EtcdLock) Lock(ctx context.Context, id string, timeout time.Duration) (*concurrency.Mutex, *concurrency.Session, error) {
	session, err := concurrency.NewSession(e.client)
	if err != nil {
		return nil, nil, err
	}

	ctx, cancel := context.WithTimeout(ctx, timeout)
	defer cancel()

	mutex := concurrency.NewMutex(session, "/lock-"+id)
	if err := mutex.Lock(ctx); err != nil {
		return nil, nil, err
	}

	return mutex, session, nil
}

func (e *EtcdLock) Unlock(mutex *concurrency.Mutex, session *concurrency.Session) error {
	//修复加锁失败后解锁，导致mutex 和 session 进行了空指针引用，避免了空指针解引用导致的 panic 的BUG
	if mutex == nil || session == nil {
		return errors.New("mutex or session is nil")
	}

	if err := mutex.Unlock(context.Background()); err != nil {
		return err
	}

	// 修复未删除key，导致etcd OOM的bug
	//_, err := e.client.Delete(context.Background(), mutex.Key())
	//if err != nil {
	//	return err
	//}

	if err := session.Close(); err != nil {
		return err
	}

	return nil
}

/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/4/8 15:26
*/

package kafka

import "time"

//type Config struct {
//	Brokers                []string              // Kafka集群的地址列表
//	Topic                  string                // 要消费的主题名称
//	GroupID                string                // 消费者组ID
//	GroupTopics            []string              // 消费者组订阅的多个主题
//	Partition              int                   // 指定的分区
//	Dialer                 *kafka.Dialer         // 自定义的连接器
//	QueueCapacity          int                   // 内部消息队列的容量
//	MinBytes               int                   // 消费者接受的最小批量大小
//	MaxBytes               int                   // 消费者接受的最大批量大小
//	MaxWait                time.Duration         // 从 Kafka 获取消息的最大等待时间
//	ReadBatchTimeout       time.Duration         // 读取消息批次的超时时间
//	ReadLagInterval        time.Duration         // 更新读取落后的间隔时间
//	GroupBalancers         []kafka.GroupBalancer // 消费者组的平衡策略列表
//	HeartbeatInterval      time.Duration         // 心跳间隔时间
//	CommitInterval         time.Duration         // 自动提交偏移量的时间间隔
//	PartitionWatchInterval time.Duration         // 检查分区变化的时间间隔
//	WatchPartitionChanges  bool                  // 是否监听分区变化
//	SessionTimeout         time.Duration         // 会话超时时间
//	RebalanceTimeout       time.Duration         // 重新平衡的超时时间
//	JoinGroupBackoff       time.Duration         // 重新加入消费者组的间隔时间
//	RetentionTime          time.Duration         // 消费者组保存的时间
//	StartOffset            int64                 // 起始偏移量
//	ReadBackoffMin         time.Duration         // 读取消息的最小退避时间
//	ReadBackoffMax         time.Duration         // 读取消息的最大退避时间
//	Logger                 kafka.Logger          // 日志记录器
//	ErrorLogger            kafka.Logger          // 错误日志记录器
//	IsolationLevel         kafka.IsolationLevel  // 事务隔离级别
//	MaxAttempts            int                   // 连接尝试的最大次数
//	OffsetOutOfRangeError  bool                  // 偏移量超出范围是否返回错误
//}
//
//func (c *Config) NewKafkaConfig(kc types.KafkaConfig) *Config {
//	return &Config{
//		Brokers:  kc.Brokers,
//		GroupID:  kc.GroupID,
//		Topic:    kc.Topic,
//		MinBytes: kc.MinBytes,
//		MaxBytes: kc.MaxBytes,
//		MaxWait:  kc.MaxWait,
//	}
//}
//
//func (c *Config) NewReader() *kafka.Reader {
//	return kafka.NewReader(kafka.ReaderConfig{
//		Brokers:                c.Brokers,
//		GroupID:                c.GroupID,
//		Topic:                  c.Topic,
//		MinBytes:               c.MinBytes,
//		MaxBytes:               c.MaxBytes,
//		MaxWait:                c.MaxWait,
//		ReadBatchTimeout:       10 * time.Second,
//		ReadLagInterval:        -1,
//		GroupBalancers:         nil,
//		HeartbeatInterval:      3 * time.Second,
//		CommitInterval:         0,
//		PartitionWatchInterval: 5 * time.Second,
//		WatchPartitionChanges:  true,
//		SessionTimeout:         30 * time.Second,
//		RebalanceTimeout:       30 * time.Second,
//		JoinGroupBackoff:       5 * time.Second,
//		RetentionTime:          24 * time.Hour,
//		StartOffset:            kafka.LastOffset,
//		ReadBackoffMin:         100 * time.Millisecond,
//		ReadBackoffMax:         time.Second,
//		Logger:                 nil,
//		ErrorLogger:            nil,
//		IsolationLevel:         kafka.ReadCommitted,
//		MaxAttempts:            3,
//		OffsetOutOfRangeError:  false,
//	})
//}

type Config struct {
	Brokers        []string      // Kafka集群的地址列表
	Topic          string        // 要消费的主题名称
	GroupID        string        // 消费者组ID
	QueueCapacity  int           // 内部消息队列的容量
	CommitInterval time.Duration // 自动提交偏移量的时间间隔
	SessionTimeout time.Duration // 会话超时时间
}

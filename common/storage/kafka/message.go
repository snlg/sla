/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/4/8 17:50
*/

package kafka

type Message struct {
	Key   []byte
	Value []byte
}

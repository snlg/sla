/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/4/8 15:25
*/

package kafka

import (
	"context"
	"github.com/IBM/sarama"
	"go.uber.org/zap"
	"os"
	"os/signal"
	"sla/common/storage/mongodb"
	"sla/common/transaction"
	"sync"
)

type MessageHandler func(msg *Message, mongo *mongodb.Mongo, etcdLock *transaction.EtcdLock)

type KafkaConsumer struct {
	Config         *Config
	MessageHandler MessageHandler
	Mongo          *mongodb.Mongo
	EtcdLock       *transaction.EtcdLock
}

func NewKafkaConsumer(config *Config, handler MessageHandler, mongo *mongodb.Mongo, etcdLock *transaction.EtcdLock) *KafkaConsumer {
	return &KafkaConsumer{
		Config:         config,
		MessageHandler: handler,
		Mongo:          mongo,
		EtcdLock:       etcdLock,
	}
}

func (c *KafkaConsumer) ConsumeMessages(ctx context.Context) {
	config := sarama.NewConfig()
	config.Version = sarama.V2_7_0_0
	config.Consumer.Group.Rebalance.Strategy = sarama.BalanceStrategyRange
	config.Consumer.Offsets.AutoCommit.Enable = true
	config.Consumer.Offsets.AutoCommit.Interval = c.Config.CommitInterval
	config.Consumer.Group.Session.Timeout = c.Config.SessionTimeout

	// 修改部分：优化配置
	//config.Consumer.Fetch.Min = 1
	//config.Consumer.Fetch.Default = 64 * 1024   // 64 KB, 调低以便及时获取少量消息
	//config.Consumer.Fetch.Max = 4 * 1024 * 1024 // 4 MB
	//config.Consumer.MaxWaitTime = 100 * time.Millisecond
	//config.Consumer.MaxProcessingTime = 100 * time.Millisecond
	//config.ChannelBufferSize = 512 // 增大缓冲区大小
	// 性能优化

	consumer, err := sarama.NewConsumerGroup(c.Config.Brokers, c.Config.GroupID, config)
	if err != nil {
		zap.L().Error("创建kafka消费组失败: ", zap.Error(err))
		return
	}
	defer consumer.Close()

	wg := sync.WaitGroup{}
	wg.Add(1)

	go func() {
		defer wg.Done()
		for {
			if err := consumer.Consume(ctx, []string{c.Config.Topic}, c); err != nil {
				zap.L().Error("异常消费: ", zap.Error(err))
				return
			}
			if ctx.Err() != nil {
				return
			}
		}
	}()

	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt)
	<-sigChan
	zap.L().Debug("Received termination signal, shutting down")

	consumer.Close()
	wg.Wait()
}

func (c *KafkaConsumer) Setup(sarama.ConsumerGroupSession) error {
	return nil
}

func (c *KafkaConsumer) Cleanup(sarama.ConsumerGroupSession) error {
	return nil
}

func (c *KafkaConsumer) ConsumeClaim(session sarama.ConsumerGroupSession, claim sarama.ConsumerGroupClaim) error {
	numGoroutines := 10
	messageChan := make(chan *sarama.ConsumerMessage, numGoroutines)

	var wg sync.WaitGroup

	for i := 0; i < numGoroutines; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			for message := range messageChan {
				customMsg := &Message{
					Key:   message.Key,
					Value: message.Value,
				}
				c.MessageHandler(customMsg, c.Mongo, c.EtcdLock)
				session.MarkMessage(message, "")
			}
		}()
	}

	for message := range claim.Messages() {
		messageChan <- message
	}

	close(messageChan)
	wg.Wait()
	return nil
}

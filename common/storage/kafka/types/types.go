/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/4/9 10:08
*/

package types

import "time"

type KafkaConfig struct {
	Brokers        []string      `json:"brokers"`
	GroupID        string        `json:"groupID"`
	Topic          string        `json:"topic"`
	QueueCapacity  int           `json:"queueCapacity"`
	CommitInterval time.Duration `json:"commitInterval"`
	SessionTimeout time.Duration `json:"sessionTimeout"`
}

/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/4/12 11:06
*/

package conditions

import "go.mongodb.org/mongo-driver/bson"

// 备用
func CreateFilter() Filter {
	return &filter{}
}

type Filter interface {
	Field(k string, v interface{}) bson.D
}
type filter struct {
	filter []bson.D
}

func (f filter) Field(k string, v interface{}) bson.D {
	if len(k) > 0 {
		return bson.D{{k, v}}
	}
	return bson.D{}
}

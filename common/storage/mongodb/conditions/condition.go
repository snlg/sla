/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/4/8 14:33
*/

package conditions

import (
	"go.mongodb.org/mongo-driver/bson"
	"reflect"
)

// 备用

func CreateCondition() Condition {
	return &condition{}
}

type Condition interface {
	Eq(k string, v interface{}) Condition
	Ne(k string, v interface{}) Condition
	Field(k string, v interface{}) bson.E
	Or(v ...interface{}) Condition
	In(k string, v ...interface{}) Condition
	Lt(k string, v interface{}) Condition
	Lte(k string, v interface{}) Condition
	Gt(k string, v interface{}) Condition
	Gte(k string, v interface{}) Condition
	Not(v ...interface{}) Condition
	And(v ...interface{}) Condition
	Nor(v ...interface{}) Condition
	ToBsonD() bson.D
}
type condition struct {
	filter []bson.E
}

func (c *condition) Ne(k string, v interface{}) Condition {
	//TODO implement me
	panic("implement me")
}

func (c *condition) In(k string, v ...interface{}) Condition {
	//TODO implement me
	panic("implement me")
}

//func (c *condition) Lt(k string, v interface{}) Condition {
//	if len(k) > 0 {
//		c.filter = append(c.filter, bson.E{})
//		c.filter[len(c.filter)-1].Key = k
//		c.filter[len(c.filter)-1].Value = bson.E{Key: "$lt", Value: v}
//	}
//	return c
//}

func (c *condition) Lt(k string, v interface{}) Condition {
	if len(k) > 0 {
		c.filter = append(c.filter, bson.E{
			Key:   k,
			Value: bson.D{{"$lt", v}},
		})
	}
	return c
}

func (c *condition) Lte(k string, v interface{}) Condition {
	if len(k) > 0 {
		c.filter = append(c.filter, bson.E{})
		c.filter[len(c.filter)-1].Key = k
		c.filter[len(c.filter)-1].Value = bson.E{Key: "$lte", Value: v}
	}
	return c
}

func (c *condition) Gt(k string, v interface{}) Condition {
	if len(k) > 0 {
		c.filter = append(c.filter, bson.E{})
		c.filter[len(c.filter)-1].Key = k
		c.filter[len(c.filter)-1].Value = bson.E{Key: "$gt", Value: v}
	}
	return c
}

func (c *condition) Gte(k string, v interface{}) Condition {
	if len(k) > 0 {
		c.filter = append(c.filter, bson.E{})
		c.filter[len(c.filter)-1].Key = k
		c.filter[len(c.filter)-1].Value = bson.E{Key: "$gte", Value: v}
	}
	return c
}

func (c *condition) Not(v ...interface{}) Condition {
	//TODO implement me
	panic("implement me")
}

func (c *condition) And(v ...interface{}) Condition {
	ele := []bson.M{}
	for r := range v {
		typeR := reflect.TypeOf(v[r])
		if typeR.String() == "primitive.E" {
			ele = append(ele, bson.M{})
			ele[r] = bson.M{v[r].(bson.E).Key: v[r].(bson.E).Value}
		}
	}
	if len(ele) > 0 {
		c.filter = append(c.filter, bson.E{})
		c.filter[len(c.filter)-1] = bson.E{"$and", ele}
	}
	return c
}

func (c *condition) Nor(v ...interface{}) Condition {
	//TODO implement me
	panic("implement me")
}

func (c *condition) Or(v ...interface{}) Condition {
	ele := []bson.M{}
	for r := range v {
		typeR := reflect.TypeOf(v[r])
		if typeR.String() == "primitive.E" {
			ele = append(ele, bson.M{})
			ele[r] = bson.M{v[r].(bson.E).Key: v[r].(bson.E).Value}
		}
	}
	if len(ele) > 0 {
		c.filter = append(c.filter, bson.E{})
		c.filter[len(c.filter)-1] = bson.E{"$or", ele}
	}
	return c
}

func (c condition) Field(k string, v interface{}) bson.E {
	if len(k) > 0 {
		return bson.E{k, v}
	}
	return bson.E{}
}

func (c *condition) Eq(k string, v interface{}) Condition {
	if len(k) > 0 {
		c.filter = append(c.filter, bson.E{})
		c.filter[len(c.filter)-1].Key = k
		c.filter[len(c.filter)-1].Value = v
	}
	return c

}
func (c condition) ToBsonD() bson.D {
	return c.filter
}

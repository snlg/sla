/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/4/8 14:31
*/

package types

import (
	"context"
	"errors"
)

// Errors defines
var (
	ErrTransactionStated   = errors.New("transaction already started")
	ErrTransactionNotFound = errors.New("not in transaction environment")
	ErrDocumentNotFound    = errors.New("document not found")
	ErrDuplicated          = errors.New("duplicated")
	ErrSessionNotStarted   = errors.New("session is not started")

	ErrUpdateOpAddToSet = "addToSet"
	ErrUpdateOpPull     = "pull"

	ErrDeleteNoParam = errors.New("delete without params is not allowed")
)

type Filter interface{}

// WriteModel 批量写入操作的模型
type WriteModel interface{}

type Table interface {
	Find(filter Filter, opts ...*FindOpts) Find
	Insert(ctx context.Context, docs interface{}) error
	Update(ctx context.Context, filter Filter, docs interface{}) error
	Upsert(ctx context.Context, filter Filter, docs interface{}) error
	DeleteOne(ctx context.Context, filter Filter) error
	Distinct(ctx context.Context, field string, filter Filter) ([]interface{}, error)
	AggregateOne(ctx context.Context, pipeline interface{}, result interface{}) error
	AggregateAll(ctx context.Context, pipeline interface{}, result interface{}, opts ...*AggregateOpts) error
	BulkWrite(ctx context.Context, models []interface{}) (int64, error)
}

type Find interface {
	// Fields 设置查询字段
	Fields(fields ...string) Find
	// Sort 设置查询排序
	Sort(sort string) Find
	// Start 设置限制查询上标
	Start(start uint64) Find
	// Limit 设置查询数量
	Limit(limit uint64) Find
	// All 查询多个
	All(ctx context.Context, result interface{}) error
	// One 查询单个
	One(ctx context.Context, result interface{}) error
	// Count 统计数量(非事务)
	Count(ctx context.Context) (uint64, error)

	Option(opts ...*FindOpts)
}

type FindOpts struct {
	WithObjectID *bool
	WithCount    *bool
}

type AggregateOpts struct {
	AllowDiskUse *bool
}

func NewAggregateOpts() *AggregateOpts {
	return &AggregateOpts{}
}

func (a *AggregateOpts) SetAllowDiskUse(bl bool) *AggregateOpts {
	a.AllowDiskUse = &bl
	return a
}

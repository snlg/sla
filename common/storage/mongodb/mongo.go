/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/4/8 14:38
*/

package mongodb

import (
	"context"
	"errors"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/mongo/driver/connstring"
	"reflect"
	"sla/common/storage/mongodb/types"
	"time"
)

type MongoDB interface {
	Close() error
}

type Mongo struct {
	dbc    *mongo.Client
	dbname string
}

type Collection struct {
	collName string
	*Mongo
}

func (c *Collection) BulkWrite(ctx context.Context, models []interface{}) (int64, error) {
	var mongoModels []mongo.WriteModel
	for _, m := range models {
		// 将接口转换为 map[string]interface{} 类型
		model, ok := m.(map[string]interface{})
		if !ok {
			return 0, errors.New("类型不正确, 必须是 map[string]interface{}")
		}

		// 提取过滤条件和更新操作
		filter, ok := model["filter"].(map[string]interface{})
		if !ok {
			return 0, errors.New("filter类型不正确, 必须是 map[string]interface{}")
		}

		update, ok := model["update"].(map[string]interface{})
		if !ok {
			return 0, errors.New("update类型不正确, 必须是 map[string]interface{}")
		}

		upsert, ok := model["upsert"].(bool)
		if !ok {
			return 0, errors.New("upsert类型不正确, 必须是 bool")
		}

		// 创建 mongo.WriteModel 对象
		updateModel := mongo.NewUpdateOneModel()
		updateModel.SetFilter(filter)
		updateModel.SetUpdate(update)
		updateModel.SetUpsert(upsert) // 设置为 upsert 模式

		// 添加到 mongoModels 中
		mongoModels = append(mongoModels, updateModel)
	}

	// 执行 bulkWrite 操作
	result, err := c.dbc.Database(c.dbname).Collection(c.collName).BulkWrite(ctx, mongoModels)
	if err != nil {
		return 0, err
	}
	return result.InsertedCount + result.UpsertedCount + result.ModifiedCount, nil
}

func (c *Collection) AggregateAll(ctx context.Context, pipeline interface{}, result interface{}, opts ...*types.AggregateOpts) error {

	var aggregateOption *options.AggregateOptions
	for _, opt := range opts {
		if opt == nil {
			continue
		}
		if opt.AllowDiskUse != nil {
			aggregateOption = &options.AggregateOptions{AllowDiskUse: opt.AllowDiskUse}
		}
	}

	opt := getCollectionOption(ctx)
	cursor, err := c.dbc.Database(c.dbname).Collection(c.collName, opt).Aggregate(ctx, pipeline, aggregateOption)
	defer cursor.Close(ctx)
	if err != nil {
		return err
	}
	return decodeCursorIntoSlice(ctx, cursor, result)

}

func decodeCursorIntoSlice(ctx context.Context, cursor *mongo.Cursor, result interface{}) error {
	resultv := reflect.ValueOf(result)
	if resultv.Kind() != reflect.Ptr || resultv.Elem().Kind() != reflect.Slice {
		return errors.New("result argument must be a slice address")
	}

	elemt := resultv.Elem().Type().Elem()
	slice := reflect.MakeSlice(resultv.Elem().Type(), 0, 10)
	for cursor.Next(ctx) {
		elemp := reflect.New(elemt)
		if err := cursor.Decode(elemp.Interface()); nil != err {
			return err
		}
		slice = reflect.Append(slice, elemp.Elem())
	}
	if err := cursor.Err(); err != nil {
		return err
	}

	resultv.Elem().Set(slice)
	return nil
}

func (c *Collection) AggregateOne(ctx context.Context, pipeline interface{}, result interface{}) error {

	opt := getCollectionOption(ctx)
	cursor, err := c.dbc.Database(c.dbname).Collection(c.collName, opt).Aggregate(ctx, pipeline)
	defer cursor.Close(ctx)
	if err != nil {
		return err
	}
	for cursor.Next(ctx) {
		return cursor.Decode(result)
	}
	return types.ErrDocumentNotFound
}

func (c *Collection) Distinct(ctx context.Context, field string, filter types.Filter) ([]interface{}, error) {

	if filter == nil {
		filter = bson.M{}
	}

	opt := getCollectionOption(ctx)
	var results []interface{} = nil
	var err error
	results, err = c.dbc.Database(c.dbname).Collection(c.collName, opt).Distinct(ctx, field, filter)
	return results, err
}

type Find struct {
	*Collection

	projection map[string]int
	filter     types.Filter
	start      int64
	limit      int64
	sort       bson.D

	option types.FindOpts
}

func (f *Find) Fields(fields ...string) types.Find {
	for _, field := range fields {
		if len(field) <= 0 {
			continue
		}
		f.projection[field] = 1
	}
	return f
}

func (f *Find) Sort(sort string) types.Find {
	//TODO implement me
	panic("implement me")
}

func (f *Find) Start(start uint64) types.Find {
	//TODO implement me
	panic("implement me")
}

func (f *Find) Limit(limit uint64) types.Find {
	//TODO implement me
	panic("implement me")
}

func (f *Find) All(ctx context.Context, result interface{}) error {
	findOpts := f.generateMongoOption()
	if f.filter == nil {
		f.filter = bson.M{}
	}
	opt := getCollectionOption(ctx)
	cursor, err := f.dbc.Database(f.dbname).Collection(f.collName, opt).Find(ctx, f.filter, findOpts)
	if err != nil {
		return err
	}
	return cursor.All(ctx, result)
}

func (f *Find) One(ctx context.Context, result interface{}) error {
	findOpts := f.generateMongoOption()
	if f.filter == nil {
		f.filter = bson.M{}
	}
	opt := getCollectionOption(ctx)
	cursor, err := f.dbc.Database(f.dbname).Collection(f.collName, opt).Find(ctx, f.filter, findOpts)
	if err != nil {
		return err
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		return cursor.Decode(result)
	}
	return types.ErrDocumentNotFound

}

func (f *Find) Count(ctx context.Context) (uint64, error) {
	//TODO implement me
	panic("implement me")
}

func (c *Collection) Find(filter types.Filter, opts ...*types.FindOpts) types.Find {
	find := &Find{
		Collection: c,
		filter:     filter,
		projection: make(map[string]int),
	}
	find.Option(opts...)
	return find
}

func (c *Collection) Insert(ctx context.Context, docs interface{}) error {
	rows := ConverToInterfaceSlice(docs)

	_, err := c.dbc.Database(c.dbname).Collection(c.collName).InsertMany(ctx, rows)
	if err != nil {
		return err
	}
	return nil
}

func ConverToInterfaceSlice(value interface{}) []interface{} {
	rflVal := reflect.ValueOf(value)
	for rflVal.CanAddr() {
		rflVal = rflVal.Elem()
	}
	if rflVal.Kind() != reflect.Slice {
		return []interface{}{value}
	}

	result := make([]interface{}, 0)
	for i := 0; i < rflVal.Len(); i++ {
		if rflVal.Index(i).CanInterface() {
			result = append(result, rflVal.Index(i).Interface())
		}
	}

	return result
}
func (c *Collection) Update(ctx context.Context, filter types.Filter, docs interface{}) error {

	if filter == nil {
		filter = bson.M{}
	}

	data := bson.M{"$set": docs}
	_, err := c.dbc.Database(c.dbname).Collection(c.collName).UpdateMany(ctx, filter, data)
	if err != nil {
		return err
	}
	return nil
}

func (c *Collection) Upsert(ctx context.Context, filter types.Filter, docs interface{}) error {

	doUpsert := true
	replaceOpt := &options.UpdateOptions{
		Upsert: &doUpsert,
	}
	data := bson.M{"$set": docs}
	_, err := c.dbc.Database(c.dbname).Collection(c.collName).UpdateOne(ctx, filter, data, replaceOpt)
	if err != nil {
		return err
	}

	//mongodb调试下使用
	//需要将_, err 改成 i, err
	//zap.L().Debug("UpsertedCount:" + strconv.FormatInt(i.UpsertedCount, 10) + ", ModifiedCount:" + strconv.FormatInt(i.ModifiedCount, 10) + ", MatchedCount:" + strconv.FormatInt(i.MatchedCount, 10))
	return nil
}

func (c *Collection) DeleteOne(ctx context.Context, filter types.Filter) error {
	// set upsert option
	if filter == nil {
		return types.ErrDeleteNoParam
	}

	_, err := c.dbc.Database(c.dbname).Collection(c.collName).DeleteOne(ctx, filter)

	if err != nil {
		return err
	}
	return nil
}

func (m *Mongo) FindOne(ctx context.Context, result interface{}) error {
	//TODO implement me
	c, err := m.dbc.Database(m.dbname).Collection("users").Find(ctx, bson.D{})
	if err != nil {
		return err
	}
	defer c.Close(ctx)
	for c.Next(ctx) {
		err = c.Decode(result)
		if err != nil {
			return err
		}
		return nil
	}
	return nil
}

func (c *Mongo) Close() error {
	c.dbc.Disconnect(context.TODO())
	return nil
}

// MongoConf TODO
type MongoConf struct {
	TimeoutSeconds int
	MaxOpenConns   uint64
	MaxIdleConns   uint64
	URI            string
	RsName         string
	SocketTimeout  int
}

func NewMgo(config MongoConf, timeout time.Duration) (*Mongo, error) {
	connStr, err := connstring.Parse(config.URI)
	if nil != err {
		return nil, err
	}
	if config.RsName == "" {
		return nil, fmt.Errorf("mongodb rsName not set")
	}
	socketTimeout := time.Second * time.Duration(config.SocketTimeout)
	maxConnIdleTime := 25 * time.Minute
	appName := "SLA"

	disableWriteRetry := false
	conOpt := options.ClientOptions{
		MaxPoolSize:    &config.MaxOpenConns,
		MinPoolSize:    &config.MaxIdleConns,
		ConnectTimeout: &timeout,
		SocketTimeout:  &socketTimeout,
		//ReplicaSet:      &config.RsName,
		RetryWrites:     &disableWriteRetry,
		MaxConnIdleTime: &maxConnIdleTime,
		AppName:         &appName,
	}

	client, err := mongo.NewClient(options.Client().ApplyURI(config.URI), &conOpt)
	if nil != err {
		return nil, err
	}

	if err := client.Connect(context.TODO()); nil != err {
		return nil, err
	}

	return &Mongo{
		dbc:    client,
		dbname: connStr.Database,
	}, nil
}
func (c *Mongo) Table(collName string) types.Table {
	col := Collection{}
	col.collName = collName
	col.Mongo = c
	return &col
}

func (f *Find) Option(opts ...*types.FindOpts) {
	for _, opt := range opts {
		if opt == nil {
			continue
		}
		if opt.WithObjectID != nil {
			f.option.WithObjectID = opt.WithObjectID
		}
		if opt.WithCount != nil {
			f.option.WithCount = opt.WithCount
		}
	}
}
func (f *Find) generateMongoOption() *options.FindOptions {
	findOpts := &options.FindOptions{}
	if f.projection == nil {
		f.projection = make(map[string]int, 0)
	}
	if f.option.WithObjectID != nil && *f.option.WithObjectID {
		// mongodb 要求，当有字段设置未1, 不设置都不显示
		// 没有设置projection 的时候，返回所有字段
		if len(f.projection) > 0 {
			f.projection["_id"] = 1
		}
	} else {
		if _, exists := f.projection["_id"]; !exists {
			f.projection["_id"] = 0
		}
	}
	if len(f.projection) != 0 {
		findOpts.Projection = f.projection
	}

	if f.start != 0 {
		findOpts.SetSkip(f.start)
	}
	if f.limit != 0 {
		findOpts.SetLimit(f.limit)
	}
	if len(f.sort) != 0 {
		findOpts.SetSort(f.sort)
	}

	return findOpts
}
func getCollectionOption(ctx context.Context) *options.CollectionOptions {
	var opt *options.CollectionOptions
	return opt
}

/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/4/15 9:48
*/

package elasticsearch

import (
	"context"
	"encoding/json"
	"errors"
	"github.com/elastic/go-elasticsearch/v7"
	"github.com/elastic/go-elasticsearch/v7/esapi"
	"log"
	"strings"
	"time"
)

type ElasticClient struct {
	client *elasticsearch.Client
}

func NewElasticClient(monitor MonitorES) (*ElasticClient, error) {
	cfg := elasticsearch.Config{
		Addresses: monitor.Address,
		Username:  monitor.Username,
		Password:  monitor.Password,
	}

	es, err := elasticsearch.NewClient(cfg)
	if err != nil {
		return nil, err
	}

	return &ElasticClient{
		client: es,
	}, nil
}

func (ec *ElasticClient) DeleteDocumentsByTimeRange(index string, startTime, endTime int64) error {
	query := map[string]interface{}{
		"query": map[string]interface{}{
			"range": map[string]interface{}{
				"@timestamp": map[string]interface{}{
					"gte": time.Unix(startTime, 0).Format(time.RFC3339),
					"lte": time.Unix(endTime, 0).Format(time.RFC3339),
				},
			},
		},
	}

	queryJSON, err := json.Marshal(query)
	if err != nil {
		return err
	}

	return ec.deleteDocumentsByQuery(index, string(queryJSON))
}

func (ec *ElasticClient) deleteDocumentsByQuery(index string, query string) error {
	req := esapi.DeleteByQueryRequest{
		Index: []string{index},
		Body:  strings.NewReader(query),
	}

	res, err := req.Do(context.Background(), ec.client)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	if res.IsError() {
		return errors.New(res.String())
	}

	return nil
}

func (ec *ElasticClient) IndexDocumentsInBatches(index string, documents []map[string]interface{}, batchSize int) error {
	totalDocs := len(documents)
	for i := 0; i < totalDocs; i += batchSize {
		end := i + batchSize
		if end > totalDocs {
			end = totalDocs
		}
		batchDocs := documents[i:end]
		if err := ec.indexBatch(index, batchDocs); err != nil {
			return err
		}
	}
	return nil
}

func (ec *ElasticClient) indexBatch(index string, batchDocs []map[string]interface{}) error {
	var bulkStrings []string

	for _, doc := range batchDocs {
		var metaData []byte
		var err error

		if id, ok := doc["id"].(string); ok && id != "" {
			delete(doc, "id")
			metaData, err = json.Marshal(map[string]interface{}{
				"index": map[string]interface{}{
					"_index": index,
					"_id":    id,
				},
			})
		} else {
			metaData, err = json.Marshal(map[string]interface{}{
				"index": map[string]interface{}{
					"_index": index,
				},
			})
		}
		if err != nil {
			return err
		}

		data, err := json.Marshal(doc)
		if err != nil {
			return err
		}

		bulkStrings = append(bulkStrings, string(metaData))
		bulkStrings = append(bulkStrings, string(data))
	}

	bulkRequestBody := strings.Join(bulkStrings, "\n") + "\n"

	bulkReq := esapi.BulkRequest{
		Body: strings.NewReader(bulkRequestBody),
	}
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	res, err := bulkReq.Do(ctx, ec.client)
	if err != nil {
		return err
	}

	if res.IsError() {
		log.Printf("[%s] 批量索引文档时出错", res.Status())
		return errors.New(res.Status())
	}

	return nil
}

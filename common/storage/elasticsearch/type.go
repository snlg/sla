/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/4/15 9:48
*/

package elasticsearch

type MonitorES struct {
	Address     []string `json:"address"`
	Username    string   `json:"Username"`
	Password    string   `json:"Password"`
	SLAIndex    string   `json:"SLAIndex"`
	DetailIndex string   `json:"DetailIndex"`
	GroupIndex  string   `json:"GroupIndex"`
}

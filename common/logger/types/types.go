/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/4/8 14:21
*/

package types

type LogConfig struct {
	Level      string `json:"level"`
	Filename   string `json:"filename"`
	MaxSize    int    `json:"maxsize"`
	MaxAge     int    `json:"maxage"`
	MaxBackups int    `json:"maxbackups"`
	Compress   bool   `json:"compress"`
}

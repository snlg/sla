package router

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"sla/common/logger"
	"sla/services"
)

func SetupRouter(service service.Service) *gin.Engine {
	router := gin.Default()
	router.Use(cors.Default())
	router.Use(logger.GinLogger(), logger.GinRecovery(true))

	userRouter := router.Group("/admin/api/v1/jobs")
	{
		userRouter.POST("/sla/recalculate", service.CalculateSLA)
		userRouter.POST("/sla/es/push", service.SendSLAToES)
		userRouter.POST("/sla/details/es/push", service.SendSLADetailToES)
	}

	apiRouter := router.Group("/api/v1/")
	{
		apiRouter.GET("/sla/details", service.GetSLADetailByTimestamp)
		apiRouter.GET("/sla/list", service.GetSLAsByType)
	}

	index := router.Group("/")
	{
		index.Any("", service.HelloSLA)
	}

	return router
}

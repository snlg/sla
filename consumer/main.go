/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/4/8 10:40
*/

package main

import (
	"context"
	"encoding/json"
	"fmt"
	"go.uber.org/zap"
	"os"
	"os/signal"
	"regexp"
	"sla/common/config"
	log "sla/common/logger"
	"sla/common/storage/kafka"
	"sla/common/storage/mongodb"
	"sla/common/tools"
	"sla/common/transaction"
	"sla/public/metaData"
	p "sla/public/parameters"
	"syscall"
	"time"
)

var parameters *p.Parameters
var filters *metaData.Filters
var reg []*regexp.Regexp

func handleMessage(msg *kafka.Message, mongo *mongodb.Mongo, etcd *transaction.EtcdLock) {
	start := time.Now()
	defer func() {
		zap.L().Debug("handleMessage总耗时", zap.Any("during", time.Since(start)))
	}()
	logger := zap.L()
	uuid := tools.GetUUID()
	logger = logger.With(zap.String("uuid", uuid))

	logger.Info("开始处理消息", zap.Any("消息", msg.Value))
	var entry metaData.MessageEntry

	err := json.Unmarshal(msg.Value, &entry)
	if err != nil {
		logger.Error("解析JSON失败", zap.Any("error", err))
		return
	}
	logger.Info("序列化数据", zap.Any("消息", entry))

	if filters.IsMatch(entry) {
		logger.Debug("命中规则")
		return
	}
	logger.Debug("未命中规则")
	m := metaData.CreateMetaData(entry.Province, mongo)
	m.SetUUID(uuid)
	m.UpdateParameters(parameters)
	m.UpdateReCache(reg)
	if entry.Province == p.SLATypeSMS {
		m.(metaData.MetaSMSData).InitByEntry(entry).UpdateUrl()
		err = m.(metaData.MetaSMSData).SetID()
	} else {
		m.(metaData.MetaData).InitByEntry(entry).UpdateUrl()
		err = m.SetID()
	}
	if err != nil {
		logger.Error("SetID失败", zap.Any("error", err))
		return
	}

	if m.GetID() != "" && m.InList() {
		for {
			startTime := time.Now()
			logger.Debug("开始获取lock...")

			mutex, leaseResp, err := etcd.Lock(m.GetContext(), m.GetID(), 5*time.Second)

			if err != nil {
				logger.Error("访问etcd（lock）失败", zap.Any("error", err))
				// 增加重试间隔
				time.Sleep(1 * time.Second)
				continue // 重试
			}

			endTime := time.Now()
			elapsedTime := endTime.Sub(startTime).String()
			logger.Debug("lock成功", zap.String("elapsedTime", elapsedTime))

			if entry.Province == p.SLATypeSMS {
				err = m.(metaData.MetaSMSData).GetDataFromMongoByID().Append(entry).Write()
			} else {
				err = m.(metaData.MetaData).GetDataFromMongoByID().Append(entry).Write()
			}
			if err != nil {
				logger.Error("Write失败", zap.Any("error", err))
				return
			}
			logger.Debug("消息写库成功")
			//避免在for循环中使用defer，导致资源泄露，内存溢出的bug
			if err := etcd.Unlock(mutex, leaseResp); err != nil {
				logger.Error("访问etcd（unlock）失败", zap.Any("error", err))
			} else {
				logger.Debug("unlock成功")
			}
			logger.Info("消息处理成功")
			break // 成功获取锁并写入数据后退出循环
		}

	}
}

func LoadToMemory(timer *time.Timer, mongo *mongodb.Mongo, interval time.Duration) {
	for {
		<-timer.C
		parameters = p.LoadConfig(mongo)
		timer.Reset(interval)
	}
}

func LoadFilters(timer *time.Timer, mongo *mongodb.Mongo, interval time.Duration) {
	for {
		<-timer.C
		filters = metaData.CreateFilter(mongo).GetFilters()
		timer.Reset(interval)
	}
}

func LoadRegex(timer *time.Timer, parameters p.Parameters, interval time.Duration) {
	for {
		<-timer.C
		reg = tools.InitRegexpCache(metaData.ExtractUrls(parameters.Urls))
		timer.Reset(interval)
	}
}

func main() {
	c := config.CreateConfig().GetConfig()
	err := log.InitLogger(&c.Log)
	if err != nil {
		fmt.Println("*** 无法加载日志模块 ***")
		return
	}
	zap.L().Info("加载日志模块成功")

	mConf := c.Mongo
	mgConf := mongodb.MongoConf{
		TimeoutSeconds: 0,
		MaxOpenConns:   0,
		MaxIdleConns:   0,
		URI:            mConf.BuildURI(),
		RsName:         mConf.RsName,
		SocketTimeout:  0,
	}
	m, err := mongodb.NewMgo(mgConf, time.Second*15)
	if err != nil {
		zap.L().Error(err.Error())
		return
	}

	etcdLock, err := transaction.NewEtcdLock(c.Etcd.EndPoints, c.Etcd.Timeout*time.Second)
	//dl, err := transaction.NewDistributedLock(c.Etcd.EndPoints, c.Etcd.Timeout*time.Second)
	if err != nil {
		zap.L().Error("连接etcd失败," + err.Error())
		return
	}

	parameters = p.LoadConfig(m)
	filters = metaData.CreateFilter(m).GetFilters()
	reg = tools.InitRegexpCache(metaData.ExtractUrls(parameters.Urls))
	//定时已刷新内存配置
	interval := 5 * time.Minute
	timer := time.NewTimer(interval)
	timerFilters := time.NewTimer(interval)
	timerRegex := time.NewTimer(interval)
	go LoadToMemory(timer, m, interval)
	go LoadFilters(timerFilters, m, interval)
	go LoadRegex(timerRegex, *parameters, interval)
	// 获取kafka配置
	//kafkaConfig := &kafka.Config{}
	//consumer := kafka.NewKafkaConsumer(kafkaConfig.NewKafkaConfig(c.Kafka), handleMessage)

	kafkaConfig := &kafka.Config{
		Brokers:        c.Kafka.Brokers,
		Topic:          c.Kafka.Topic,
		GroupID:        c.Kafka.GroupID,
		QueueCapacity:  c.Kafka.QueueCapacity,
		CommitInterval: c.Kafka.CommitInterval,
		SessionTimeout: c.Kafka.SessionTimeout,
	}
	consumer := kafka.NewKafkaConsumer(kafkaConfig, handleMessage, m, etcdLock)

	ctx, cancel := context.WithCancel(context.Background())

	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		<-sigChan
		cancel()
	}()

	consumer.ConsumeMessages(ctx)
	//consumer.ConsumeMessages(ctx, m, etcdLock)
}

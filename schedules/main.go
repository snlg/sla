/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/4/8 10:41
*/

package main

import (
	"fmt"
	"github.com/robfig/cron"
	"go.uber.org/zap"
	"os"
	"os/signal"
	"sla/common/config"
	log "sla/common/logger"
	"sla/common/storage/elasticsearch"
	"sla/common/storage/mongodb"
	"sla/common/tools"
	p "sla/public/parameters"
	sla "sla/public/sla/Display"
	"syscall"
	"time"
)

var parameters *p.Parameters

func LoadToMemory(timer *time.Timer, mongo *mongodb.Mongo, interval time.Duration) {
	for {
		<-timer.C
		parameters = p.LoadConfig(mongo)
		timer.Reset(interval)
	}
}

func execute(mongo *mongodb.Mongo, conf config.Cron, es elasticsearch.MonitorES, slaalert config.Alert) {
	parameters = p.LoadConfig(mongo)

	interval := 5 * time.Minute
	timer := time.NewTimer(interval)
	go LoadToMemory(timer, mongo, interval)

	uuid := tools.GetUUID()
	logger := zap.L().With(zap.String("uuid", uuid))

	c := cron.New()

	scheduleTask(c, conf.Interval.CalSLA, "执行sla生成定时任务", logger, func() {
		cs := sla.CreateStand(mongo, parameters)
		cs.GeneratorRecentSLA()
	})

	scheduleTask(c, conf.Interval.Alert, "执行sla告警定时任务", logger, func() {
		cs := sla.CreateStand(mongo, parameters)
		cs.Alert(slaalert)
	})

	scheduleTask(c, conf.Interval.WriteES, "执行sla写入ES定时任务", logger, func() {
		cs := sla.CreateStand(mongo, parameters)
		cs.SetES(es).GetRecentSLA()
	})

	scheduleTask(c, conf.Interval.DetailToES, "执行sla明细写入ES定时任务", logger, func() {
		cs := sla.CreateStand(mongo, parameters)
		cs.SetES(es).GetRecentSLADetail()
	})
	//cs := sla.CreateStand(mongo, parameters)
	//cs.SetES(es).GetRecentSLADetail()
	scheduleTask(c, conf.Interval.RefreshRef, "执行刷新Ref定时任务", logger, func() {
		if err := p.RefreshRefs(mongo, *parameters); err != nil {
			logger.Error("刷新Ref失败: " + err.Error())
		}

	})

	scheduleTask(c, conf.Interval.SMSRpmToES, "执行SMS请求量写入ES定时任务", logger, func() {
		cs := sla.CreateStand(mongo, parameters)
		cs.SetES(es).GetRPMByGroup()
	})
	c.Start()

	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, syscall.SIGINT, syscall.SIGTERM)
	<-signalChan

	c.Stop()
}

func scheduleTask(c *cron.Cron, interval string, taskDescription string, logger *zap.Logger, task func()) {
	err := c.AddFunc(interval, func() {
		logger.Info(taskDescription + "...")
		task()
		logger.Info(taskDescription + "完成")
	})
	if err != nil {
		logger.Error("无法添加到调度器", zap.Any("error", err))
	}
}

func main() {
	c := config.CreateConfig().GetConfig()
	err := log.InitLogger(&c.Log)
	if err != nil {
		fmt.Println("*** 无法加载日志模块 ***")
		return
	}
	zap.L().Info("加载日志模块成功")

	mConf := c.Mongo
	mgConf := mongodb.MongoConf{
		TimeoutSeconds: 0,
		MaxOpenConns:   0,
		MaxIdleConns:   0,
		URI:            mConf.BuildURI(),
		RsName:         mConf.RsName,
		SocketTimeout:  0,
	}
	m, err := mongodb.NewMgo(mgConf, time.Second*15)
	if err != nil {
		zap.L().Error(err.Error())
		return
	}

	execute(m, c.Cron, c.MonitorES, c.SLAAlertConfig)
}

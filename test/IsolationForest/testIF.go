package main

import (
	"fmt"
	"math"
	"math/rand"
	"time"
)

// IsolationTree represents a single tree in the Isolation Forest.
type IsolationTree struct {
	Feature     int     // The feature index used for splitting
	SplitValue  float64 // The split value
	Left, Right *IsolationTree
	Size        int // Number of samples in this node
}

// FitIsolationTree builds an IsolationTree recursively.
func FitIsolationTree(data [][]float64, depth, maxDepth int) *IsolationTree {
	if len(data) == 0 || depth >= maxDepth {
		return &IsolationTree{Size: len(data)}
	}

	// Randomly choose a feature and split value
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	feature := r.Intn(len(data[0]))
	minVal, maxVal := data[0][feature], data[0][feature]
	for _, row := range data {
		if row[feature] < minVal {
			minVal = row[feature]
		}
		if row[feature] > maxVal {
			maxVal = row[feature]
		}
	}

	if minVal == maxVal {
		return &IsolationTree{Size: len(data)}
	}

	splitValue := minVal + r.Float64()*(maxVal-minVal)

	// Split data
	left, right := [][]float64{}, [][]float64{}
	for _, row := range data {
		if row[feature] < splitValue {
			left = append(left, row)
		} else {
			right = append(right, row)
		}
	}

	return &IsolationTree{
		Feature:    feature,
		SplitValue: splitValue,
		Left:       FitIsolationTree(left, depth+1, maxDepth),
		Right:      FitIsolationTree(right, depth+1, maxDepth),
		Size:       len(data),
	}
}

// PathLength computes the path length for a sample in the tree.
func (tree *IsolationTree) PathLength(sample []float64) float64 {
	if tree.Left == nil && tree.Right == nil {
		if tree.Size <= 1 {
			return 0
		}
		return float64(tree.Size)
	}

	if sample[tree.Feature] < tree.SplitValue {
		return 1 + tree.Left.PathLength(sample)
	}
	return 1 + tree.Right.PathLength(sample)
}

// IsolationForest represents the entire forest of trees.
type IsolationForest struct {
	Trees    []*IsolationTree
	MaxDepth int
}

// Fit builds an Isolation Forest with the given data.
func (forest *IsolationForest) Fit(data [][]float64, numTrees int) {
	forest.Trees = make([]*IsolationTree, numTrees)
	forest.MaxDepth = int(2 * math.Log2(float64(len(data))))

	for i := 0; i < numTrees; i++ {
		// Randomly sample data for each tree
		sample := SampleData(data, len(data)/2)
		forest.Trees[i] = FitIsolationTree(sample, 0, forest.MaxDepth)
	}
}

// Predict computes the anomaly score for a sample.
func (forest *IsolationForest) Predict(sample []float64) float64 {
	pathLengths := 0.0
	for _, tree := range forest.Trees {
		pathLengths += tree.PathLength(sample)
	}
	avgPathLength := pathLengths / float64(len(forest.Trees))

	// Convert path length to anomaly score
	c := 2 * (math.Log2(float64(len(sample))) - (float64(len(sample))-1)/float64(len(sample)))
	return math.Pow(2, -avgPathLength/c)
}

// SampleData samples a subset of data.
func SampleData(data [][]float64, size int) [][]float64 {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	sample := make([][]float64, size)
	for i := 0; i < size; i++ {
		sample[i] = data[r.Intn(len(data))]
	}
	return sample
}

func main() {
	// Example data: [statusCode_ratio, avg_bytes, avg_response_time, request_count]
	data := [][]float64{
		{0.1, 1345, 250, 1200},
		{0.1, 1345, 250, 1200},
		{0.1, 1345, 250, 1200},
		{0.1, 1345, 250, 1200}, // Anomalous point
	}

	// Train Isolation Forest
	forest := &IsolationForest{}
	numTrees := 100
	forest.Fit(data, numTrees)

	// Predict anomaly scores
	newData := []float64{0.1, 1345, 250, 1200} // Test point
	score := forest.Predict(newData)
	fmt.Printf("Anomaly Score: %.2f\n", score)

	if score > 0.7 {
		fmt.Println("Anomaly detected!")
	} else {
		fmt.Println("Data is normal.")
	}
}

/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/5/29 15:43
*/

package main

import (
	"fmt"
	"regexp"
)

//
//func FindMatches(arr []string, target string) ([]string, int, bool) {
//	var matches []string
//	var maxMatchLength int
//	var bestMatchIndex int = -1
//	var bestMatchProportion float64
//
//	for _, pattern := range arr {
//		re, err := regexp.Compile(pattern)
//		if err != nil {
//			continue
//		}
//		match := re.FindString(target)
//		if match != "" {
//			matches = append(matches, pattern)
//			matchLength := len(match)
//			patternLength := len(pattern)
//			matchProportion := float64(matchLength) / float64(patternLength)
//
//			if matchLength > maxMatchLength || (matchLength == maxMatchLength && matchProportion > bestMatchProportion) {
//				maxMatchLength = matchLength
//				bestMatchProportion = matchProportion
//				bestMatchIndex = len(matches) - 1
//			}
//		}
//	}
//
//	return matches, bestMatchIndex, len(matches) > 0
//}

var reCache []*regexp.Regexp

func initRegexpCache(arr []string) {
	// 预编译正则表达式并缓存
	reCache = make([]*regexp.Regexp, len(arr))
	for i, pattern := range arr {
		re, err := regexp.Compile(pattern)
		if err != nil {
			continue
		}
		reCache[i] = re
	}
}

func FindMatches(target string) ([]string, int, bool) {
	var matches []string
	var maxMatchLength int
	var bestMatchIndex = 0
	var bestMatchProportion float64

	for i, re := range reCache {
		if re == nil {
			continue
		}
		match := re.FindString(target)
		if match != "" {
			matches = append(matches, reCache[i].String())
			matchLength := len(match)
			patternLength := len(reCache[i].String())
			matchProportion := float64(patternLength) / float64(matchLength)

			if matchLength > maxMatchLength || (matchLength == maxMatchLength && matchProportion > bestMatchProportion) {
				maxMatchLength = matchLength
				bestMatchProportion = matchProportion
				bestMatchIndex = len(matches) - 1
			}
		}
	}

	return matches, bestMatchIndex, len(matches) > 0
}

func main() {
	//arr := []string{`/v3/cloudpay/queryPrice`, `/vas-api/v3/cloudpay/queryPrice`, `/v3/cloudpay/queryPrice`}
	//target := "/vas-api/v3/cloudpay/queryPrice"
	//arr := []string{`^/images/$`, `.*/html/index_entry.html$`}
	//arr := []string{"^/ysten-bus.*.m3u8$"}
	//target := "/ysten-businessmobile/live/SD-4000k-576P-fengshanggw/1.m3u8"
	//arr := []string{"^/ysten-bus.*\\*$"}
	//target := "/ysten-business/live/HD-4000k-1080P-jiangsustv/*"
	arr := []string{"^/OOS/newapktc/js/config.js$", ".*/config.js$"}
	target := "/OOS/newapktc/js/config.js"
	initRegexpCache(arr)
	matches, bestMatchIndex, found := FindMatches(target)
	fmt.Println("Matches:", matches)
	fmt.Println("Best Match Index:", bestMatchIndex)
	fmt.Println("Found:", found)
}

/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/4/30 11:09
*/
package main

import (
	"fmt"
	"time"
)

type Counter struct {
	count int
}

func (c *Counter) Increment() {
	c.count++
}

func (c *Counter) GetCount() int {
	return c.count
}

func printRunning(timer *time.Timer, interval time.Duration, counter *Counter) {
	for {
		<-timer.C
		counter.Increment()
		fmt.Printf("x: %d\n", counter.GetCount())
		timer.Reset(interval)
	}
}

func main() {
	interval := 5 * time.Second
	timer := time.NewTimer(interval)
	counter := &Counter{}
	go printRunning(timer, interval, counter)

	// 主函数进行其他操作，例如执行 cron 任务
	for {
		time.Sleep(1 * time.Second)
		fmt.Printf("Main function is still running... x: %d\n", counter.GetCount())
		// 执行其他操作
	}
}

/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/4/26 15:01
*/

package main

import "fmt"

func main() {
	// 示例数据
	valueData := []float64{
		0.027602750190985494,
		0.02545454545454545,
		0.026019839041736854,
	}

	// 计算整个周期的平均值
	var sum float64
	for _, value := range valueData {
		sum += value
	}
	average := sum / float64(len(valueData))

	// 获取指定时间点的因变量值
	currentValue := valueData[len(valueData)-1]

	// 计算变化率
	changeRate := (currentValue - average) / average * 100

	// 打印结果
	fmt.Printf("变化率: %.2f%%\n", changeRate)
}

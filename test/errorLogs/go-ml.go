/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/12/31 21:16
*/

package main

//
//import (
//	"fmt"
//	"log"
//
//	"gorgonia.org/gorgonia"
//	"gorgonia.org/tensor"
//)
//
//// trainModel 训练逻辑，返回训练好的模型节点
//func trainModel(features [][]float64, labels []int, inputDim, outputDim, epochs int) (*gorgonia.Node, error) {
//	g := gorgonia.NewGraph()
//
//	// 定义输入和目标张量
//	input := gorgonia.NewMatrix(g, tensor.Float64, gorgonia.WithShape(len(features), inputDim), gorgonia.WithName("input"))
//	target := gorgonia.NewMatrix(g, tensor.Float64, gorgonia.WithShape(len(labels), outputDim), gorgonia.WithName("target"))
//
//	// 初始化权重和偏置，标记为可学习
//	weights := gorgonia.NewMatrix(g, tensor.Float64, gorgonia.WithShape(inputDim, outputDim), gorgonia.WithName("weights"), gorgonia.WithInit(gorgonia.GlorotU(1.0)))
//	bias := gorgonia.NewVector(g, tensor.Float64, gorgonia.WithShape(outputDim), gorgonia.WithName("bias"), gorgonia.WithInit(gorgonia.Zeroes()))
//
//	// 将权重和偏置标记为可训练
//	gorgonia.Read(weights, weights)
//	gorgonia.Read(bias, bias)
//
//	// 定义输出：线性模型 output = input * weights + bias
//	predicted := gorgonia.Must(gorgonia.BroadcastAdd(gorgonia.Must(gorgonia.Mul(input, weights)), bias, nil, []byte{0}))
//
//	// 定义损失函数（均方误差）
//	loss := gorgonia.Must(gorgonia.Mean(gorgonia.Must(gorgonia.Square(gorgonia.Must(gorgonia.Sub(predicted, target))))))
//
//	// 准备训练数据
//	featureTensor := tensor.New(tensor.WithShape(len(features), inputDim), tensor.Of(tensor.Float64), tensor.WithBacking(flatten(features)))
//	labelTensor := tensor.New(tensor.WithShape(len(labels), outputDim), tensor.Of(tensor.Float64), tensor.WithBacking(oneHot(labels, outputDim)))
//
//	// 将数据绑定到计算图
//	if err := gorgonia.Let(input, featureTensor); err != nil {
//		return nil, fmt.Errorf("failed to bind input data: %v", err)
//	}
//	if err := gorgonia.Let(target, labelTensor); err != nil {
//		return nil, fmt.Errorf("failed to bind target data: %v", err)
//	}
//
//	// 创建 VM
//	vm := gorgonia.NewTapeMachine(g, gorgonia.BindDualValues(weights, bias)) // 权重和偏置绑定为可学习节点
//
//	// 创建优化器
//	learnables := []*gorgonia.Node{weights, bias}
//	solver := gorgonia.NewAdamSolver(gorgonia.WithLearnRate(0.01))
//
//	// 开始训练
//	for epoch := 0; epoch < epochs; epoch++ {
//		// 运行计算图并计算梯度
//		if err := vm.RunAll(); err != nil {
//			return nil, fmt.Errorf("error during training: %v", err)
//		}
//
//		// 更新权重和偏置
//		if err := solver.Step(gorgonia.NodesToValueGrads(learnables)); err != nil {
//			return nil, fmt.Errorf("error during optimization step: %v", err)
//		}
//
//		// 打印损失值
//		fmt.Printf("Epoch %d: Loss: %v\n", epoch+1, loss.Value())
//
//		// 重置 VM 以便下一轮训练
//		vm.Reset()
//	}
//
//	// 释放资源
//	vm.Close()
//	return predicted, nil
//}
//
//// flatten 将二维数组展平为一维数组
//func flatten(data [][]float64) []float64 {
//	var flat []float64
//	for _, row := range data {
//		flat = append(flat, row...)
//	}
//	return flat
//}
//
//// oneHot 将标签转换为 One-Hot 编码
//func oneHot(labels []int, outputDim int) []float64 {
//	encoded := make([]float64, len(labels)*outputDim)
//	for i, label := range labels {
//		encoded[i*outputDim+label] = 1.0
//	}
//	return encoded
//}
//
//func main() {
//	// 示例特征和标签
//	features := [][]float64{
//		{1.0, 0.5, 0.2},
//		{0.9, 0.6, 0.3},
//		{0.8, 0.7, 0.4},
//	}
//	labels := []int{0, 1, 1}
//	inputDim, outputDim, epochs := 3, 2, 10
//
//	// 训练模型
//	model, err := trainModel(features, labels, inputDim, outputDim, epochs)
//	if err != nil {
//		log.Fatalf("Training failed: %v", err)
//	}
//
//	fmt.Println("Model trained successfully:", model)
//}

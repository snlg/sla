/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/4/12 22:57
*/

package main

import (
	"context"
	"fmt"
	clientv3 "go.etcd.io/etcd/client/v3"
	"time"
)

func main() {
	// 定义etcd集群的地址
	endpoints := []string{"http://127.0.0.1:2379"}

	// 创建etcd客户端连接
	cli, err := clientv3.New(clientv3.Config{
		Endpoints:   endpoints,
		DialTimeout: 5 * time.Second,
	})
	if err != nil {
		fmt.Printf("Failed to connect to etcd: %v\n", err)
		return
	}
	defer cli.Close()

	// 设置键值对
	key := "test_key"
	value := "test_value"
	_, err = cli.Put(context.Background(), key, value)
	if err != nil {
		fmt.Printf("Failed to set key-value pair: %v\n", err)
		return
	}

	// 获取键值对
	resp, err := cli.Get(context.Background(), key)
	if err != nil {
		fmt.Printf("Failed to get key-value pair: %v\n", err)
		return
	}

	// 检查值是否正确
	for _, kv := range resp.Kvs {
		if string(kv.Key) == key && string(kv.Value) == value {
			fmt.Println("etcd is available and working correctly.")
			return
		}
	}

	fmt.Println("Failed to verify etcd availability.")
}

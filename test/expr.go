/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/4/29 17:46
*/

package main

import (
	"fmt"
	"github.com/Knetic/govaluate"
	"go.uber.org/zap"
	"reflect"
	"time"
)

type MessageEntry struct {
	Timestamp   time.Time `json:"@timestamp"`
	Province    string    `json:"host_group"`
	ReMark      string    `json:"host_remark_type"`
	URL         string    `json:"url"`
	Status      int32     `json:"status"`
	RequestTime float64   `json:"request_time_all"`
	Lines       float64   `json:"lines"`
	Bytes       float64   `json:"body_byte_sent"`
	Type        int32     `json:"type"`
	Action      string    `json:"action"`
	IpAddress   string    `json:"ip"`
}

type ExpressionMatcher struct {
	expressions []string
}

func NewExpressionMatcher(expressions []string) *ExpressionMatcher {
	return &ExpressionMatcher{
		expressions: expressions,
	}
}

func (em *ExpressionMatcher) Match(data MessageEntry) bool {
	parameters := make(map[string]interface{})
	fillParameters(data, parameters)

	for _, expressionString := range em.expressions {
		expression, err := govaluate.NewEvaluableExpression(expressionString)
		if err != nil {
			zap.L().Error("Error:" + err.Error())
			continue
		}

		result, err := expression.Evaluate(parameters)
		if err != nil {
			zap.L().Error("Error:" + err.Error())
			continue
		}

		// If any expression matches, return false
		if result.(bool) {
			fmt.Println("Matches")
			zap.L().Debug("匹配规则："+expressionString, zap.Any("msg", data))
			return true
		}
	}

	// If none of the expressions match, return true
	return false
}

func fillParameters(data interface{}, parameters map[string]interface{}) {
	val := reflect.ValueOf(data)
	typ := val.Type()

	for i := 0; i < val.NumField(); i++ {
		field := typ.Field(i)
		fieldValue := val.Field(i)

		// Only add exported fields
		if field.PkgPath != "" {
			continue
		}

		parameters[field.Tag.Get("json")] = fieldValue.Interface()
	}

	fmt.Printf("Parameters: %+v\n", parameters)

}

func main() {
	//expressions := []string{
	//	"(url == \"/topic\" && status == 499 && request_time_all < 100) || host_group == \"江苏\"",
	//	"status == 499 && request_time_all < 1000",
	//}

	expressions := []string{
		"(url == \"/topic\" && status == 499 && request_time_all < 100) || host_group == \"江苏\"",
		"status == 499 && request_time_all < 1000",
		//"(ip == \"112.25.36.226\" && url == \"/ysten-business/live/HD-4000k-1080P-cctv15/*\") || (ip == \"112.25.36.227\" && ( url == \"/ysten-business/live/HD-8000k-1080P-lowcctv13/*\" || url == \"/ysten-business/live/HD-8000k-1080P-lowcctv5/*\" )) || (ip == \"112.25.36.225\" && url == \"/ysten-business/live/HD-8000k-1080P-lowcctv1/*\")",
		//"ip == \"112.25.75.29\" && url =~ \"^/ysten-business/live/.*\"",
		"(url =~ \".*/ott/src/config.js$\")",
		//"(status == 404) && (( ip == \"111.63.42.58\" && (url == \"/ysten-business/live/SD-1500k-576P-bokesen/1.m3u8\" ||url == \"/ysten-business/live/hdheilongjiangstv/1.m3u8\" ||url == \"/ysten-business/live/HD-2500k-1080P-beijingjishi/1.m3u8\" ||url == \"/ysten-business/live/HD-2500k-1080P-beijingstv/1.m3u8\" ||url == \"/ysten-business/live/HD-2500k-1080P-cctv05plus/1.m3u8\" ||url == \"/ysten-business/live/HD-2500k-1080P-cctv1/1.m3u8\" ||url == \"/ysten-business/live/HD-2500k-1080P-cctv3/1.m3u8\" ||url == \"/ysten-business/live/HD-2500k-1080P-cctv8/1.m3u8\" ||url == \"/ysten-business/live/HD-2500k-1080P-dongfangstv/1.m3u8\" ||url == \"/ysten-business/live/HD-2500k-1080P-guangdongstv/1.m3u8\" ||url == \"/ysten-business/live/HD-2500k-1080P-jiangsustv/1.m3u8\" ||url == \"/ysten-business/live/HD-2500k-1080P-shandongstv/1.m3u8\" ||url == \"/ysten-business/live/HD-2500k-1080P-shanghaijishi/1.m3u8\" ||url == \"/ysten-business/live/HD-2500k-1080P-tianjinstv/1.m3u8\" ||url == \"/ysten-business/live/HD-8000k-1080P-migudongao5/1.m3u8\" ||url == \"/ysten-business/live/HD-8000k-1080P-migudongao6/1.m3u8\" ||url == \"/ysten-business/live/HD-2500k-1080P-cctv5/1.m3u8\" ||url == \"/ysten-business/live/HD-2500k-1080P-cctv6/1.m3u8\" ||url == \"/ysten-business/live/HD-2500k-1080P-hunanstv/1.m3u8\" ||url == \"/ysten-business/live/HD-2500k-1080P-shenzhenstv/1.m3u8\" ||url == \"/ysten-business/live/HD-2500k-1080P-zhejiangstv/1.m3u8\" ||url == \"/ysten-business/live/SD-1500k-576P-bokesen/playlist.m3u8\" ||url == \"/ysten-business/live/HD-2500k-1080P-cctv1/playlist.m3u8\" ||url == \"/ysten-business/live/HD-2500k-1080P-guangdongstv/playlist.m3u8\" ||url == \"/ysten-business/live/HD-2500k-1080P-beijingstv/playlist.m3u8\" ||url == \"/ysten-business/live/HD-2500k-1080P-cctv05plus/playlist.m3u8\" ||url == \"/ysten-business/live/HD-2500k-1080P-zhejiangstv/playlist.m3u8\" ||url == \"/ysten-business/live/HD-2500k-1080P-cctv3/playlist.m3u8\" ||url == \"/ysten-business/live/HD-2500k-1080P-cctv5/playlist.m3u8\" ||url == \"/ysten-business/live/HD-2500k-1080P-jiangsustv/playlist.m3u8\" ||url == \"/ysten-business/live/HD-2500k-1080P-cctv6/playlist.m3u8\" ||url == \"/ysten-business/live/HD-2500k-1080P-shenzhenstv/playlist.m3u8\" ||url == \"/ysten-business/live/HD-2500k-1080P-tianjinstv/playlist.m3u8\" ||url == \"/ysten-business/live/HD-2500k-1080P-cctv8/playlist.m3u8\" ||url == \"/ysten-business/live/HD-2500k-1080P-shandongstv/playlist.m3u8\" ||url == \"/ysten-business/live/HD-2500k-1080P-dongfangstv/playlist.m3u8\" ||url == \"/ysten-business/live/HD-2500k-1080P-hunanstv/playlist.m3u8\"))|| (ip == \"218.207.221.39\" && (url == \"/ysten-business/live/HD-8000k-1080P-npgg/1.m3u8\" ||url == \"/ysten-business/live/HD-8000k-1080P-npxwzh/1.m3u8\"))|| (ip == \"218.207.221.43\" && (url == \"/ysten-bussiness/live/fjfuzhouxwzh/1.m3u8\" ||url == \"/ysten-bussiness/live/fjlongyyantv1/1.m3u8\" ||url == \"/ysten-bussiness/live/fjningdetv1/1.m3u8\" ||url == \"/ysten-bussiness/live/fjfenghuang1/1.m3u8\" ||url == \"/ysten-bussiness/live/fjpingtanxwzh/1.m3u8\" ||url == \"/ysten-bussiness/live/fjzhangzhoutv1/1.m3u8\" ||url == \"/ysten-bussiness/live/fjputianxwzh/1.m3u8\" ||url == \"/ysten-bussiness/live/fjxuandongkatong/1.m3u8\" ||url == \"/ysten-bussiness/live/fjxiamentv1/1.m3u8\" ||url == \"/ysten-bussiness/live/fjquanzhouzh/1.m3u8\" ||url == \"/ysten-bussiness/live/fjfenghuang2/1.m3u8\")) || (ip == \"183.224.42.227\" && (url == \"/ysten-business/live/ynkmtv6/1.m3u8\" ||url == \"/ysten-business/live/ynkmtv5/1.m3u8\" ||url == \"/ysten-business/live/ynkmtv1/1.m3u8\" ||url == \"/ysten-business/live/SD-1200k-720P-ynkmtv4/1.m3u8\" ||url == \"/ysten-business/live/SD-1200k-720P-ynkmtv3/1.m3u8\" ||url == \"/ysten-business/live/SD-1200k-720P-ynkmtv2/1.m3u8\")) || (ip == \"112.25.36.204\" && (url == \"/ysten-business/reallive/jsHD4-5M/1.m3u8\" ||url == \"/ysten-business/reallive/jsguangbo/1.m3u8\" ||url == \"/ysten-business/live/H265-5500k-1080P-cctv10/1/202408/031000/*\")) || (ip == \"112.25.36.227\" && url == \"/ysten-business/live/HD-100M-4320P-cctv8k/*\") || (ip == \"112.25.36.225\" && url == \"/ysten-business/live/cctv-10/1.m3u8\") || (ip == \"58.211.142.199\" && (url == \"/ysten-business/live/H265-8000k-4320P-cctv10/1/202408/031000/*\" ||url == \"/ysten-business/live/H265-8000k-4320P-cctv10/playlist.m3u8\" ||url == \"/ysten-business/live/H265-4000k-1080P-cctv6/playlist.m3u8\" ||url == \"/ysten-business/live/H265-4000k-1080P-cctv1/playlist.m3u8\" ||url == \"/ysten-business/live/H265-4000k-1080P-cctv3/playlist.m3u8\")) || (ip == \"218.108.0.115\" && (url == \"/ysten-business/reallive/ysydianbo/1.m3u8\" ||url == \"/ysten-business/live/HD-4M-720P-jxsctv/1.m3u8\" ||url == \"/ysten-business/live/HD-4M-720P-xsstv/1.m3u8\")) || (ip == \"223.82.250.92\" && url == \"/ysten-business/reallive/jshunanstv/1.m3u8\") || (ip == \"61.160.105.170\" && url == \"/ysten-business/reallive/jsguangbo/1.m3u8\") || (ip == \"36.99.146.59\" && url == \"/ysten-business/reallive/yzgdzhanwang/1.m3u8\") || (ip == \"223.82.250.91\" && (url == \"/ysten-business/reallive/jshunanstv/1.m3u8\" ||url == \"/ysten-business/live/HD-4M-720P-zhangjiang/1.m3u8\")) || (ip == \"36.99.146.56\" && (url == \"/ysten-business/reallive/hdguangdongstv/1.m3u8\" ||url == \"/ysten-business/reallive/hdhunanstv/1.m3u8\"))|| (ip == \"112.25.48.181\" && (url == \"/ysten-business/reallive/jshunanstv/1.m3u8\" ||url == \"/ysten-business/reallive/yzgdzhanwang/1.m3u8\")) || (ip == \"112.25.48.180\" && url == \"/ysten-business/reallive/jsguangbo/1.m3u8\") || (ip == \"112.25.48.182\" && (url == \"/ysten-business/live/HD-100M-4320P-cctv8k/*\" ||url == \"/ysten-business/reallive/yzgdzhanwang/1.m3u8\")) || (ip == \"36.99.146.61\" && url == \"/ysten-business/reallive/hdshandongstv/1.m3u8\") || (ip == \"183.224.42.179\" && (url == \"/ysten-business/reallive/SD-1500k-576P-yncctvzsgw/1.m3u8\" ||url == \"/ysten-business/reallive/SD-1500k-576P-ynsanshastv/1.m3u8\"))|| (ip == \"117.169.52.69\" && (url == \"/ysten-businessmobile/live/SD-1200K-720P-XHSEN/1.m3u8\" ||url == \"/ysten-businessmobile/live/SD-1200K-720P-XHSZH/1.m3u8\"))|| (ip == \"112.25.75.29\" && url == \"/ysten-business/live/HD-8000k-1080P-hunanstv/1.m3u8\")|| (ip == \"112.25.36.202\" && (url == \"/ysten-business/reallive/lvyoustv/1.m3u8\" ||url == \"/ysten-business/reallive/cctv-8/1.m3u8\"\t||url == \"/ysten-business/reallive/hdhunanstv/1.m3u8\"\t||url == \"/ysten-business/reallive/jzongyi/1.m3u8\" ||url == \"/ysten-business/reallive/hdtianjinstv/1.m3u8\" ||url == \"/ysten-business/reallive/hdshandongstv/hdshandongstv_3000.m3u8\" ||url == \"/ysten-business/reallive/hdguangdongstv/1.m3u8\" ||url == \"/ysten-business/reallive/hdshandongstv/hdshandongstv.m3u8\" ||url == \"/ysten-business/reallive/hunanstv/1.m3u8\" ||url == \"/ysten-business/reallive/beijingstv/1.m3u8\" ||url == \"/ysten-business/reallive/hdhubeistv/1.m3u8\" ||url == \"/ysten-business/reallive/hddongfangstv/hddongfangstv.m3u8\" ||url == \"/ysten-business/reallive/hdheilongjiangstv/hdheilongjiangstv.m3u8\" ||url == \"/ysten-business/reallive/zhejiangstv/23.m3u8\" ||url == \"/ysten-business/reallive/hdzhejiangstv/hdzhejiangstv.m3u8\" ||url == \"/ysten-business/reallive/SD-1500k-576P-jiayougw/1.m3u8\" ||url == \"/ysten-business/reallive/hdcctv01/hdcctv01.m3u8\" ||url == \"/ysten-business/reallive/jiangsustv/1.m3u8\" ||url == \"/ysten-business/reallive/cctvennews/1.m3u8\" ||url == \"/ysten-business/reallive/hdbeijingstv/hdbeijingstv.m3u8\" ||url == \"/ysten-business/reallive/hdjiangsustv/1.m3u8\" ||url == \"/ysten-business/reallive/hdshenzhenstv/1.m3u8\" ||url == \"/ysten-business/reallive/jilinstv/1.m3u8\" ||url == \"/ysten-business/reallive/H265-4000k-1080P-cctv1/1.m3u8\" ||url == \"/ysten-business/reallive/H265-5500k-1080P-beijingjishi/1.m3u8\" ||url == \"/ysten-bussiness/live/cctv-8/1.m3u8\" ||url == \"/ysten-business/reallive/H265-5500k-1080P-cctv6/1.m3u8\" ||url == \"/ysten-business/reallive/HD-1500k-720P-jsdhxw/1.m3u8\" ||url == \"/ysten-business/reallive/cntvtest1/1.m3u8\" ||url == \"/ysten-business/reallive/XKDHDR-50M-50p-Z4k/1.m3u8\" ||url == \"/ysten-business/reallive/XKDHD-4000k-1080P-cxs/1.m3u8\" ||url == \"/ysten-business/reallive/yntv1/1.m3u8\" ||url == \"/ysten-business/reallive/JS_jiangsu/1.m3u8\")))",
		//"(status == 404) && ( ip == \"112.25.36.225\" || ip == \"218.207.221.39\" || ip == \"117.169.52.64\" || ip == \"117.169.52.62\" || ip == \"117.169.52.66\" || ip == \"111.28.3.47\")",
	}

	matcher := NewExpressionMatcher(expressions)

	data := MessageEntry{
		Timestamp:   time.Now(),
		Province:    "江苏1",
		ReMark:      "remark",
		URL:         "/topics/2024/1108/ott/src/config.js",
		Status:      200,
		RequestTime: 5000.5,
		Lines:       100,
		Bytes:       1024,
		Type:        1,
		IpAddress:   "112.25.75.29",
	}

	matchResult := matcher.Match(data)
	fmt.Println("Match Result:", matchResult)
}

//func main() {
//	expressions := []string{
//		"regex(`.*/ott/src/config.js$`, url)",
//	}
//
//	matcher := NewExpressionMatcher(expressions)
//
//	entry := MessageEntry{
//		URL:         "http://example.com/ott/src/config.js",
//		Status:      499,
//		RequestTime: 50,
//		Province:    "江苏",
//	}
//
//	if matcher.Match(entry) {
//		fmt.Println("匹配成功")
//	} else {
//		fmt.Println("匹配失败")
//	}
//}

/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/7/16 19:51
*/

package main

import (
	"encoding/json"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
)

type MetaBase struct {
	ID        string `json:"_id" bson:"_id"`
	Timestamp int64  `json:"timestamp" bson:"timestamp"`
	Province  string `json:"province" bson:"province"`
	ReMark    string `json:"reMark" bson:"reMark"`
	Regex     string `json:"regex" bson:"regex"`
}

type MetaSMS struct {
	MetaBase  `bson:",inline"`
	IpAddress string `json:"ipAddress" bson:"ipAddress"`
}

func main() {
	meta := MetaSMS{
		MetaBase: MetaBase{
			ID:        "49c835c0dc754fc3944d8c1c00ba00ec6b5a0819",
			Timestamp: 1721013120,
			Province:  "直播源",
			ReMark:    "重庆移动水土机房",
			Regex:     "^/ysten-bus*",
		},
		IpAddress: "",
	}

	// 将 MetaSMS 序列化为 JSON
	jsonData, err := json.MarshalIndent(meta, "", "  ")
	if err != nil {
		fmt.Println("JSON Marshal Error:", err)
		return
	}
	fmt.Println("JSON Output:\n", string(jsonData))

	// 将 MetaSMS 序列化为 BSON
	bsonData, err := bson.Marshal(meta)
	if err != nil {
		fmt.Println("BSON Marshal Error:", err)
		return
	}

	var bsonOutput map[string]interface{}
	err = bson.Unmarshal(bsonData, &bsonOutput)
	if err != nil {
		fmt.Println("BSON Unmarshal Error:", err)
		return
	}

	bsonJsonData, err := json.MarshalIndent(bsonOutput, "", "  ")
	if err != nil {
		fmt.Println("BSON to JSON Marshal Error:", err)
		return
	}
	fmt.Println("BSON Output:\n", string(bsonJsonData))
}

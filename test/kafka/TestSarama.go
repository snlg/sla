/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/5/1 15:20
*/

package main

import (
	"context"
	"os"
	"os/signal"
	"sync"
	"time"

	"github.com/IBM/sarama"
	"go.uber.org/zap"
)

type KafkaConsumer struct {
	Config         *Config
	MessageHandler MessageHandler
}

type MessageHandler func(msg *Message)

type Message struct {
	Key   []byte
	Value []byte
}

type Config struct {
	Brokers        []string      // Kafka集群的地址列表
	Topic          string        // 要消费的主题名称
	GroupID        string        // 消费者组ID
	QueueCapacity  int           // 内部消息队列的容量
	CommitInterval time.Duration // 自动提交偏移量的时间间隔
	SessionTimeout time.Duration // 会话超时时间
}

func NewKafkaConsumer(config *Config, handler MessageHandler) *KafkaConsumer {
	return &KafkaConsumer{
		Config:         config,
		MessageHandler: handler,
	}
}

func (c *KafkaConsumer) ConsumeMessages(ctx context.Context) {
	config := sarama.NewConfig()
	config.Version = sarama.V2_8_0_0
	config.Consumer.Group.Rebalance.Strategy = sarama.BalanceStrategyRange
	config.Consumer.Offsets.AutoCommit.Enable = true
	config.Consumer.Offsets.AutoCommit.Interval = c.Config.CommitInterval
	config.Consumer.Group.Session.Timeout = c.Config.SessionTimeout

	consumer, err := sarama.NewConsumerGroup(c.Config.Brokers, c.Config.GroupID, config)
	if err != nil {
		zap.L().Error("Error creating Kafka consumer group: ", zap.Error(err))
		return
	}
	defer consumer.Close()

	wg := sync.WaitGroup{}
	wg.Add(1)

	go func() {
		defer wg.Done()
		for {
			if err := consumer.Consume(ctx, []string{c.Config.Topic}, c); err != nil {
				zap.L().Error("Error from consumer: ", zap.Error(err))
				return
			}
			if ctx.Err() != nil {
				return
			}
		}
	}()

	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt)
	<-sigChan
	zap.L().Debug("Received termination signal, shutting down")

	consumer.Close()
	wg.Wait()
}

func (c *KafkaConsumer) Setup(sarama.ConsumerGroupSession) error {
	return nil
}

func (c *KafkaConsumer) Cleanup(sarama.ConsumerGroupSession) error {
	return nil
}

func (c *KafkaConsumer) ConsumeClaim(session sarama.ConsumerGroupSession, claim sarama.ConsumerGroupClaim) error {
	for message := range claim.Messages() {
		customMsg := &Message{
			Key:   message.Key,
			Value: message.Value,
		}
		go c.MessageHandler(customMsg)
		session.MarkMessage(message, "")
	}
	return nil
}

func handleMessage(msg *Message) {
	zap.L().Info("Received message", zap.ByteString("key", msg.Key), zap.ByteString("value", msg.Value))
}

func main() {
	logger, err := zap.NewDevelopment()
	if err != nil {
		zap.L().Error("Error initializing logger: ", zap.Error(err))
		return
	}
	defer logger.Sync()
	zap.ReplaceGlobals(logger)

	kafkaConfig := &Config{
		Brokers:        []string{"localhost:9092"},
		Topic:          "test",
		GroupID:        "consumer-group",
		QueueCapacity:  256,
		CommitInterval: 1 * time.Second,
		SessionTimeout: 30 * time.Second,
	}

	consumer := NewKafkaConsumer(kafkaConfig, handleMessage)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	consumer.ConsumeMessages(ctx)
}

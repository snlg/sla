/*
@Project ：sla
@Author  ：SHAN
@Date    ：2024/4/15 10:22
*/

package main

import (
	"fmt"
	"log"
	"sla/common/storage/elasticsearch"
)

func main() {
	ea := elasticsearch.MonitorES{
		Address:  []string{"http://escollect.kunlun.bcs.ottcn.com:29200"},
		Username: "elasticsearch",
		Password: "4nIrN2gZ4kKM",
		SLAIndex: "slatest",
	}
	client, err := elasticsearch.NewElasticClient(ea)
	if err != nil {
		log.Fatalf("Error creating ElasticSearch client: %s", err)
	}

	index := ea.SLAIndex
	documents := []map[string]interface{}{
		{"id": "1", "title": "Document 1", "body": "This is document 1."},
		{"id": "2", "title": "Document 2", "body": "This is document 2."},
	}

	res, err := client.IndexDocuments(index, documents)
	if err != nil {
		log.Fatalf("Error indexing document: %s", err)
	}

	fmt.Println("Indexed document:", res)

}
